pypmadra
========

.. currentmodule:: pypmadra

.. automodule:: pypmadra

.. autosummary::

   libpmadra_info
   fold_Mc


libpmadra_info
--------------

.. autofunction:: libpmadra_info


fold_Mc
-------

.. autofunction:: fold_Mc
