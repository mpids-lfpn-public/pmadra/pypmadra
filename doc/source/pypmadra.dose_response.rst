pypmadra.dose_response
======================

.. currentmodule:: pypmadra.dose_response

.. automodule:: pypmadra.dose_response

.. autosummary::

   mean_risk_exponential


mean_risk_exponential
---------------------

.. autofunction:: mean_risk_exponential
