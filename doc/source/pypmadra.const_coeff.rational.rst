pypmadra.const_coeff.rational
=============================

.. currentmodule:: pypmadra.const_coeff.rational

.. automodule:: pypmadra.const_coeff.rational

.. autosummary::

   Vks
   Uks
   n_inf


Vks
---

.. autofunction:: Vks


Uks
---

.. autofunction:: Uks


n_inf
-----

.. autofunction:: n_inf
