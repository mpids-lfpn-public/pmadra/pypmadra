API
===

.. toctree::
   :maxdepth: 2

   pypmadra
   pypmadra.const_coeff
   pypmadra.const_coeff.rational
   pypmadra.const_coeff.multi
   pypmadra.dose_response
