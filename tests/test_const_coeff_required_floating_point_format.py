# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose

import pytest

from pypmadra.const_coeff import required_floating_point_format


# Initialize the random generators.
random.seed()
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_real_params = (complex(1, 2), complex(1, 0), ..., slice(4),
                   [], (), set(), dict())


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# non-real alpha, gamma, emax_safety_factor

@pytest.mark.parametrize('try_libpmadra,alpha',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_alpha_nonnumber(try_libpmadra, alpha):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,gamma',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_gamma_nonnumber(try_libpmadra, gamma):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,emax_safety_factor',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_emax_safety_factor_nonnumber(try_libpmadra,
                                      emax_safety_factor):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)


# non-Sequence n0 and beta

@pytest.mark.parametrize('try_libpmadra,n0',
                         [(t, b)
                          for t in (True, False)
                          for b in non_seq_params])
def test_n0_nonseq(try_libpmadra, n0):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,beta',
                         [(t, b)
                          for t in (True, False)
                          for b in non_seq_params])
def test_beta_nonseq(try_libpmadra, beta):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# non-real element of n0 and beta

@pytest.mark.parametrize('try_libpmadra,element',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_n0_hasnonreal(try_libpmadra, element):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    n0 = rng.random((len(beta), ), 'float32').tolist()
    n0[random.randrange(len(n0))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,element',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_beta_hasnonreal(try_libpmadra, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32').tolist()
    beta[random.randrange(len(beta))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# NaN or negative element of n0 and beta

@pytest.mark.parametrize('param,try_libpmadra,kind',
                         [(p, t, k)
                          for p in ('n0', 'beta')
                          for t in (False, True)
                          for k in ('nan', '-inf',
                                    'negative')])
def test_n0_beta_nannegative(param, try_libpmadra, kind):
    beta_base = rng.random((random.randint(2, 100), ), 'float32')
    n0_base = rng.random((len(beta_base), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    for _ in range(10):
        if kind in ('nan', 'inf', '-inf'):
            element = float(kind)
        else:
            element = -1e4 * rng.random((1, ), 'float32')[0]
        if param == 'n0':
            n0 = n0_base.copy()
            n0[random.randrange(len(n0))] = element
            beta = beta_base
        else:
            n0 = n0_base
            beta = beta_base.copy()
            beta[random.randrange(len(n0))] = element
    with pytest.raises(ValueError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# unsupported dtype of n0 and beta

@pytest.mark.parametrize('try_libpmadra,tp',
                         [(t, v)
                          for t in (False, True)
                          for v in (numpy.complex64,
                                    numpy.complex128,
                                    numpy.object_)])
def test_n0_unsupported_dtype(try_libpmadra, tp):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    n0 = tp(rng.random((len(beta), ), 'float32'))
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,tp',
                         [(t, v)
                          for t in (False, True)
                          for v in (numpy.complex64,
                                    numpy.complex128,
                                    numpy.object_)])
def test_beta_unsupported_dtype(try_libpmadra, tp):
    beta = tp(rng.random((random.randint(2, 100), ), 'float32'))
    n0 = rng.random((len(beta), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# n0 and beta have too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i) for v in (True, False)
                          for i in range(2, 6)])
def test_n0_ndim_toohigh(try_libpmadra, ndim):
    n0 = rng.random([random.randint(1, 3) for i in range(ndim)],
                    'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i) for v in (True, False)
                          for i in range(2, 6)])
def test_beta_ndim_toohigh(try_libpmadra, ndim):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random([random.randint(1, 3) for i in range(ndim)],
                      'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        required_floating_point_format(n0, beta, alpha, gamma,
                                       try_libpmadra=try_libpmadra)


# n0 and beta's lengths don't match

@pytest.mark.parametrize('try_libpmadra', [True, False])
def test_n0_beta_lengthsdontmatch(try_libpmadra):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        length = random.randint(2, 100)
        while length == len(n0):
            length = random.randint(2, 100)
        beta = rng.random((length, ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            required_floating_point_format(n0, beta, alpha, gamma,
                                           try_libpmadra=try_libpmadra)


# NaN and infinite alpha, gamma, emax_safety_factor

@pytest.mark.parametrize('param,try_libpmadra,value',
                         [(p, t, v)
                          for p in ('alpha', 'gamma',
                                    'emax_safety_factor')
                          for t in (False, True)
                          for v in (numpy.nan, -numpy.inf, numpy.inf)])
def test_scalar_arg_nanorinf(param, try_libpmadra, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    if param == 'alpha':
        alpha = value
    else:
        alpha = rng.random((1, ), 'float32')[0]
    if param == 'gamma':
        gamma = value
    else:
        gamma = rng.random((1, ), 'float32')[0]
    if param == 'emax_safety_factor':
        emax_safety_factor = value
    else:
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
    if param == 'emax_safety_factor' or numpy.isnan(value) or value < 0:
        with pytest.raises(ValueError):
            required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
    else:
        emax, fmt = required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
        assert numpy.isfinite(emax)
        assert fmt is not None


# negative alpha, gamma, and emax_safety_factor

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_alpha_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = -1000 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_gamma_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = -1000 * rng.random((1, ), 'float32')[0]
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_emax_safety_factor_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = -1000 * rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)


# Check that as emax_safety_factor increases, the required emax
# stays constant and the required floating format gets progressively
# larger.

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_ordering_increasing_emax_safety_factor(try_libpmadra):
    if try_libpmadra:
        ordering = ('float32', 'float64', 'longdouble', 'quad', None)
    else:
        ordering = ('float32', 'float64', 'longdouble', None)
    lookup = {v: i for i, v in enumerate(ordering)}
    for _ in range(10):
        n0 = numpy.atleast_1d(rng.random(
            (max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3)), ),
            'float32'))
        beta = numpy.atleast_1d(rng.random(n0.shape, 'float32'))
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        last_index = None
        last_req_emax = None
        for emax_safety_factor in numpy.logspace(-1, 5, 100):
            req_emax, fmt = required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert isinstance(req_emax, float)
            assert fmt in lookup
            if gamma != 0.0:
                assert req_emax >= 0.0
            index = lookup[fmt]
            if last_index is not None:
                assert index >= last_index
                assert req_emax == last_req_emax
            elif len(n0) < 500:
                assert fmt is not None
            last_index = index
            last_req_emax = req_emax
        assert ordering[last_index] is None


# Check that as gamma <= 1 decreases, the required emax increases and
# the required floating format gets progressively larger.

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_ordering_decreasing_gamma(try_libpmadra):
    if try_libpmadra:
        ordering = ('float32', 'float64', 'longdouble', 'quad', None)
    else:
        ordering = ('float32', 'float64', 'longdouble', None)
    lookup = {v: i for i, v in enumerate(ordering)}
    for _ in range(5):
        n0 = numpy.longdouble(numpy.atleast_1d(rng.random(
            (max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3)), ),
            'float64')))
        beta = numpy.longdouble(numpy.atleast_1d(rng.random(n0.shape,
                                                            'float64')))
        alpha = numpy.longdouble(rng.random((1, ), 'float32')[0])
        emax_safety_factor = numpy.longdouble(
            10 * rng.random((1, ), 'float64')[0])
        last_index = None
        last_req_emax = None
        for gamma in numpy.logspace(
                0,
                (1 - 1e-6) * numpy.log10(
                    numpy.finfo('longdouble').max**-1),
                1000, dtype='longdouble'):
            req_emax, fmt = required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert isinstance(req_emax, float)
            assert fmt in lookup
            assert req_emax >= 0.0
            index = lookup[fmt]
            if last_index is not None:
                assert index >= last_index
                assert req_emax > last_req_emax
            elif len(n0) < 500 and gamma >= 0.5:
                assert fmt is not None
            last_index = index
            last_req_emax = req_emax
        assert ordering[last_index] is None


# Check that as largest element of n0 and beta increases, the required
# emax increases and the required floating format gets progressively
# larger.

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_ordering_increasing_n0_max(try_libpmadra):
    if try_libpmadra:
        ordering = ('float32', 'float64', 'longdouble', 'quad', None)
    else:
        ordering = ('float32', 'float64', 'longdouble', None)
    lookup = {v: i for i, v in enumerate(ordering)}
    for _ in range(10):
        n0 = numpy.longdouble(
            numpy.atleast_1d(rng.random(
                (max(1, int(2e4 * rng.random((1, ),
                                             'float64')[0]**3)), ),
                'float64')))
        beta = numpy.longdouble(numpy.atleast_1d(rng.random(n0.shape,
                                                            'float64')))
        alpha = numpy.longdouble(rng.random((1, ), 'float64')[0])
        gamma = numpy.longdouble(rng.random((1, ), 'float64')[0])
        emax_safety_factor = numpy.longdouble(
            10 * rng.random((1, ), 'float64')[0])
        index_to_change = random.randrange(len(n0))
        last_index = None
        last_req_emax = None
        for value in numpy.logspace(
                0, (1.0 - 1e-6) * numpy.log10(
                    numpy.finfo('longdouble').max),
                1000, dtype='longdouble'):
            n0[index_to_change] = value
            req_emax, fmt = required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert isinstance(req_emax, float)
            assert fmt in lookup
            if gamma != 0.0:
                assert req_emax >= 0.0
            index = lookup[fmt]
            if last_index is not None:
                assert index >= last_index
                assert req_emax > last_req_emax
            elif len(n0) < 500:
                assert fmt is not None
            last_index = index
            last_req_emax = req_emax
        assert ordering[last_index] is None


@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_ordering_increasing_beta_max(try_libpmadra):
    if try_libpmadra:
        ordering = ('float32', 'float64', 'longdouble', 'quad', None)
    else:
        ordering = ('float32', 'float64', 'longdouble', None)
    lookup = {v: i for i, v in enumerate(ordering)}
    for _ in range(10):
        n0 = numpy.longdouble(
            numpy.atleast_1d(rng.random(
                (max(1, int(2e4 * rng.random((1, ),
                                             'float64')[0]**3)), ),
                'float64')))
        beta = numpy.longdouble(numpy.atleast_1d(rng.random(n0.shape,
                                                            'float64')))
        alpha = numpy.longdouble(rng.random((1, ), 'float64')[0])
        gamma = numpy.longdouble(rng.random((1, ), 'float64')[0])
        emax_safety_factor = numpy.longdouble(
            10 * rng.random((1, ), 'float64')[0])
        index_to_change = random.randrange(len(n0))
        last_index = None
        last_req_emax = None
        for value in numpy.logspace(
                0, (1.0 - 1e-6) * numpy.log10(
                    numpy.finfo('longdouble').max),
                1000, dtype='longdouble'):
            beta[index_to_change] = value
            req_emax, fmt = required_floating_point_format(
                n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert isinstance(req_emax, float)
            assert fmt in lookup
            if gamma != 0.0:
                assert req_emax >= 0.0
            index = lookup[fmt]
            if last_index is not None:
                assert index >= last_index
                assert req_emax > last_req_emax
            elif len(n0) < 500:
                assert fmt is not None
            last_index = index
            last_req_emax = req_emax
        assert ordering[last_index] is None


# Check that as Mc increases, the required emax increases and the
# required floating format gets progressively larger.

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_ordering_increasing_Mc(try_libpmadra):
    if try_libpmadra:
        ordering = ('float32', 'float64', 'longdouble', 'quad', None)
    else:
        ordering = ('float32', 'float64', 'longdouble', None)
    lookup = {v: i for i, v in enumerate(ordering)}
    for _ in range(5):
        n0 = rng.random((20000, ), 'float32')
        beta = rng.random(n0.shape, 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        last_index = None
        last_req_emax = None
        for Mc in range(len(n0)):
            req_emax, fmt = required_floating_point_format(
                n0[:Mc], beta[:Mc], alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert isinstance(req_emax, float)
            assert fmt in lookup
            if gamma != 0.0:
                assert req_emax >= 0.0
            index = lookup[fmt]
            if last_index is not None:
                assert index >= last_index
                if Mc == 1 and gamma != 0.0:
                    assert req_emax >= last_req_emax
                else:
                    assert req_emax > last_req_emax
            elif Mc < 500:
                assert fmt is not None
            last_index = index
            last_req_emax = req_emax
        assert ordering[last_index] is None


# Check gamma = 0

def test_gamma_zero():
    for i in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = numpy.float32(1) + rng.random((1, ), 'float32')[0]
        gamma = (numpy.float32, numpy.float64,
                 numpy.longdouble)[i % 3](0)
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        req_max_f, fmt_f = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=True)
        req_max_p, fmt_p = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=False)
        assert fmt_f is not None
        assert fmt_p is not None
        assert req_max_f <= 1.0
        assert_allclose(req_max_f, req_max_p, rtol=0, atol=1e-6)


# Check +inf element of n0 and beta.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d) for t in (True, False)
                          for d in dtypes])
def test_n0_has_inf(try_libpmadra, dtype):
    tp = numpy.dtype(dtype).type
    for _ in range(10):
        n0 = tp(rng.random((random.randint(2, 100), ), 'float32'))
        n0[random.randrange(len(n0))] = numpy.inf
        beta = tp(rng.random((len(n0), ), 'float32'))
        alpha = tp(numpy.float32(1) + rng.random((1, ), 'float32')[0])
        gamma = tp(rng.random((1, ), 'float32')[0])
        emax_safety_factor = tp(10 * rng.random((1, ), 'float32')[0])
        req_max, fmt = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)
        assert numpy.isinf(req_max)
        assert req_max > 0
        assert fmt is None


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d) for t in (True, False)
                          for d in dtypes])
def test_beta_has_inf(try_libpmadra, dtype):
    tp = numpy.dtype(dtype).type
    for _ in range(10):
        n0 = tp(rng.random((random.randint(2, 100), ), 'float32'))
        beta = tp(rng.random((len(n0), ), 'float32'))
        beta[random.randrange(len(beta))] = numpy.inf
        alpha = tp(numpy.float32(1) + rng.random((1, ), 'float32')[0])
        gamma = tp(rng.random((1, ), 'float32')[0])
        emax_safety_factor = tp(10 * rng.random((1, ), 'float32')[0])
        req_max, fmt = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)
        assert numpy.isinf(req_max)
        assert req_max > 0
        assert fmt is None


# Compare libpmadra and python versions.

def test_compare():
    for i in range(100):
        tp = (numpy.float32, numpy.float64,
              numpy.longdouble)[i % 3]
        n0 = tp(rng.random((random.randint(2, 100), ), 'float64'))
        beta = tp(rng.random((len(n0), ), 'float64'))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = tp(10 * rng.random((1, ), 'float64')[0])
        req_max_f, _ = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=True)
        req_max_p, _ = required_floating_point_format(
            n0, beta, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=False)
        assert_allclose(req_max_f, req_max_p, rtol=1e-6, atol=1e-6)
