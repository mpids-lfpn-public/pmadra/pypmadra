# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose, assert_array_less, \
    assert_array_equal


import pytest

from pypmadra.dose_response import mean_risk_exponential


# Initialize the random generators.
random.seed()
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')
dtypes_all = ('auto', 'float32', 'float64', 'longdouble')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_scalar_params = (None, ..., slice(4), [], (), set(), dict(),
                     numpy.int32((3, 1)))
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_real_params = (complex(1, 2), complex(1, 0), ..., slice(4),
                   [], (), set(), dict())


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    mus = rng.random((random.randint(2, 40), ), 'float32')
    r = rng.random((1, ))[0]
    with pytest.raises(TypeError):
        mean_risk_exponential(mus, r, try_libpmadra=try_libpmadra)


# invalid dtype

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_dtype_invalid(try_libpmadra, dtype):
    mus = rng.random((random.randint(2, 40), ), 'float32')
    r = rng.random((1, ))[0]
    with pytest.raises(ValueError):
        mean_risk_exponential(mus, r, dtype=dtype,
                              try_libpmadra=try_libpmadra)


# non-Sequence mus

@pytest.mark.parametrize('try_libpmadra,dtype,mus',
                         [(t, d, b)
                          for t in (True, False)
                          for d in dtypes_all
                          for b in non_seq_params])
def test_mus_nonseq(try_libpmadra, dtype, mus):
    r = rng.random((1, ))[0]
    with pytest.raises(TypeError):
        mean_risk_exponential(mus, r, dtype=dtype,
                              try_libpmadra=try_libpmadra)


# mus has too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(t, i) for t in (True, False)
                          for i in range(3, 6)])
def test_mus_ndim_toohigh(try_libpmadra, ndim):
    mus = rng.random([random.randint(1, 3) for i in range(ndim)],
                     'float32')
    r = rng.random((1, ))[0]
    with pytest.raises(ValueError):
        mean_risk_exponential(mus, r, try_libpmadra=try_libpmadra)


# complex mus

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_mus_complex(try_libpmadra, dtype):
    mus = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    r = rng.random((1, ))[0]
    with pytest.raises(TypeError):
        mean_risk_exponential(mus, r, dtype='auto',
                              try_libpmadra=try_libpmadra)


# non-real element of mus.

@pytest.mark.parametrize('try_libpmadra,dims,dtype,element',
                         [(t, ds, d, v)
                          for t in (False, True)
                          for ds in (1, 2)
                          for d in ('auto', 'float32')
                          for v in non_real_params])
def test_mus_hasnonreal(try_libpmadra, dims, dtype, element):
    if dims == 1:
        mus = rng.random((random.randint(2, 100), ), 'float32')
        mus_to_pass = mus.tolist()
        mus_to_pass[random.randrange(len(mus))] = element
    else:
        mus = rng.random((random.randint(1, 10), random.randint(1, 10)),
                         'float32')
        mus_to_pass = mus.tolist()
        mus_to_pass[random.randrange(mus.shape[0])][
            random.randrange(mus.shape[1])] = element
    r = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        mean_risk_exponential(mus_to_pass, r, dtype=dtype,
                              try_libpmadra=try_libpmadra)


# NaN or -inf element of mus.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in (numpy.nan, -numpy.inf)])
def test_mus_hasnanninf(try_libpmadra, dtype, value):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        mus = tp(rng.random((random.randint(2, 100), ), 'float64'))
        mus[random.randrange(len(mus))] = value
        r = tp(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            mean_risk_exponential(mus, r, dtype=dtype,
                                  try_libpmadra=try_libpmadra)


# negative element of mus.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all])
def test_mus_hasnegative(try_libpmadra, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        mus = tp(rng.random((random.randint(2, 100), ), 'float64'))
        mus[random.randrange(len(mus))] *= -10
        r = tp(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            mean_risk_exponential(mus, r, dtype=dtype,
                                  try_libpmadra=try_libpmadra)


# non-scalar r

@pytest.mark.parametrize('try_libpmadra,r',
                         [(t, b)
                          for t in (True, False)
                          for b in non_scalar_params])
def test_r_nonscalar(try_libpmadra, r):
    mus = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(TypeError):
        mean_risk_exponential(mus, r, dtype='auto',
                              try_libpmadra=try_libpmadra)


# NaN or infinite r

@pytest.mark.parametrize('try_libpmadra,dtype,r',
                         [(t, d, b)
                          for t in (True, False)
                          for d in dtypes
                          for b in (numpy.nan, -numpy.inf, numpy.inf)])
def test_r_naninf(try_libpmadra, dtype, r):
    tp = numpy.dtype(dtype).type
    mus = tp(rng.random((random.randint(2, 100), ), 'float32'))
    with pytest.raises(ValueError):
        mean_risk_exponential(mus, tp(r), dtype=dtype,
                              try_libpmadra=try_libpmadra)


# r out of range.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (True, False)
                          for d in dtypes_all])
def test_r_out_of_range(try_libpmadra, dtype):
    vals = numpy.hstack((-100 * rng.random((100, ), 'float64'),
                         1 + 10 * rng.random((100, ), 'float64')))
    for i, r in enumerate(vals):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        mus = tp(rng.random((random.randint(2, 100), ), 'float64'))
        with pytest.raises(ValueError):
            mean_risk_exponential(mus, tp(r), dtype=dtype,
                                  try_libpmadra=try_libpmadra)


# r = 0 (risk should be zero)

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, n, d)
                          for t in (True, False)
                          for n in (1, 2)
                          for d in dtypes_all])
def test_r_zero(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dims == 1:
            shape = (random.randint(1, 100), )
        else:
            shape = tuple([random.randint(1, 10) for _ in range(2)])
        mus = tp(rng.random(shape, 'float64')).reshape(shape)
        risks = mean_risk_exponential(mus, 0, dtype=dtype,
                                      try_libpmadra=try_libpmadra)
        if dims == 1:
            assert risks.shape == ()
            assert risks == 0
        else:
            assert risks.shape == (shape[1], )
            assert_array_equal(risks,
                               numpy.zeros((shape[1], ), dtype=tp))


# mus = 0 (risk should be zero)

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, n, d)
                          for t in (True, False)
                          for n in (1, 2)
                          for d in dtypes_all])
def test_mus_zero(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dims == 1:
            shape = (random.randint(1, 100), )
        else:
            shape = tuple([random.randint(1, 10) for _ in range(2)])
        mus = numpy.zeros(shape, dtype=tp)
        if i == 0:
            r = tp(1)
        else:
            r = tp(rng.random((1, ), 'float64')[0])
        risks = mean_risk_exponential(mus, r, dtype=dtype,
                                      try_libpmadra=try_libpmadra)
        if dims == 1:
            assert risks.shape == ()
            assert risks == 0
        else:
            assert risks.shape == (shape[1], )
            assert_array_equal(risks,
                               numpy.zeros((shape[1], ), dtype=tp))



# risks are less than 1 if all mus are finite and 1 if at least one is
# infinite.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (True, False)
                          for d in dtypes_all])
def test_mus_finite_infinite(try_libpmadra, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        shape = tuple([random.randint(1, 10) for _ in range(2)])
        mus = tp(rng.random(shape, 'float64')).reshape(shape)
        mask = (rng.random((shape[1], )) >= 0.5)
        for j, v in enumerate(mask):
            if v:
                mus[random.randrange(shape[0]), j] = numpy.inf
        if i == 0:
            r = tp(1)
        else:
            r = tp(rng.random((1, ), 'float64')[0])
        risks = mean_risk_exponential(mus, r, dtype=dtype,
                                      try_libpmadra=try_libpmadra)
        assert numpy.isfinite(risks).all()
        assert_array_less(0, risks)
        assert_array_equal(risks[mask], 1)
        assert_array_less(risks[~mask], 1)


# Risk increases as r increases.

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (True, False)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_increasing_r(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        shape = tuple([random.randint(1, 10) for _ in range(dims)])
        mus = tp(rng.random(shape, 'float64')).reshape(shape)
        last_risks = None
        for r in numpy.logspace(-4, 0, 20):
            risks = mean_risk_exponential(mus, r, dtype=dtype,
                                          try_libpmadra=try_libpmadra)
            assert_array_less(0, risks)
            assert_array_less(risks, 1)
            if last_risks is not None:
                assert_array_less(last_risks, risks)
            last_risks = risks


# Risk increases as more rows are included, and match the truncated
# formulas from the manuscript for Mc <= 3.

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (True, False)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_increasing_Mc(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        shape = tuple([random.randint(4, 10) for _ in range(dims)])
        mus = tp(rng.random(shape, 'float64')).reshape(shape)
        r = tp(rng.random((1, ), 'float64')[0])
        last_risks = None
        for Mc in range(1, shape[0]):
            risks = mean_risk_exponential(mus[:Mc], r, dtype=dtype,
                                          try_libpmadra=try_libpmadra)
            assert_array_less(0, risks)
            assert_array_less(risks, 1)
            if last_risks is not None:
                assert_array_less(last_risks, risks)
            if Mc == 1:
                assert_allclose(risks, tp(1) - numpy.exp(-r * mus[0]),
                                rtol=1e-4, atol=1e-7)
            elif Mc == 2:
                assert_allclose(
                    risks,
                    tp(1) - numpy.exp(-r * (mus[0]
                                            + (tp(2) - r) * mus[1])),
                    rtol=1e-4, atol=1e-7)
            elif Mc == 3:
                assert_allclose(
                    risks,
                    tp(1) - numpy.exp(-r * (mus[0]
                                            + (tp(2) - r) * mus[1]
                                            + (tp(3) - tp(3) * r
                                               + r**2) * mus[2])),
                    rtol=1e-4, atol=1e-7)
            last_risks = risks


# Risk does not increase when adding zero rows.

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (True, False)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_adding_zero_rows(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        shape = tuple([random.randint(4, 10) for _ in range(dims)])
        mus = tp(rng.random(shape, 'float64')).reshape(shape)
        r = tp(rng.random((1, ), 'float64')[0])
        risks = mean_risk_exponential(mus, r, dtype=dtype,
                                      try_libpmadra=try_libpmadra)
        for extra in range(1, 10):
            new_risks = mean_risk_exponential(
                numpy.concatenate(
                    (mus, numpy.zeros([extra] + list(shape[1:]),
                                      dtype=tp)), 0), r, dtype=dtype,
                try_libpmadra=try_libpmadra)
            if try_libpmadra:
                assert_array_equal(new_risks, risks)
            else:
                assert_allclose(new_risks, risks,
                                rtol=10*numpy.finfo(tp).eps, atol=1e-7)


# Do a cross comparison between all functions calculating the risk.

def test_compare_all():
    for i in range(10):
        Mc = random.randint(1, 30)
        nmu = random.randint(3, 10)
        shape = (Mc, nmu)
        mus = rng.random(shape, 'float32').reshape(shape)
        # r will be 0, 1, and then random.
        if i == 0:
            r = 0.0
        elif i == 1:
            r = 1.0
        else:
            r = rng.random((1, ), 'float32')[0]
        # Get the risks en mass for all dtypes with and without
        # libpmadra and check that they agree with each other.
        by_p = {v: mean_risk_exponential(numpy.dtype(v).type(mus),
                                         numpy.dtype(v).type(r),
                                         dtype=v,
                                         try_libpmadra=False)
                for v in dtypes}
        by_f = {v: mean_risk_exponential(numpy.dtype(v).type(mus),
                                         numpy.dtype(v).type(r),
                                         dtype=v,
                                         try_libpmadra=True)
                for v in dtypes}
        for k in dtypes:
            risks_p = by_p[k]
            risks_f = by_f[k]
            # Check shape and dtype.
            assert risks_p.shape == tuple(shape[1:])
            assert risks_f.shape == tuple(shape[1:])
            assert risks_p.dtype == numpy.dtype(k)
            assert risks_f.dtype == numpy.dtype(k)
            # Check that the values are all close.
            tol = 50 * numpy.finfo(k).eps
            assert_allclose(risks_f, risks_p, rtol=tol, atol=1e-7)
            if k != 'longdouble':
                assert_allclose(by_f['longdouble'], risks_f,
                                rtol=tol, atol=1e-7)
            # Check that all values are between 0 and 1.
            assert numpy.all(risks_p >= 0.0)
            assert numpy.all(risks_f >= 0.0)
            assert numpy.all(risks_p <= 1.0)
            assert numpy.all(risks_f <= 1.0)
