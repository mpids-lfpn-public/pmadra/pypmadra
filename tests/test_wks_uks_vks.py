# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose, assert_array_less, \
    assert_array_equal

import gmpy2

import pytest

import pypmadra.const_coeff as cf
import pypmadra.const_coeff.multi as cfm
import pypmadra.const_coeff.rational as cfr



# Initialize the random generators.
random.seed()
gmpy2rng = gmpy2.random_state(random.randrange(2**32))
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')
dtypes_all = ('auto', 'float32', 'float64', 'longdouble')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_int_params = (-1.3, None, ..., slice(9), [], (), set(), dict(),
                  gmpy2.mpq(3, 2), numpy.float64(-3),
                  numpy.int32((3, 1)))
non_real_params = (complex(1, 2), complex(1, 0), None, ..., slice(4),
                   [], (), set(), dict())
non_real_params_numpy = (complex(1, 2), complex(1, 0), ..., slice(4),
                         [], (), set(), dict())


# gmpy2 is missing.

def test_rational_vks_gmpy2_missing():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    old_value = cfr._has_gmpy2
    try:
        cfr._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfr.Vks(y, x)
    except:
        raise
    finally:
        cfr._has_gmpy2 = old_value


def test_multi_vks_gmpy2_missing():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    old_value = cfm._has_gmpy2
    try:
        cfm._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfm.Vks(y, x)
    except:
        raise
    finally:
        cfm._has_gmpy2 = old_value


def test_rational_uks_gmpy2_missing():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    old_value = cfr._has_gmpy2
    try:
        cfr._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfr.Uks(y, x, alpha, gamma)
    except:
        raise
    finally:
        cfr._has_gmpy2 = old_value


def test_multi_uks_gmpy2_missing():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    old_value = cfm._has_gmpy2
    try:
        cfm._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfm.Uks(y, x, alpha, gamma)
    except:
        raise
    finally:
        cfm._has_gmpy2 = old_value


def test_multi_wks_gmpy2_missing():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    old_value = cfm._has_gmpy2
    try:
        cfm._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfm.Wks(y, x, alpha, gamma)
    except:
        raise
    finally:
        cfm._has_gmpy2 = old_value


# NaN and -inf alpha, and +inf for the rational versions.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_uks_alpha_nan_or_ninf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = float(value)
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_uks_alpha_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = float(value)
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(ValueError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_uks_alpha_nan_or_ninf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(rng.random((1, ), 'float32')[0])
    alpha = float(value)
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_wks_alpha_nan_or_ninf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = float(value)
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_wks_alpha_nan_or_ninf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(rng.random((1, ), 'float32')[0])
    alpha = float(value)
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Wks(y, x, alpha, gamma)


# NaN and -inf gamma, and +inf for the rational versions.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_uks_gamma_nan_or_ninf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    gamma = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_uks_gamma_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(1, 100), 100)
    gamma = float(value)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(ValueError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_uks_gamma_nan_or_ninf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(rng.random((1, ), 'float32')[0])
    gamma = float(value)
    alpha = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_wks_gamma_nan_or_ninf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    gamma = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_wks_gamma_nan_or_ninf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(rng.random((1, ), 'float32')[0])
    gamma = float(value)
    alpha = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Wks(y, x, alpha, gamma)


# NaN, -inf, or +inf x or element of x

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf', 'inf')])
def test_vks_x_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    x[random.randrange(len(x))] = float(value)
    with pytest.raises(ValueError):
        cf.Vks(y, x, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_vks_x_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = float(value)
    with pytest.raises(ValueError):
        cfr.Vks(y, x)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_multi_vks_x_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(value)
    with pytest.raises(ValueError):
        cfm.Vks(y, x)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf', 'inf')])
def test_uks_x_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    x[random.randrange(len(x))] = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_uks_x_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = float(value)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(ValueError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_multi_uks_x_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(value)
    alpha = float(rng.random((1, ), 'float32')[0])
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf', 'inf')])
def test_wks_x_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    x[random.randrange(len(x))] = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_multi_wks_x_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x = float(value)
    alpha = float(rng.random((1, ), 'float32')[0])
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Wks(y, x, alpha, gamma)


# -inf and NaN element of y, and +inf for the rational version.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_vks_y_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    y[random.randrange(len(y))] = float(value)
    x = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(ValueError):
        cf.Vks(y, x, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_vks_y_element_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    y[random.randrange(len(y))] = float(value)
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(ValueError):
        cfr.Vks(y, x)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_vks_y_element_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = float(value)
    x = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Vks(y, x)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_uks_y_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    y[random.randrange(len(y))] = float(value)
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_uks_y_element_nan_or_inf(value):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    y[random.randrange(len(y))] = float(value)
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(ValueError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_uks_y_element_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = float(value)
    x = float(rng.random((1, ), 'float32')[0])
    alpha = float(rng.random((1, ), 'float32')[0])
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_wks_y_element_nan_or_inf(try_libpmadra, value):
    y = rng.random((random.randint(2, 100), ), 'float32')
    y[random.randrange(len(y))] = float(value)
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_wks_y_element_nan_or_inf(value):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = float(value)
    x = float(rng.random((1, ), 'float32')[0])
    alpha = float(rng.random((1, ), 'float32')[0])
    gamma = float(rng.random((1, ), 'float32')[0])
    with pytest.raises(ValueError):
        cfm.Wks(y, x, alpha, gamma)


# negative alpha

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_uks_alpha_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        alpha = -100 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_uks_alpha_negative():
    for _ in range(10):
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        x = gmpy2.mpq(random.randint(1, 100), 100)
        alpha = gmpy2.mpq(random.randint(-10000, -1),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        with pytest.raises(ValueError):
            cfr.Uks(y, x, alpha, gamma)


def test_multi_uks_alpha_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        alpha = float(-100 * rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_wks_alpha_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        alpha = -100 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_multi_wks_alpha_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        alpha = float(-100 * rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma)


# negative gamma

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_uks_gamma_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        gamma = -100 * rng.random((1, ), 'float32')[0]
        alpha = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_uks_gamma_negative():
    for _ in range(10):
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        x = gmpy2.mpq(random.randint(1, 100), 100)
        gamma = gmpy2.mpq(random.randint(-10000, -1),
                          random.randint(1, 100))
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        with pytest.raises(ValueError):
            cfr.Uks(y, x, alpha, gamma)


def test_multi_uks_gamma_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        gamma = float(-100 * rng.random((1, ), 'float32')[0])
        alpha = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_wks_gamma_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        gamma = -100 * rng.random((1, ), 'float32')[0]
        alpha = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_multi_wks_gamma_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        gamma = float(-100 * rng.random((1, ), 'float32')[0])
        alpha = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma)


# negative element of y

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_vks_y_element_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        y[random.randrange(len(y))] *= -1
        x = rng.random((random.randint(2, 100), ), 'float32')
        with pytest.raises(ValueError):
            cf.Vks(y, x, try_libpmadra=try_libpmadra)


def test_rational_vks_y_element_negative():
    for _ in range(10):
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        y[random.randrange(len(y))] *= -1
        x = gmpy2.mpq(random.randint(1, 100), 100)
        with pytest.raises(ValueError):
            cfr.Vks(y, x)


def test_multi_vks_y_element_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        y[random.randrange(len(y))] *= -1
        x = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Vks(y, x)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_uks_y_element_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        y[random.randrange(len(y))] *= -1
        x = rng.random((random.randint(2, 100), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_uks_y_element_negative():
    for _ in range(10):
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        y[random.randrange(len(y))] *= -1
        x = gmpy2.mpq(random.randint(1, 100), 100)
        alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
        with pytest.raises(ValueError):
            cfr.Uks(y, x, alpha, gamma)


def test_multi_uks_y_element_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        y[random.randrange(len(y))] *= -1
        x = float(rng.random((1, ), 'float32')[0])
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_wks_y_element_negative(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        y[random.randrange(len(y))] *= -1
        x = rng.random((random.randint(2, 100), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_multi_wks_y_element_negative():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        y[random.randrange(len(y))] *= -1
        x = float(rng.random((1, ), 'float32')[0])
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma)


# x or element of x outside of [0, 1] range.

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_vks_x_element_outside_range(try_libpmadra):
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for value in vals:
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        x[random.randrange(len(x))] = value
        with pytest.raises(ValueError):
            cf.Vks(y, x, try_libpmadra=try_libpmadra)


def test_rational_vks_x_outside_range():
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for x in vals:
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        with pytest.raises(ValueError):
            cfr.Vks(y, x)


def test_multi_vks_x_outside_range():
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for x in vals:
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        with pytest.raises(ValueError):
            cfm.Vks(y, x)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_uks_x_element_outside_range(try_libpmadra):
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for value in vals:
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        x[random.randrange(len(x))] = value
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_uks_x_outside_range():
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for x in vals:
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
        with pytest.raises(ValueError):
            cfr.Uks(y, x, alpha, gamma)


def test_multi_uks_x_outside_range():
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for x in vals:
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_wks_x_element_outside_range(try_libpmadra):
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for value in vals:
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        x[random.randrange(len(x))] = value
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_multi_wks_x_outside_range():
    vals = numpy.hstack((-100 * rng.random((100, )),
                         1 + 100 * rng.random((100, ))))
    for x in vals:
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma)


# non-real alpha

@pytest.mark.parametrize('try_libpmadra,dtype,alpha',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_uks_alpha_nonnumber(try_libpmadra, dtype, alpha):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('alpha', non_real_params)
def test_rational_uks_alpha_nonnumber(alpha):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('alpha', non_real_params)
def test_multi_uks_alpha_nonnumber(alpha):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,dtype,alpha',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_wks_alpha_nonnumber(try_libpmadra, dtype, alpha):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('alpha', non_real_params)
def test_multi_wks_alpha_nonnumber(alpha):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma)


# non-real gamma

@pytest.mark.parametrize('try_libpmadra,dtype,gamma',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_uks_gamma_nonnumber(try_libpmadra, dtype, gamma):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('gamma', non_real_params)
def test_rational_uks_gamma_nonnumber(gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('gamma', non_real_params)
def test_multi_uks_gamma_nonnumber(gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,dtype,gamma',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_wks_gamma_nonnumber(try_libpmadra, dtype, gamma):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('gamma', non_real_params)
def test_multi_wks_gamma_nonnumber(gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    x = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma)


# non-real x

@pytest.mark.parametrize('x', non_real_params)
def test_rational_vks_x_nonnumber(x):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    with pytest.raises(TypeError):
        cfr.Vks(y, x)


@pytest.mark.parametrize('x', non_real_params)
def test_multi_vks_x_nonnumber(x):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    with pytest.raises(TypeError):
        cfm.Vks(y, x)


@pytest.mark.parametrize('x', non_real_params)
def test_rational_uks_x_nonnumber(x):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('x', non_real_params)
def test_multi_uks_x_nonnumber(x):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('x', non_real_params)
def test_multi_wks_x_nonnumber(x):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma)


# non-real element of x

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_vks_x_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x[random.randrange(len(x))] = element
    with pytest.raises((TypeError, ValueError)):
        cf.Vks(y, x, dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_uks_x_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x[random.randrange(len(x))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_wks_x_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32')
    x = rng.random((random.randint(2, 100), ), 'float32').tolist()
    x[random.randrange(len(x))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


# non-real element of y

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_vks_y_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = element
    x = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.Vks(y, x, dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_rational_vks_y_hasnonreal(element):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    y[random.randrange(len(y))] = element
    x = gmpy2.mpq(random.randint(1, 100), 100)
    with pytest.raises(TypeError):
        cfr.Vks(y, x)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_vks_y_hasnonreal(element):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    y[random.randrange(len(y))] = element
    x = gmpy2.mpq(random.randint(1, 100), 100)
    with pytest.raises(TypeError):
        cfm.Vks(y, x)


@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_uks_y_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = element
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_rational_uks_y_hasnonreal(element):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    y[random.randrange(len(y))] = element
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_uks_y_hasnonreal(element):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    y[random.randrange(len(y))] = element
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_wks_y_hasnonreal(try_libpmadra, dtype, element):
    y = rng.random((random.randint(2, 100), ), 'float32').tolist()
    y[random.randrange(len(y))] = element
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_wks_y_hasnonreal(element):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 40))]
    y[random.randrange(len(y))] = element
    x = gmpy2.mpq(random.randint(1, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma)


# non-Sequence y

@pytest.mark.parametrize('try_libpmadra,dtype,y',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_vks_y_nonseq(try_libpmadra, dtype, y):
    x = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(TypeError):
        cf.Vks(y, x, dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('y', non_seq_params)
def test_vks_rational_y_nonseq(y):
    x = gmpy2.mpq(random.randint(0, 100), 100)
    with pytest.raises(TypeError):
        cfr.Vks(y, x)


@pytest.mark.parametrize('y', non_seq_params)
def test_vks_multi_y_nonseq(y):
    x = gmpy2.mpq(random.randint(0, 100), 100)
    with pytest.raises(TypeError):
        cfm.Vks(y, x)


@pytest.mark.parametrize('try_libpmadra,dtype,y',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_uks_y_nonseq(try_libpmadra, dtype, y):
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('y', non_seq_params)
def test_uks_rational_y_nonseq(y):
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('y', non_seq_params)
def test_uks_multi_y_nonseq(y):
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra,dtype,y',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_wks_y_nonseq(try_libpmadra, dtype, y):
    x = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('y', non_seq_params)
def test_wks_multi_y_nonseq(y):
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma)


# non-Sequence x

@pytest.mark.parametrize('try_libpmadra,dtype,x',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_vks_x_nonseq(try_libpmadra, dtype, x):
    y = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(TypeError):
        cf.Vks(y, x, dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype,x',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_uks_x_nonseq(try_libpmadra, dtype, x):
    y = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype,x',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_wks_x_nonseq(try_libpmadra, dtype, x):
    y = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


# non-bool binom_from_gamma

@pytest.mark.parametrize('binom_from_gamma', non_bool_params)
def test_vks_multi_binom_from_gamma_nonbool(binom_from_gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    with pytest.raises(TypeError):
        cfm.Vks(y, x, binom_from_gamma=binom_from_gamma)


@pytest.mark.parametrize('binom_from_gamma', non_bool_params)
def test_uks_multi_binom_from_gamma_nonbool(binom_from_gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma, binom_from_gamma=binom_from_gamma)


@pytest.mark.parametrize('binom_from_gamma', non_bool_params)
def test_wks_multi_binom_from_gamma_nonbool(binom_from_gamma):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma, binom_from_gamma=binom_from_gamma)


# non-bool try_libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_vks_try_libpmadra_nonbool(try_libpmadra):
    y = rng.random((random.randint(1, 30), ), 'float32')
    x = rng.random((random.randint(1, 50), ), 'float32')
    with pytest.raises(TypeError):
        cf.Vks(y, x, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_uks_try_libpmadra_nonbool(try_libpmadra):
    y = rng.random((random.randint(1, 30), ), 'float32')
    x = rng.random((random.randint(1, 50), ), 'float32')
    alpha = rng.random((1, ))[0]
    gamma = rng.random((1, ))[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_wks_try_libpmadra_nonbool(try_libpmadra):
    y = rng.random((random.randint(1, 30), ), 'float32')
    x = rng.random((random.randint(1, 50), ), 'float32')
    alpha = rng.random((1, ))[0]
    gamma = rng.random((1, ))[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


# non-int precision and emax

@pytest.mark.parametrize('precision', non_int_params)
def test_vks_multi_precision_nonint(precision):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    with pytest.raises(TypeError):
        cfm.Vks(y, x, precision=precision)


@pytest.mark.parametrize('precision', non_int_params)
def test_uks_multi_precision_nonint(precision):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma, precision=precision)


@pytest.mark.parametrize('precision', non_int_params)
def test_wks_multi_precision_nonint(precision):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma, precision=precision)


@pytest.mark.parametrize('emax', non_int_params)
def test_vks_multi_emax_nonint(emax):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    with pytest.raises(TypeError):
        cfm.Vks(y, x, emax=emax)


@pytest.mark.parametrize('emax', non_int_params)
def test_uks_multi_emax_nonint(emax):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Uks(y, x, alpha, gamma, emax=emax)


@pytest.mark.parametrize('emax', non_int_params)
def test_wks_multi_emax_nonint(emax):
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.Wks(y, x, alpha, gamma, emax=emax)


# precison and emax too small

def test_vks_multi_precision_nonpositive():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    for precision in range(0, -10, -1):
        with pytest.raises(ValueError):
            cfm.Vks(y, x, precision=precision)
    for _ in range(1000):
        precision = random.randint(-2**63 + 1, 0)
        with pytest.raises(ValueError):
            cfm.Vks(y, x, precision=precision)


def test_uks_multi_precision_nonpositive():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for precision in range(0, -10, -1):
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma, precision=precision)
    for _ in range(1000):
        precision = random.randint(-2**63 + 1, 0)
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma, precision=precision)


def test_wks_multi_precision_nonpositive():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for precision in range(0, -10, -1):
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma, precision=precision)
    for _ in range(1000):
        precision = random.randint(-2**63 + 1, 0)
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma, precision=precision)


def test_vks_multi_emax_lessthantwo():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    for emax in range(1, -10, -1):
        with pytest.raises(ValueError):
            cfm.Vks(y, x, emax=emax)
    for _ in range(1000):
        emax = random.randint(-2**63 + 1, 1)
        with pytest.raises(ValueError):
            cfm.Vks(y, x, emax=emax)


def test_uks_multi_emax_lessthantwo():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for emax in range(1, -10, -1):
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma, emax=emax)
    for _ in range(1000):
        emax = random.randint(-2**63 + 1, 1)
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma, emax=emax)


def test_wks_multi_emax_lessthantwo():
    y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
         for _ in range(random.randint(1, 30))]
    x = gmpy2.mpq(random.randint(0, 100), 100)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for emax in range(1, -10, -1):
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma, emax=emax)
    for _ in range(1000):
        emax = random.randint(-2**63 + 1, 1)
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma, emax=emax)


# invalid dtype

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_vks_dtype_invalid(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.Vks(y, x, dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_uks_dtype_invalid(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_wks_dtype_invalid(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, dtype=dtype,
               try_libpmadra=try_libpmadra)


# complex y

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_vks_y_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    x = rng.random((random.randint(1, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.Vks(y, x, dtype='auto', try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_uks_y_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_wks_y_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


# complex x

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_vks_x_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    with pytest.raises(TypeError):
        cf.Vks(y, x, dtype='auto', try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_uks_x_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Uks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_wks_x_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.Wks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


# y has too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(True, i) for i in range(3, 6)]
                         + [(False, i) for i in range(2, 6)])
def test_vks_y_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.Vks(y, x, dtype='auto', try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(True, i) for i in range(3, 6)]
                         + [(False, i) for i in range(2, 6)])
def test_uks_y_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(True, i) for i in range(3, 6)]
                         + [(False, i) for i in range(2, 6)])
def test_wks_y_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    x = rng.random((random.randint(1, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


# x has too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i)
                          for v in (False, True)
                          for i in range(2, 6)])
def test_vks_x_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    with pytest.raises(ValueError):
        cf.Vks(y, x, dtype='auto', try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i)
                          for v in (False, True)
                          for i in range(2, 6)])
def test_uks_x_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Uks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i)
                          for v in (False, True)
                          for i in range(2, 6)])
def test_wks_x_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random((random.randint(1, 40), ), 'float32')
    x = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.Wks(y, x, alpha, gamma, dtype='auto',
               try_libpmadra=try_libpmadra)


# alpha and gamma are zero simultaneously.

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_uks_alpha_gamma_zero(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        gamma = 0
        alpha = 0
        with pytest.raises(ValueError):
            cf.Uks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_uks_alpha_gamma_zero():
    for _ in range(10):
        y = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
             for _ in range(random.randint(1, 30))]
        x = gmpy2.mpq(random.randint(1, 100), 100)
        gamma = 0
        alpha = 0
        with pytest.raises(ValueError):
            cfr.Uks(y, x, alpha, gamma)


def test_multi_uks_alpha_gamma_zero():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        gamma = 0
        alpha = 0
        with pytest.raises(ValueError):
            cfm.Uks(y, x, alpha, gamma)


@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_wks_alpha_gamma_zero(try_libpmadra):
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32')
        x = rng.random((random.randint(2, 100), ), 'float32')
        gamma = 0
        alpha = 0
        with pytest.raises(ValueError):
            cf.Wks(y, x, alpha, gamma, try_libpmadra=try_libpmadra)


def test_multi_wks_alpha_gamma_zero():
    for _ in range(10):
        y = rng.random((random.randint(2, 100), ), 'float32').tolist()
        x = float(rng.random((1, ), 'float32')[0])
        gamma = 0
        alpha = 0
        with pytest.raises(ValueError):
            cfm.Wks(y, x, alpha, gamma)


# If x = 1, then vks should just equal y.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(False, v) for v in dtypes_all]
                         + [(True, v) for v in dtypes_all])
def test_vks_x_one(try_libpmadra, dtype):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dtype not in ('float32', 'float64'):
            y = tp(rng.random((Mc, ), 'float64'))
        else:
            y = rng.random((Mc, ), dtype)
        vks = cf.Vks(numpy.atleast_1d(y), numpy.uint8([1]), dtype=dtype,
                     try_libpmadra=try_libpmadra)
        assert vks.shape == (Mc, 1)
        if dtype != 'auto':
            assert vks.dtype == numpy.dtype(dtype)
        numpy.testing.assert_equal(vks[:, 0], y)


def test_vks_x_one_rational():
    for _ in range(10):
        Mc = random.randint(1, 40)
        y = [gmpy2.mpq(random.randint(0, 100), random.randint(1, 100))
             for _ in range(Mc)]
        vks = cfr.Vks(y, 1)
        assert y == vks


@pytest.mark.parametrize('precision,emax,binom_from_gamma',
                         [(p, e, b) for p in (2, 8, 16, 30, 64, 128, 280)
                          for e in (128, 256, 1024, 2000)
                          for b in (False, True)])
def test_vks_x_one_multi(precision, emax, binom_from_gamma):
    for _ in range(10):
        Mc = random.randint(1, 40)
        with gmpy2.local_context(gmpy2.context(), precision=precision,
                                 emax=emax, emin=1 - emax) as ctx:
            y = [gmpy2.mpfr_random(gmpy2rng) for _ in range(Mc)]
        vks = cfm.Vks(y, 1, binom_from_gamma=binom_from_gamma,
                      precision=precision, emax=emax)
        assert y == vks


# If x = 1, then wks should be 0 and uks == uks_one

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(False, v) for v in dtypes_all]
                         + [(True, v) for v in dtypes_all])
def test_wks_x_one(try_libpmadra, dtype):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dtype not in ('float32', 'float64'):
            y = tp(rng.random((Mc, ), 'float64'))
            alpha = tp(rng.random((1, ), 'float64')[0])
            gamma = tp(rng.random((1, ), 'float64')[0])
        else:
            y = rng.random((Mc, ), dtype)
            alpha = rng.random((1, ), dtype)[0]
            gamma = rng.random((1, ), dtype)[0]
        wks, uks, _, uks_one = cf.Wks(
            numpy.atleast_1d(y), numpy.uint8([1]), alpha, gamma,
            dtype=dtype, try_libpmadra=try_libpmadra)
        assert wks.shape == (Mc, 1)
        assert wks.dtype == numpy.dtype(tp)
        numpy.testing.assert_equal(uks[:, 0], uks_one)
        numpy.testing.assert_equal(wks, 0)


@pytest.mark.parametrize('precision,emax,binom_from_gamma',
                         [(p, e, b) for p in (2, 8, 16, 30, 64, 128, 280)
                          for e in (128, 256, 1024, 2000)
                          for b in (False, True)])
def test_wks_x_one_multi(precision, emax, binom_from_gamma):
    for _ in range(10):
        Mc = random.randint(1, 40)
        with gmpy2.local_context(gmpy2.context(), precision=precision,
                                 emax=emax, emin=1 - emax) as ctx:
            y = [gmpy2.mpfr_random(gmpy2rng) for _ in range(Mc)]
            alpha = gmpy2.mpfr_random(gmpy2rng)
            gamma = gmpy2.mpfr_random(gmpy2rng)
            wks, uks, _, uks_one = cfm.Wks(
                y, 1, alpha, gamma,
                binom_from_gamma=binom_from_gamma,
                precision=precision, emax=emax)
        assert uks == uks_one
        assert all([0 == v for v in wks])


# If gamma = 0, then Uk = -Vk / alpha and Wk = 0.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d) for t in (True, False)
                          for d in dtypes_all])
def test_uks_wks_gamma_zero(try_libpmadra, dtype):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        y = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        x = numpy.atleast_1d(tp(rng.random((random.randint(1, 100), ),
                                           'float64')))
        alpha = tp(1 + rng.random((1, ), 'float64')[0])
        gamma = tp(0)
        wks, uks, vks, _ = cf.Wks(
            y, x, alpha, gamma,
            dtype=dtype, try_libpmadra=try_libpmadra)
        assert_allclose(-vks / alpha,
                        uks, atol=1e-4, rtol=1e-4)
        numpy.testing.assert_equal(wks, 0)


def test_rational_uks_gamma_zero():
    for _ in range(10):
        Mc = random.randint(2, 100)
        y = [gmpy2.mpq(random.randint(0, 100), random.randint(1, 100))
             for _ in range(Mc)]
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        x = gmpy2.mpq(random.randint(1, 100), 100)
        gamma = 0
        uks, vks = cfr.Uks(y, x, alpha, gamma)
        assert uks == [-v / alpha for v in vks]


@pytest.mark.parametrize('binom_from_gamma,precision,emax',
                         [(t, p, e) for t in (True, False)
                          for p in (33, 49, 102)
                          for e in (200, 400, 501)])
def test_multi_uks_wks_gamma_zero(binom_from_gamma, precision, emax):
    for _ in range(10):
        Mc = random.randint(2, 100)
        y = rng.random((Mc, ), 'float64').tolist()
        x = rng.random((1, ), 'float64')[0]
        alpha = 1 + rng.random((1, ), 'float64')[0]
        gamma = 0
        wks, uks, vks, _ = cfm.Wks(
            y, x, alpha, gamma,
            binom_from_gamma=binom_from_gamma, precision=precision,
            emax=emax)
        assert_allclose(-numpy.longdouble(vks) / alpha,
                        numpy.longdouble(uks), atol=1e-6, rtol=1e-6)
        assert wks == Mc * [0]


# Do a cross comparison between all functions to calculate Wk, Uk, and
# Vk.
def test_compare_all_wks_vks_uks():
    for _ in range(10):
        Mc = random.randint(1, 30)
        y = [gmpy2.mpq(random.randint(0, 100), random.randint(1, 100))
             for _ in range(Mc)]
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        # Make random x values between 0 and 1 and then sort them. We
        # just make random rationals and invert them if they are greater
        # than 1.
        x = [gmpy2.mpq(random.randint(0, 100), random.randint(1, 100))
             for _ in range(10)]
        for i in range(len(x)):
            if x[i] > 1:
                x[i] = gmpy2.div(1, x[i])
        x.sort()
        # Get the wks, uks, and vks in masse from const_coeff.Wks for
        # all dtypes with and without libpmadra and check that they
        # agree with each other. Also check that the values are
        # decreasing with increasing x.
        by_p = {v: cf.Wks(y, x, numpy.dtype(v).type(alpha),
                          numpy.dtype(v).type(gamma), dtype=v,
                          try_libpmadra=False)
               for v in dtypes}
        by_f = {v: cf.Wks(y, x, numpy.dtype(v).type(alpha),
                          numpy.dtype(v).type(gamma), dtype=v,
                          try_libpmadra=True)
                for v in dtypes}
        for k in dtypes:
            wks_p, uks_p, vks_p, uks_one_p = by_p[k]
            wks_f, uks_f, vks_f, uks_one_f = by_f[k]
            # Check that Uks and Vks give the same answers for uks and
            # vks.
            assert_array_equal(vks_p, cf.Vks(y, x, dtype=k,
                                             try_libpmadra=False))
            assert_array_equal(vks_f, cf.Vks(y, x, dtype=k,
                                             try_libpmadra=True))
            tp = numpy.dtype(k).type
            uks_t, vks_t = cf.Uks(y, x, tp(alpha), tp(gamma), dtype=k,
                                  try_libpmadra=False)
            assert_array_equal(uks_p, uks_t)
            assert_array_equal(vks_p, vks_t)
            uks_t, vks_t = cf.Uks(y, x, tp(alpha), tp(gamma), dtype=k,
                                  try_libpmadra=True)
            assert_array_equal(uks_f, uks_t)
            assert_array_equal(vks_f, vks_t)
            # Check shape.
            assert wks_p.shape == (Mc, len(x))
            assert wks_f.shape == (Mc, len(x))
            assert uks_one_p.shape == (Mc, )
            assert uks_one_f.shape == (Mc, )
            assert uks_p.shape == wks_p.shape
            assert vks_p.shape == wks_p.shape
            assert uks_f.shape == wks_f.shape
            assert vks_f.shape == wks_f.shape
            # Check dtype
            assert vks_p.dtype == numpy.dtype(k)
            assert vks_f.dtype == numpy.dtype(k)
            assert uks_p.dtype == numpy.dtype(k)
            assert uks_f.dtype == numpy.dtype(k)
            assert wks_p.dtype == numpy.dtype(k)
            assert wks_f.dtype == numpy.dtype(k)
            assert uks_one_p.dtype == numpy.dtype(k)
            assert uks_one_f.dtype == numpy.dtype(k)
            # Check that the values are close.
            assert_allclose(vks_p,
                            vks_f,
                            rtol=1e-4, atol=1e-6)
            assert_allclose(uks_p,
                            uks_f,
                            rtol=1e-4, atol=1e-6)
            assert_allclose(wks_p,
                            wks_f,
                            rtol=1e-4, atol=1e-6)
            assert_allclose(uks_one_p,
                            uks_one_f,
                            rtol=1e-4, atol=1e-6)
            if k != 'longdouble':
                assert_allclose(
                    wks_f,
                    by_f['longdouble'][0],
                    rtol=1e-4, atol=1e-6)
                assert_allclose(
                    uks_f,
                    by_f['longdouble'][1],
                    rtol=1e-4, atol=1e-6)
                assert_allclose(
                    vks_f,
                    by_f['longdouble'][2],
                    rtol=1e-4, atol=1e-6)
                assert_allclose(
                    uks_one_f,
                    by_f['longdouble'][3],
                    rtol=1e-4, atol=1e-6)
            # Check that each row of vks is non-increasing, uks is
            # non-decreasing, and wks is non-increasing.
            assert numpy.all(numpy.diff(vks_f,
                                        axis=vks_f.ndim - 1) <= 0)
            assert numpy.all(numpy.diff(uks_f,
                                        axis=uks_f.ndim - 1) >= 0)
            assert numpy.all(numpy.diff(wks_f,
                                        axis=wks_f.ndim - 1) <= 0)
            # Check that vks are all non-negative, uks are all
            # non-positive, and wks are all non-negative.
            assert numpy.all(vks_p >= 0.0)
            assert numpy.all(vks_f >= 0.0)
            assert numpy.all(uks_p <= 0.0)
            assert numpy.all(uks_f <= 0.0)
            assert numpy.all(wks_p >= 0.0)
            assert numpy.all(wks_f >= 0.0)
            assert numpy.all(uks_one_p <= 0.0)
            assert numpy.all(uks_one_f <= 0.0)
        # Go through each element of x and check that the longdouble
        # solution and the multi precision solution match with the
        # rational solution.
        wks_p, uks_p, vks_p, uks_one_p = by_p['longdouble']
        for i, v in enumerate(x):
            uks_r, vks_r = cfr.Uks(y, v, alpha, gamma)
            assert isinstance(uks_r, list)
            assert isinstance(vks_r, list)
            # Check that calling Vks directly gives the same answer.
            assert vks_r == cfr.Vks(y, v)
            # Check that vks are all non-negative and uks are all non-positive.
            assert all([v >= 0 for v in vks_r])
            assert all([v <= 0 for v in uks_r])
            # Do the comparisons.
            uks_r_ld = numpy.longdouble(uks_r)
            vks_r_ld = numpy.longdouble(vks_r)
            assert_allclose(uks_p[:, i], uks_r_ld, rtol=1e-8, atol=1e-8)
            assert_allclose(vks_p[:, i], vks_r_ld, rtol=1e-8, atol=1e-8)
            # Do the multi-precision comparisons.
            for precision in (40, 60, 200):
                for emax in (128, 800):
                    for binom_from_gamma in (False, True):
                        wks_m, uks_m, vks_m, uks_one_m = cfm.Wks(
                            y, v, alpha, gamma,
                            binom_from_gamma=binom_from_gamma,
                            precision=precision, emax=emax)
                        assert isinstance(wks_m, list)
                        assert isinstance(uks_m, list)
                        assert isinstance(vks_m, list)
                        assert isinstance(uks_one_m, list)
                        assert_allclose(numpy.longdouble(uks_m),
                                        uks_r_ld, rtol=1e-8, atol=1e-8)
                        assert_allclose(numpy.longdouble(vks_m),
                                        vks_r_ld, rtol=1e-8, atol=1e-8)
                        assert_allclose(numpy.longdouble(wks_m),
                                        wks_p[:, i],
                                        rtol=1e-8, atol=1e-8)
                        assert_allclose(numpy.longdouble(uks_one_m),
                                        uks_one_p,
                                        rtol=1e-8, atol=1e-8)
                        # Check that Uks and Vks give the same uks and
                        # vks.
                        assert vks_m == cfm.Vks(
                            y, v, binom_from_gamma=binom_from_gamma,
                            precision=precision, emax=emax)
                        uks_t, vks_t = cfm.Uks(
                            y, v, alpha, gamma,
                            binom_from_gamma=binom_from_gamma,
                            precision=precision, emax=emax)
                        assert uks_m == uks_t
                        assert vks_m == vks_t
                        # Check that vks are all non-negative, uks are
                        # all non-positive, and wks are all
                        # non-negative.
                        assert all([v >= 0.0 for v in vks_m])
                        assert all([v <= 0.0 for v in uks_m])
                        assert all([v >= 0.0 for v in wks_m])
                        assert all([v <= 0.0 for v in uks_one_m])
