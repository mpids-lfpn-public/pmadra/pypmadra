# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random

import pytest

from pypmadra import fold_Mc
from pypmadra.const_coeff import required_Mc_folded, \
    required_floating_point_format


# Initialize the random generators.
random.seed()
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')

# Possible desired_dtypes
desired_dtypes = ('float32', 'float64', 'longdouble', 'quad')

# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_real_params = (complex(1, 2), complex(1, 0), ..., slice(4),
                   [], (), set(), dict())
non_dtype_params = (None, True, False, 101, -31.8, complex(3, 2),
                    numpy.int8(1), numpy.float64(-3), ..., slice(4),
                    set(), frozenset(), dict(), b'float32', b'quad',
                    'a93akva', '391', numpy.dtype('float32'))


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# Invalid desired_dtype

@pytest.mark.parametrize('try_libpmadra,desired_dtype',
                         [(t, v) for t in (False, True)
                          for v in non_dtype_params])
def test_try_desired_dtype_invalid(try_libpmadra, desired_dtype):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# non-real alpha, gamma, emax_safety_factor

@pytest.mark.parametrize('try_libpmadra,alpha',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_alpha_nonnumber(try_libpmadra, alpha):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,gamma',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_gamma_nonnumber(try_libpmadra, gamma):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,emax_safety_factor',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_emax_safety_factor_nonnumber(try_libpmadra,
                                      emax_safety_factor):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           emax_safety_factor=emax_safety_factor,
                           try_libpmadra=try_libpmadra)


# non-Sequence n0 and beta

@pytest.mark.parametrize('try_libpmadra,n0',
                         [(t, b)
                          for t in (True, False)
                          for b in non_seq_params])
def test_n0_nonseq(try_libpmadra, n0):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,beta',
                         [(t, b)
                          for t in (True, False)
                          for b in non_seq_params])
def test_beta_nonseq(try_libpmadra, beta):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(TypeError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)

# non-real element of n0 and beta

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,element',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_n0_hasnonreal(try_libpmadra, element):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    n0 = rng.random((len(beta), ), 'float32').tolist()
    n0[random.randrange(len(n0))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises((TypeError, ValueError)):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,element',
                         [(t, v)
                          for t in (False, True)
                          for v in non_real_params])
def test_beta_hasnonreal(try_libpmadra, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32').tolist()
    beta[random.randrange(len(beta))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises((TypeError, ValueError)):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# NaN or negative element of n0 and beta

@pytest.mark.parametrize('param,try_libpmadra,kind',
                         [(p, t, k)
                          for p in ('n0', 'beta')
                          for t in (False, True)
                          for k in ('nan', '-inf',
                                    'negative')])
def test_n0_beta_nannegative(param, try_libpmadra, kind):
    beta_base = rng.random((random.randint(2, 100), ), 'float32')
    n0_base = rng.random((len(beta_base), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    for _ in range(10):
        if kind in ('nan', 'inf', '-inf'):
            element = float(kind)
        else:
            element = -1e4 * rng.random((1, ), 'float32')[0]
        if param == 'n0':
            n0 = n0_base.copy()
            n0[random.randrange(len(n0))] = element
            beta = beta_base
        else:
            n0 = n0_base
            beta = beta_base.copy()
            beta[random.randrange(len(n0))] = element
    with pytest.raises(ValueError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# unsupported dtype of n0 and beta

@pytest.mark.parametrize('try_libpmadra,tp',
                         [(t, v)
                          for t in (False, True)
                          for v in (numpy.complex64,
                                    numpy.complex128,
                                    numpy.object_)])
def test_n0_unsupported_dtype(try_libpmadra, tp):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    n0 = tp(rng.random((len(beta), ), 'float32'))
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises((TypeError, ValueError)):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,tp',
                         [(t, v)
                          for t in (False, True)
                          for v in (numpy.complex64,
                                    numpy.complex128,
                                    numpy.object_)])
def test_beta_unsupported_dtype(try_libpmadra, tp):
    beta = tp(rng.random((random.randint(2, 100), ), 'float32'))
    n0 = rng.random((len(beta), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises((TypeError, ValueError)):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# n0 and beta have too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i) for v in (True, False)
                          for i in range(2, 6)])
def test_n0_ndim_toohigh(try_libpmadra, ndim):
    n0 = rng.random([random.randint(1, 3) for i in range(ndim)],
                    'float32')
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(ValueError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(v, i) for v in (True, False)
                          for i in range(2, 6)])
def test_beta_ndim_toohigh(try_libpmadra, ndim):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random([random.randint(1, 3) for i in range(ndim)],
                      'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(ValueError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           try_libpmadra=try_libpmadra)


# n0 and beta's lengths don't match

@pytest.mark.parametrize('try_libpmadra', [True, False])
def test_n0_beta_lengthsdontmatch(try_libpmadra):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        length = random.randint(2, 100)
        while length == len(n0):
            length = random.randint(2, 100)
        beta = rng.random((length, ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        desired_dtype = random.choice(desired_dtypes)
        with pytest.raises(ValueError):
            required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                               try_libpmadra=try_libpmadra)


# NaN and -inf alpha, gamma, emax_safety_factor

@pytest.mark.parametrize('param,try_libpmadra,value',
                         [(p, t, v)
                          for p in ('alpha', 'gamma',
                                    'emax_safety_factor')
                          for t in (False, True)
                          for v in (numpy.nan, -numpy.inf)])
def test_scalar_arg_nanorinf(param, try_libpmadra, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    if param == 'alpha':
        alpha = value
    else:
        alpha = rng.random((1, ), 'float32')[0]
    if param == 'gamma':
        gamma = value
    else:
        gamma = rng.random((1, ), 'float32')[0]
    if param == 'emax_safety_factor':
        emax_safety_factor = value
    else:
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
    desired_dtype = random.choice(desired_dtypes)
    with pytest.raises(ValueError):
        required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                           emax_safety_factor=emax_safety_factor,
                           try_libpmadra=try_libpmadra)


# negative alpha, gamma, and emax_safety_factor

@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_alpha_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = -1000 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        desired_dtype = random.choice(desired_dtypes)
        with pytest.raises(ValueError):
            required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                               emax_safety_factor=emax_safety_factor,
                               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_gamma_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = -1000 * rng.random((1, ), 'float32')[0]
        emax_safety_factor = 10 * rng.random((1, ), 'float32')[0]
        desired_dtype = random.choice(desired_dtypes)
        with pytest.raises(ValueError):
            required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                               emax_safety_factor=emax_safety_factor,
                               try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra', [False, True])
def test_emax_safety_factor_negative(try_libpmadra):
    for _ in range(100):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = -1000 * rng.random((1, ), 'float32')[0]
        desired_dtype = random.choice(desired_dtypes)
        with pytest.raises(ValueError):
            required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                               emax_safety_factor=emax_safety_factor,
                               try_libpmadra=try_libpmadra)


# Check for random inputs that the returned Mc folded is indeed correct
# (the required format is no greater than the desired format and adding
# 1 to the folded Mc either exceeds Mc or results in a required format
# larger than the desired).

@pytest.mark.parametrize('try_libpmadra,desired_dtype,alpha,gamma',
                         [(t, v, a, g) for t in (False, True)
                          for v in desired_dtypes
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if a == 'positive' or g == 'positive'])
def test_Mc_folded_correct(try_libpmadra, desired_dtype, alpha, gamma):
    for i in range(100):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = random.randint(1, 20000)
        n0 = 10 * numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = 10 * numpy.atleast_1d(tp(rng.random(n0.shape,
                                                   'float64')))
        if alpha == 'zero':
            alph = tp(0)
        else:
            alph = 10 * tp(rng.random((1, ), 'float64')[0])
        if gamma == 'zero':
            gamm = tp(0)
        else:
            gamm = 10 * tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        Mc_folded = required_Mc_folded(
            desired_dtype, n0, beta, alph, gamm,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)
        # Handle the different cases. If Mc_folded is None, then folding
        # is insufficient. Otherwise, we can check.
        if Mc_folded is None:
            n0_folded, _ = fold_Mc(n0, 1, dtype=dtype,
                                   try_libpmadra=try_libpmadra)
            beta_folded, _ = fold_Mc(beta, 1, dtype=dtype,
                                     try_libpmadra=try_libpmadra)
            _, fmt = required_floating_point_format(
                n0_folded, beta_folded, alph, gamm,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert fmt is None
        else:
            # Make sure Mc_folded is in the range.
            assert Mc_folded >= 1
            assert Mc_folded <= Mc
            # Check that Mc_folded gives a format that is no larger than
            # the desired.
            n0_folded, _ = fold_Mc(n0, Mc_folded, dtype=dtype,
                                   try_libpmadra=try_libpmadra)
            beta_folded, _ = fold_Mc(beta, Mc_folded, dtype=dtype,
                                     try_libpmadra=try_libpmadra)
            _, fmt = required_floating_point_format(
                n0_folded, beta_folded, alph, gamm,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            assert desired_dtypes.index(fmt) \
                    <= desired_dtypes.index(desired_dtype)
            # If Mc_folded < Mc, check that Mc_folded + 1 gives a larger
            # format than desired.
            if Mc_folded < Mc:
                n0_folded, _ = fold_Mc(n0, Mc_folded + 1, dtype=dtype,
                                       try_libpmadra=try_libpmadra)
                beta_folded, _ = fold_Mc(beta, Mc_folded + 1,
                                         dtype=dtype,
                                         try_libpmadra=try_libpmadra)
                _, fmt = required_floating_point_format(
                    n0_folded, beta_folded, alph, gamm,
                    emax_safety_factor=emax_safety_factor,
                    try_libpmadra=try_libpmadra)
                assert fmt != desired_dtype
                if fmt is not None:
                    assert desired_dtypes.index(fmt) \
                        > desired_dtypes.index(desired_dtype)


# Check that as emax_safety_factor increases, Mc_folded is
# non-increasing.

@pytest.mark.parametrize('try_libpmadra,desired_dtype',
                         [(t, v) for t in (False, True)
                          for v in desired_dtypes])
def test_ordering_increasing_emax_safety_factor(try_libpmadra,
                                                desired_dtype):
    for i in range(10):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3))
        n0 = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = numpy.atleast_1d(tp(rng.random(n0.shape, 'float64')))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(rng.random((1, ), 'float64')[0])
        last_Mc_folded = None
        for emax_safety_factor in numpy.logspace(-1, 5, 30):
            Mc_folded = required_Mc_folded(
                desired_dtype, n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            if Mc_folded is None:
                break
            if last_Mc_folded is not None:
                assert Mc_folded <= last_Mc_folded
            last_Mc_folded = Mc_folded


# Check that as gamma <= 1 decreases, Mc_folded is non-increasing.

@pytest.mark.parametrize('try_libpmadra,desired_dtype',
                         [(t, v) for t in (False, True)
                          for v in desired_dtypes])
def test_ordering_decreasing_gamma(try_libpmadra, desired_dtype):
    for i in range(10):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3))
        n0 = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = numpy.atleast_1d(tp(rng.random(n0.shape, 'float64')))
        alpha = tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        last_Mc_folded = None
        for gamma in numpy.logspace(0, (1 - 1e-6) * numpy.log10(
                numpy.finfo(dtype).max**-1),
                30, dtype=dtype):
            Mc_folded = required_Mc_folded(
                desired_dtype, n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            if Mc_folded is None:
                break
            if last_Mc_folded is not None:
                assert Mc_folded <= last_Mc_folded
            last_Mc_folded = Mc_folded


# Check that as largest element of n0 and beta increases, Mc_folded is
# non-increasing.

@pytest.mark.parametrize('try_libpmadra,desired_dtype,param',
                         [(t, v, p) for t in (False, True)
                          for v in desired_dtypes
                          for p in ('n0', 'beta')])
def test_ordering_increasing_n0_beta_max(try_libpmadra, desired_dtype,
                                         param):
    for i in range(10):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3))
        n0 = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = numpy.atleast_1d(tp(rng.random(n0.shape, 'float64')))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        index_to_change = random.randrange(Mc)
        if param == 'n0':
            param_to_change = n0
        else:
            param_to_change = beta
        last_Mc_folded = None
        for value in numpy.logspace(0, (1.0 - 1e-6) * numpy.log10(
                numpy.finfo(dtype).max), 30, dtype=dtype):
            param_to_change[index_to_change] = value
            Mc_folded = required_Mc_folded(
                desired_dtype, n0, beta, alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            if Mc_folded is None:
                break
            if last_Mc_folded is not None:
                assert Mc_folded <= last_Mc_folded
            last_Mc_folded = Mc_folded


# Check that as Mc increases, Mc_folded increases at first till it is
# less than Mc after which it is non-increasing as the total values put
# into the top entry of the folded n0 and beta get progressively larger
# and larger.

@pytest.mark.parametrize('try_libpmadra,desired_dtype',
                         [(t, v) for t in (False, True)
                          for v in desired_dtypes])
def test_ordering_increasing_Mc(try_libpmadra, desired_dtype):
    for i in range(6):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        n0 = tp(1e6 * rng.random((1000, ), 'float64'))
        beta = tp(1e6 * rng.random(n0.shape, 'float64'))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        last_Mc_folded = None
        for Mc in range(len(n0)):
            Mc_folded = required_Mc_folded(
                desired_dtype, n0[:Mc], beta[:Mc], alpha, gamma,
                emax_safety_factor=emax_safety_factor,
                try_libpmadra=try_libpmadra)
            if Mc_folded is None:
                break
            assert Mc_folded <= Mc
            if last_Mc_folded is not None and last_Mc_folded < Mc - 1:
                assert Mc_folded <= last_Mc_folded
            last_Mc_folded = Mc_folded


# Check gamma = 0

@pytest.mark.parametrize('desired_dtype', desired_dtypes)
def test_gamma_zero(desired_dtype):
    for i in range(100):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = max(1, int(2e4 * rng.random((1, ), 'float64')[0]**3))
        n0 = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = numpy.atleast_1d(tp(rng.random(n0.shape, 'float64')))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(0)
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        Mc_folded_p = required_Mc_folded(
            desired_dtype, n0[:Mc], beta[:Mc], alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=False)
        Mc_folded_f = required_Mc_folded(
            desired_dtype, n0[:Mc], beta[:Mc], alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=True)
        if Mc_folded_p is None:
            assert Mc_folded_f is None
        else:
            assert abs(Mc_folded_p - Mc_folded_f) <= 1
            assert Mc_folded_p >= 1
            assert Mc_folded_p <= Mc


# Check +inf element of n0 and beta.

@pytest.mark.parametrize('try_libpmadra,desired_dtype,param',
                         [(t, d, p) for t in (False, True)
                          for d in desired_dtypes
                          for p in ('n0', 'beta')])
def test_n0_or_beta_has_inf(try_libpmadra, desired_dtype, param):
    for i in range(10):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = random.randint(1, 1000)
        n0 = numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = numpy.atleast_1d(tp(rng.random(n0.shape, 'float64')))
        alpha = tp(rng.random((1, ), 'float64')[0])
        gamma = tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        if param == 'n0':
            n0[random.randrange(Mc)] = numpy.inf
        else:
            beta[random.randrange(Mc)] = numpy.inf
        Mc_folded = required_Mc_folded(
            desired_dtype, n0, beta[:Mc], alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)
        assert Mc_folded is None


# Compare libpmadra and python versions.

@pytest.mark.parametrize('desired_dtype,alpha,gamma',
                         [(v, a, g)
                          for v in desired_dtypes
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if a == 'positive' or g == 'positive'])
def test_compare(desired_dtype, alpha, gamma):
    for i in range(100):
        dtype = ('float32', 'float64', 'longdouble')[i % 3]
        tp = numpy.dtype(dtype).type
        Mc = random.randint(1, 20000)
        n0 = 10 * numpy.atleast_1d(tp(rng.random((Mc, ), 'float64')))
        beta = 10 * numpy.atleast_1d(tp(rng.random(n0.shape,
                                                   'float64')))
        if alpha == 'zero':
            alph = tp(0)
        else:
            alph = 10 * tp(rng.random((1, ), 'float64')[0])
        if gamma == 'zero':
            gamm = tp(0)
        else:
            gamm = 10 * tp(rng.random((1, ), 'float64')[0])
        emax_safety_factor = 10 * tp(rng.random((1, ), 'float64')[0])
        Mc_folded_p = required_Mc_folded(
            desired_dtype, n0, beta, alph, gamm,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=False)
        Mc_folded_f = required_Mc_folded(
            desired_dtype, n0, beta, alph, gamm,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=True)
        if Mc_folded_f is None:
            assert Mc_folded_p is None
        elif Mc_folded_p is None:
            assert Mc_folded_f is None
        else:
            assert abs(Mc_folded_p - Mc_folded_f) <= 2
