# Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose, assert_array_less, \
    assert_array_equal

import scipy.integrate

import gmpy2

import pytest

import pypmadra.const_coeff as cf
import pypmadra.const_coeff.multi as cfm



# Initialize the random generators.
random.seed()
gmpy2rng = gmpy2.random_state(random.randrange(2**32))
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')
dtypes_all = ('auto', 'float32', 'float64', 'longdouble', 'quad')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_int_params = (-1.3, None, ..., slice(9), [], (), set(), dict(),
                  gmpy2.mpq(3, 2), numpy.float64(-3),
                  numpy.int32((3, 1)))
non_real_params = (complex(1, 2), complex(1, 0), None, ..., slice(4),
                   [], (), set(), dict())
non_real_params_numpy = (complex(1, 2), complex(1, 0), ..., slice(4),
                         [], (), set(), dict())


# gmpy2 is missing.

def test_multi_gmpy2_missing():
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    old_value = cfm._has_gmpy2
    try:
        cfm._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t)
    except:
        raise
    finally:
        cfm._has_gmpy2 = old_value


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            try_libpmadra=try_libpmadra)


# non-bool binom_from_gamma

@pytest.mark.parametrize('binom_from_gamma', non_bool_params)
def test_multi_binom_from_gamma_nonbool(binom_from_gamma):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t,
                             binom_from_gamma=binom_from_gamma)


# non-int precision and emax

@pytest.mark.parametrize('precision', non_int_params)
def test_multi_precision_nonint(precision):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t,
                             precision=precision)


@pytest.mark.parametrize('emax', non_int_params)
def test_multi_emax_nonint(emax):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t,
                             emax=emax)


# precision and emax too small

def test_multi_precision_nonpositive():
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    for precision in range(0, -10, -1):
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t,
                                 precision=precision)
    for _ in range(1000):
        precision = random.randint(-2**63 + 1, 0)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t,
                                 precision=precision)


def test_multi_emax_lessthantwo():
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    for emax in range(1, -10, -1):
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t,
                                 emax=emax)
    for _ in range(1000):
        emax = random.randint(-2**63 + 1, 1)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t,
                                 emax=emax)


# invalid dtype

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object',
                                    'quad')
                          if not v or k != 'quad'])
def test_dtype_invalid(try_libpmadra, dtype):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            dtype=dtype,
                            try_libpmadra=try_libpmadra)


# non-real alpha

@pytest.mark.parametrize('try_libpmadra,dtype,alpha',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_alpha_nonnumber(try_libpmadra, dtype, alpha):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('alpha', non_real_params)
def test_multi_alpha_nonnumber(alpha):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-real gamma

@pytest.mark.parametrize('try_libpmadra,dtype,gamma',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_gamma_nonnumber(try_libpmadra, dtype, gamma):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('gamma', non_real_params)
def test_multi_gamma_nonnumber(gamma):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-real t (multi only)

@pytest.mark.parametrize('t', non_real_params)
def test_multi_t_nonnumber(t):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-real t0

@pytest.mark.parametrize('try_libpmadra,dtype,t0',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_t0_nonnumber(try_libpmadra, dtype, t0):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            t0=t0, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('t0', non_real_params)
def test_multi_t0_nonnumber(t0):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t, t0=t0)


# non-real emax_safety_factor

@pytest.mark.parametrize('try_libpmadra,dtype,emax_safety_factor',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_emax_safety_factor_nonnumber(try_libpmadra, dtype,
                                      emax_safety_factor):
    n0 = rng.random((random.randint(2, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t,
                            emax_safety_factor=emax_safety_factor,
                            dtype=dtype, try_libpmadra=try_libpmadra)


# non-Sequence n0.

@pytest.mark.parametrize('try_libpmadra,dtype,n0',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_seq_params
                          if t or d != 'quad'])
def test_n0_nonseq(try_libpmadra, dtype, n0):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('n0', non_seq_params)
def test_multi_n0_nonseq(n0):
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-Sequence intn0.

@pytest.mark.parametrize('try_libpmadra,dtype,intn0',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_seq_params
                          if v is not None and (t or d != 'quad')])
def test_intn0_nonseq(try_libpmadra, dtype, intn0):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0,
                            dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('intn0',
                         [v for v in non_seq_params if v is not None])
def test_multi_intn0_nonseq(intn0):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0)


# non-Sequence beta.

@pytest.mark.parametrize('try_libpmadra,dtype,beta',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_seq_params
                          if t or d != 'quad'])
def test_beta_nonseq(try_libpmadra, dtype, beta):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(TypeError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('beta', non_seq_params)
def test_multi_beta_nonseq(beta):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# n0, intn0, or beta has too many axes.

@pytest.mark.parametrize('try_libpmadra,dtype,ndim',
                         [(t, d, i) for t in (True, False)
                          for d in dtypes_all
                          for i in range(2, 6)])
def test_n0_ndim_toohigh(try_libpmadra, dtype, ndim):
    n0 = rng.random([random.randint(1, 3) for i in range(ndim)],
                    'float32')
    beta = rng.random((n0.shape[0], ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype,ndim',
                         [(t, d, i) for t in (True, False)
                          for d in dtypes_all
                          for i in range(2, 6)])
def test_intn0_ndim_toohigh(try_libpmadra, dtype, ndim):
    intn0 = rng.random([random.randint(1, 3) for i in range(ndim)],
                         'float32')
    n0 = rng.random((intn0.shape[0], ), 'float32')
    beta = rng.random((intn0.shape[0], ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0,
                            dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype,ndim',
                         [(t, d, i) for t in (True, False)
                          for d in dtypes_all
                          for i in range(2, 6)])
def test_beta_ndim_toohigh(try_libpmadra, dtype, ndim):
    beta = rng.random([random.randint(1, 3) for i in range(ndim)],
                      'float32')
    n0 = rng.random((beta.shape[0], ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


# Lengths of n0, intn0, and beta don't match.

@pytest.mark.parametrize('try_libpmadra,dtype,param',
                         [(t, d, p) for t in (True, False)
                          for d in dtypes_all
                          for p in ('n0', 'intn0', 'beta')])
def test_vector_lengths_dont_match(try_libpmadra, dtype, param):
    for _ in range(10):
        length_two = random.randint(1, 50)
        length_other = random.randint(1, 50)
        while length_other == length_two:
            length_other = random.randint(1, 50)
        shapes = {k: ({True: length_other,
                       False: length_two}[k == param], )
                  for k in ('n0', 'intn0', 'beta')}
        params = {k: rng.random(v, 'float32').reshape(v)
                  for k, v in shapes.items()}
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(params['n0'], params['beta'],
                                alpha, gamma, t,
                                intn0=params['intn0'],
                                dtype=dtype,
                                try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('param', ('n0', 'intn0', 'beta'))
def test_multi_vector_lengths_dont_match(param):
    for _ in range(10):
        length_two = random.randint(1, 50)
        length_other = random.randint(1, 50)
        while length_other == length_two:
            length_other = random.randint(1, 50)
        lengths = {k: {True: length_other,
                       False: length_two}[k == param]
                   for k in ('n0', 'intn0', 'beta')}
        params = {k: [gmpy2.mpfr_random(gmpy2rng) for _ in range(v)]
                  for k, v in lengths.items()}
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t = gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(params['n0'], params['beta'],
                                 alpha, gamma, t,
                                 intn0=params['intn0'])


# non-real element of n0.

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_n0_hasnonreal(try_libpmadra, dtype, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32').tolist()
    n0[random.randrange(len(n0))] = element
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_n0_hasnonreal(element):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    n0[random.randrange(len(n0))] = element
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-real element of intn0.

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_intn0_hasnonreal(try_libpmadra, dtype, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    intn0 = rng.random(n0.shape, 'float32').tolist()
    intn0[random.randrange(len(n0))] = element
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0,
                            dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_intn0_hasnonreal(element):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    intn0 = [gmpy2.mpfr_random(gmpy2rng)
             for _ in range(len(n0))]
    intn0[random.randrange(len(n0))] = element
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0)


# non-real element of beta.

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_beta_hasnonreal(try_libpmadra, dtype, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random(n0.shape, 'float32').tolist()
    beta[random.randrange(len(n0))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_beta_hasnonreal(element):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    beta[random.randrange(len(n0))] = element
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(TypeError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# non-real element of t.

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in non_real_params_numpy
                          if t or d != 'quad'])
def test_t_hasnonreal(try_libpmadra, dtype, element):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32').tolist()
    t[random.randrange(len(t))] = element
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


# complex n0, intn0, and beta

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_n0_complex(try_libpmadra, dtype):
    n0 = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype='auto',
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_intn0_complex(try_libpmadra, dtype):
    n0 = rng.random((random.randint(1, 40), ), 'float32')
    intn0 = rng.random(n0.shape, 'float32').astype(dtype)
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0,
                            dtype='auto', try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_beta_complex(try_libpmadra, dtype):
    n0 = rng.random((random.randint(1, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32').astype(dtype)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises((TypeError, ValueError)):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype='auto',
                            try_libpmadra=try_libpmadra)


# NaN and -inf alpha

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v) for t in (True, False)
                          for d in dtypes_all
                          for v in ('nan', '-inf')])
def test_alpha_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(1, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = float(value)
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_alpha_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = float(value)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# NaN and -inf gamma

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v) for t in (True, False)
                          for d in dtypes_all
                          for v in ('nan', '-inf')])
def test_gamma_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(1, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = float(value)
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_gamma_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = float(value)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# NaN, -inf, or +inf t0.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v) for t in (True, False)
                          for d in dtypes_all
                          for v in ('nan', '-inf', 'inf')])
def test_t0_nan_or_inf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(1, 40), ), 'float32')
    beta = rng.random(n0.shape, 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    t0 = float(value)
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, t0=t0,
                            dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_t0_gamma_nan_or_inf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    t0 = float(value)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t, t0=t0)


# NaN or -inf element of n0.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in ('nan', '-inf')
                          if t or d != 'quad'])
def test_n0_has_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    n0[random.randrange(len(n0))] = float(value)
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_n0_has_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    n0[random.randrange(len(n0))] = float(value)
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# NaN or -inf element of intn0.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in ('nan', '-inf')
                          if t or d != 'quad'])
def test_intn0_has_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    intn0 = rng.random((len(n0), ), 'float32')
    intn0[random.randrange(len(n0))] = float(value)
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0,
                            dtype=dtype, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_intn0_has_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    intn0 = [gmpy2.mpfr_random(gmpy2rng)
             for _ in range(len(n0))]
    intn0[random.randrange(len(n0))] = float(value)
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t, intn0=intn0)


# NaN or -inf element of beta.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in ('nan', '-inf')
                          if t or d != 'quad'])
def test_beta_has_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    beta[random.randrange(len(n0))] = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_beta_has_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    beta[random.randrange(len(n0))] = float(value)
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = 100 * gmpy2.mpfr_random(gmpy2rng)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# NaN or -inf t or element of t.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in ('nan', '-inf')
                          if t or d != 'quad'])
def test_t_has_nan_or_ninf(try_libpmadra, dtype, value):
    n0 = rng.random((random.randint(2, 100), ), 'float32')
    beta = rng.random((len(n0), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    t = rng.random((random.randint(2, 40), ), 'float32')
    t[random.randrange(len(t))] = float(value)
    with pytest.raises(ValueError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                            try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_t_nan_or_ninf(value):
    n0 = [gmpy2.mpfr_random(gmpy2rng)
          for _ in range(random.randint(1, 40))]
    beta = [gmpy2.mpfr_random(gmpy2rng)
            for _ in range(len(n0))]
    alpha = gmpy2.mpfr_random(gmpy2rng)
    gamma = gmpy2.mpfr_random(gmpy2rng)
    t = float(value)
    with pytest.raises(ValueError):
        cfm.solve_analytical(n0, beta, alpha, gamma, t)


# negative alpha

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d) for t in (True, False)
                          for d in dtypes_all])
def test_alpha_negative(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(1, 40), ), 'float32')
        beta = rng.random(n0.shape, 'float32')
        alpha = -100 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_alpha_negative():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        alpha = -100 * gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t = 100 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t)


# negative gamma

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d) for t in (True, False)
                          for d in dtypes_all])
def test_gamma_negative(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(1, 40), ), 'float32')
        beta = rng.random(n0.shape, 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = -100 * rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_gamma_negative():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = -100 * gmpy2.mpfr_random(gmpy2rng)
        t = 100 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t)


# negative emax_safety_factor

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_emax_safety_factor_negative(try_libpmadra):
    for _ in range(10):
        n0 = rng.random((random.randint(1, 40), ), 'float32')
        beta = rng.random(n0.shape, 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        emax_safety_factor = -100 * rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, dtype='auto',
                                try_libpmadra=try_libpmadra,
                                emax_safety_factor=emax_safety_factor)


# negative element of n0.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all
                          if t or d != 'quad'])
def test_n0_has_negative(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        n0[random.randrange(len(n0))] *= -1
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_n0_has_negative():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        n0[random.randrange(len(n0))] *= -1
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t = 100 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t)


# negative element of intn0

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all
                          if t or d != 'quad'])
def test_intn0_has_negative(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        intn0 = rng.random((len(n0), ), 'float32')
        intn0[random.randrange(len(n0))] *= -1
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t,
                                intn0=intn0, dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_intn0_has_negative():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        intn0 = [gmpy2.mpfr_random(gmpy2rng)
                 for _ in range(len(n0))]
        intn0[random.randrange(len(n0))] *= -1
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t = 100 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t,
                                 intn0=intn0)


# negative element of beta.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all
                          if t or d != 'quad'])
def test_beta_has_negative(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        beta[random.randrange(len(n0))] *= -1
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t = rng.random((random.randint(2, 40), ), 'float32')
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_beta_has_negative():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        beta[random.randrange(len(n0))] *= -1
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t = 100 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t)


# t or element of t less than t0.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all
                          if t or d != 'quad'])
def test_t_has_lessthan_t0(try_libpmadra, dtype):
    for _ in range(10):
        n0 = rng.random((random.randint(2, 100), ), 'float32')
        beta = rng.random((len(n0), ), 'float32')
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        t0 = 1000 * (0.5 - rng.random((1, ), 'float32')[0])
        t = t0 + 1000 * rng.random((random.randint(2, 40), ), 'float32')
        t[random.randrange(len(t))] = t0 - 1000 * rng.random(
            (1, ), 'float32')[0]
        with pytest.raises(ValueError):
            cf.solve_analytical(n0, beta, alpha, gamma, t, t0=t0,
                                dtype=dtype,
                                try_libpmadra=try_libpmadra)


def test_multi_t_lessthan_t0():
    for _ in range(10):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 40))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        alpha = gmpy2.mpfr_random(gmpy2rng)
        gamma = gmpy2.mpfr_random(gmpy2rng)
        t0 = 1000 * (0.5 - gmpy2.mpfr_random(gmpy2rng))
        t = t0 - 1000 * gmpy2.mpfr_random(gmpy2rng)
        with pytest.raises(ValueError):
            cfm.solve_analytical(n0, beta, alpha, gamma, t, t0=t0)


# +inf element of n0 or beta means no floating format is suitable.

@pytest.mark.parametrize('try_libpmadra,dtype,param',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes
                          for v in ('n0', 'beta')])
def test_n0_or_beta_has_inf_cause_overflow(try_libpmadra, dtype, param):
    tp = numpy.dtype(dtype).type
    n0 = tp(rng.random((random.randint(2, 100), ), 'float64'))
    beta = tp(rng.random((len(n0), ), 'float64'))
    alpha = tp(rng.random((1, ), 'float64')[0])
    gamma = tp(rng.random((1, ), 'float64')[0])
    t = tp(rng.random((random.randint(2, 40), ), 'float64'))
    if param == 'n0':
        n0[random.randrange(len(n0))] = numpy.inf
    else:
        beta[random.randrange(len(n0))] = numpy.inf
    with pytest.raises(OverflowError):
        cf.solve_analytical(n0, beta, alpha, gamma, t, dtype='auto',
                            try_libpmadra=try_libpmadra)


# Time translation symmetry

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if (t or d != 'quad')
                          and (d != 'auto' or a != 'zero')])
def test_time_translation_symmetry(try_libpmadra, dtype, alpha, gamma):
    for i in range(5):
        n0 = rng.random((random.randint(2, 30), ), 'float64')
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = rng.random((len(n0), ), 'float64')
        beta = rng.random((len(n0), ), 'float64')
        if alpha == 'zero':
            alph = numpy.float32(0)
        else:
            alph = rng.random((1, ), 'float64')[0]
        if gamma == 'zero':
            gamm = numpy.float32(0)
        else:
            gamm = rng.random((1, ), 'float64')[0]
        t_base = 100 * rng.random((random.randint(2, 20), ), 'float64')
        ninf, n, intn, format_used = cf.solve_analytical(
            n0, beta, alph, gamm, t_base, intn0=intn0, dtype=dtype,
            try_libpmadra=try_libpmadra)
        for _ in range(5):
            t0 = 1000 * (0.5 - rng.random((1, ), 'float64')[0])
            ninf_t, n_t, intn_t, format_used_t = cf.solve_analytical(
                n0, beta, alph, gamm, t0 + t_base, t0=t0, intn0=intn0,
                dtype=dtype, try_libpmadra=try_libpmadra)
            assert format_used == format_used_t
            assert ninf.shape == ninf_t.shape
            assert n.shape == n_t.shape
            assert intn.shape == intn_t.shape
            assert ninf.dtype == ninf_t.dtype
            assert n.dtype == n_t.dtype
            assert intn.dtype == intn_t.dtype
            assert_array_equal(ninf, ninf_t)
            if format_used == 'float32':
                tol = 1e3 * numpy.finfo('float32').eps
            else:
                tol = 1e3 * numpy.finfo('float64').eps
            assert_allclose(n, n_t, rtol=tol, atol=tol)
            assert_allclose(intn, intn_t,
                            rtol=1e2 * tol, atol=1e2 * tol)


@pytest.mark.parametrize('binom_from_gamma,precision,alpha,gamma',
                         [(b, p, a, g)
                          for b in (False, True)
                          for p in (80, 128, 200)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_time_translation_symmetry(binom_from_gamma, precision,
                                         alpha, gamma):
    tol = 1e-10
    for i in range(5):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 30))]
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = [gmpy2.mpfr_random(gmpy2rng)
                     for _ in range(len(n0))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        if alpha == 'zero':
            alph = gmpy2.mpfr(0)
        else:
            alph = gmpy2.mpfr_random(gmpy2rng)
        if gamma == 'zero':
            gamm = gmpy2.mpfr(0)
        else:
            gamm = gmpy2.mpfr_random(gmpy2rng)
        t_base = 100 * gmpy2.mpfr_random(gmpy2rng)
        ninf, n, intn = cfm.solve_analytical(
            n0, beta, alph, gamm, t_base, intn0=intn0,
            binom_from_gamma=binom_from_gamma, precision=precision)
        for _ in range(5):
            t0 = 1000 * (0.5 - gmpy2.mpfr_random(gmpy2rng))
            ninf_t, n_t, intn_t = cfm.solve_analytical(
                n0, beta, alph, gamm, t0 + t_base, intn0=intn0, t0=t0,
                binom_from_gamma=binom_from_gamma, precision=precision)
            assert ninf == ninf_t
            assert_allclose(numpy.longdouble(n), numpy.longdouble(n_t),
                            rtol=tol, atol=tol)
            assert_allclose(numpy.longdouble(intn),
                            numpy.longdouble(intn_t),
                            rtol=tol, atol=tol)


# Changing time units

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if (t or d != 'quad')
                          and (d != 'auto'
                               or a != 'zero'
                               or g != 'zero')])
def test_changing_time_units(try_libpmadra, dtype, alpha, gamma):
    for i in range(5):
        n0 = rng.random((random.randint(2, 30), ), 'float64')
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = rng.random((len(n0), ), 'float64')
        beta = rng.random((len(n0), ), 'float64')
        if alpha == 'zero':
            alph = numpy.float32(0)
        else:
            alph = rng.random((1, ), 'float64')[0]
        if gamma == 'zero':
            gamm = numpy.float32(0)
        else:
            gamm = rng.random((1, ), 'float64')[0]
        t0 = 1000 * (0.5 - rng.random((1, ), 'float64')[0])
        t = t0 + 100 * rng.random((random.randint(2, 20), ), 'float64')
        ninf, n, intn, format_used = cf.solve_analytical(
            n0, beta, alph, gamm, t, t0=t0, intn0=intn0, dtype=dtype,
            try_libpmadra=try_libpmadra)
        for _ in range(5):
            scale = 10.0 ** (-3 + 5 * rng.random((1, ), 'float64')[0])
            if intn0 is None:
                intn0_to_pass = None
            else:
                intn0_to_pass = scale * intn0
            ninf_t, n_t, intn_t, format_used_t = cf.solve_analytical(
                n0, beta / scale, alph / scale, gamm / scale,
                scale * t, t0=scale * t0, intn0=intn0_to_pass,
                dtype=dtype, try_libpmadra=try_libpmadra)
            assert format_used == format_used_t
            assert ninf.shape == ninf_t.shape
            assert n.shape == n_t.shape
            assert intn.shape == intn_t.shape
            assert ninf.dtype == ninf_t.dtype
            assert n.dtype == n_t.dtype
            assert intn.dtype == intn_t.dtype
            if format_used == 'float32':
                tol = 1e4 * numpy.finfo('float32').eps
            else:
                tol = 1e5 * numpy.finfo('float64').eps
            assert_allclose(ninf, ninf_t, rtol=tol, atol=tol)
            assert_allclose(n, n_t, rtol=tol, atol=tol)
            assert_allclose(scale * intn, intn_t,
                            rtol=1e2 * tol, atol=1e2 * tol)


@pytest.mark.parametrize('binom_from_gamma,precision,alpha,gamma',
                         [(b, p, a, g)
                          for b in (False, True)
                          for p in (80, 128, 200)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_changing_time_units(binom_from_gamma, precision,
                                   alpha, gamma):
    tol = 1e-10
    for i in range(5):
        n0 = [gmpy2.mpfr_random(gmpy2rng)
              for _ in range(random.randint(1, 30))]
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = [gmpy2.mpfr_random(gmpy2rng)
                     for _ in range(len(n0))]
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(len(n0))]
        if alpha == 'zero':
            alph = gmpy2.mpfr(0)
        else:
            alph = gmpy2.mpfr_random(gmpy2rng)
        if gamma == 'zero':
            gamm = gmpy2.mpfr(0)
        else:
            gamm = gmpy2.mpfr_random(gmpy2rng)
        t0 = 1000 * (0.5 - gmpy2.mpfr_random(gmpy2rng))
        t = t0 + 100 * gmpy2.mpfr_random(gmpy2rng)
        ninf, n, intn = cfm.solve_analytical(
            n0, beta, alph, gamm, t, t0=t0, intn0=intn0,
            binom_from_gamma=binom_from_gamma, precision=precision)
        for _ in range(5):
            scale = gmpy2.mpfr(10.0) ** (
                -3 + 5 * gmpy2.mpfr_random(gmpy2rng))
            if intn0 is None:
                intn0_to_pass = None
            else:
                intn0_to_pass = [scale * v for v in intn0]
            ninf_t, n_t, intn_t = cfm.solve_analytical(
                n0, [v / scale for v in beta], alph / scale,
                gamm / scale, scale * t, t0=scale * t0,
                intn0=intn0_to_pass,
                binom_from_gamma=binom_from_gamma, precision=precision)
            assert_allclose(numpy.longdouble(ninf),
                            numpy.longdouble(ninf_t),
                            rtol=tol, atol=tol)
            assert_allclose(numpy.longdouble(n), numpy.longdouble(n_t),
                            rtol=tol, atol=tol)
            assert_allclose(
                numpy.longdouble([scale * v for v in intn]),
                numpy.longdouble(intn_t),
                rtol=tol, atol=tol)


# n_k == int_n_k == beta_k == 0 for all k gives zero

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if (t or d != 'quad')
                          and (d != 'auto'
                               or a != 'zero'
                               or g != 'zero')])
def test_n0_intn0_beta_zero(try_libpmadra, dtype, alpha, gamma):
    for i in range(10):
        if dtype in ('auto', 'quad'):
            tp = (numpy.float32, numpy.float64,
                  numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        n0 = numpy.zeros((random.randint(1, 100), ), dtype=tp)
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = numpy.zeros_like(n0)
        beta = numpy.zeros_like(n0)
        if alpha == 'zero':
            alph = tp(0)
        else:
            alph = tp(rng.random((1, ), 'float64')[0])
        if gamma == 'zero':
            gamm = tp(0)
        else:
            gamm = tp(rng.random((1, ), 'float64')[0])
        t0 = 1000 * (0.5 - tp(rng.random((1, ), 'float64')[0]))
        t = t0 + 100 * tp(rng.random((random.randint(2, 20), ),
                                     'float64'))
        ninf, n, intn, _ = cf.solve_analytical(
            n0, beta, alph, gamm, t, t0=t0, intn0=intn0, dtype=dtype,
            try_libpmadra=try_libpmadra)
        assert_array_equal(ninf, 0)
        assert_array_equal(n, 0)
        assert_array_equal(intn, 0)


@pytest.mark.parametrize('binom_from_gamma,precision,alpha,gamma',
                         [(b, p, a, g)
                          for b in (False, True)
                          for p in (80, 128, 200)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_n0_intn0_beta_zero(binom_from_gamma, precision,
                                  alpha, gamma):
    zero = gmpy2.mpfr(0)
    for i in range(5):
        n0 = random.randint(1, 30) * [zero]
        if i % 2 == 0:
            intn0 = None
        else:
            intn0 = len(n0) * [zero]
        beta = len(n0) * [zero]
        if alpha == 'zero':
            alph = zero
        else:
            alph = gmpy2.mpfr_random(gmpy2rng)
        if gamma == 'zero':
            gamm = zero
        else:
            gamm = gmpy2.mpfr_random(gmpy2rng)
        t0 = 1000 * (0.5 - gmpy2.mpfr_random(gmpy2rng))
        t = t0 + 100 * gmpy2.mpfr_random(gmpy2rng)
        ninf, n, intn = cfm.solve_analytical(
            n0, beta, alph, gamm, t, t0=t0, intn0=intn0,
            binom_from_gamma=binom_from_gamma, precision=precision)
        assert ninf == len(n0) * [zero]
        assert n == len(n0) * [zero]
        assert intn == len(n0) * [zero]


# Do a cross comparison between all functions calculating the solution
# and to the numerical solution
@pytest.mark.parametrize('alpha,gamma',
                         [(a, g)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_compare_all_and_numerical(alpha, gamma):
    for _ in range(10):
        Mc = random.randint(1, 30)
        n0 = [random.randint(1, 100) / random.randint(1, 100)
              for _ in range(Mc)]
        intn0 = [random.randint(0, 100) / random.randint(1, 100)
                 for _ in range(Mc)]
        beta = [random.randint(1, 100) / random.randint(1, 100)
                for _ in range(Mc)]
        if alpha == 'zero':
            alph = 0.0
        else:
            alph = random.randint(1, 100) / random.randint(1, 100)
        if gamma == 'zero':
            gamm = 0
        else:
            gamm = random.randint(1, 100) / random.randint(1, 100)
        # Make the time parameters, with t being sorted and unique with
        # the first element equal to t0 and the last being +inf.
        t0 = numpy.float64(random.randint(-1000, 1000)
                           / random.randint(1, 100))
        t = numpy.unique(numpy.hstack((
            numpy.float64((t0, numpy.inf)),
            t0 + 10.0 ** (-3.0 + 6.0 * numpy.atleast_1d(
                rng.random((random.randint(1, 20), ), 'float64'))))))
        # Get the outputs in masse from const_coeff.solve_analytical for
        # all dtypes with and without libpmadra and check that they
        # agree with each other.
        by_p = {v: cf.solve_analytical(numpy.dtype(v).type(n0),
                                       numpy.dtype(v).type(beta),
                                       numpy.dtype(v).type(alph),
                                       numpy.dtype(v).type(gamm),
                                       t.astype(v),
                                       t0=t0.astype(v),
                                       intn0=numpy.dtype(v).type(intn0),
                                       dtype=v, try_libpmadra=False)
                for v in dtypes}
        by_f = {v: cf.solve_analytical(numpy.dtype(v).type(n0),
                                       numpy.dtype(v).type(beta),
                                       numpy.dtype(v).type(alph),
                                       numpy.dtype(v).type(gamm),
                                       t.astype(v),
                                       t0=t0.astype(v),
                                       intn0=numpy.dtype(v).type(intn0),
                                       dtype=v, try_libpmadra=True)
                for v in dtypes}
        for k in dtypes:
            ninf_p, n_p, intn_p, fmt_p = by_p[k]
            ninf_f, n_f, intn_f, fmt_f = by_f[k]
            tp = numpy.dtype(k).type
            # Check format_used.
            assert fmt_p == k
            assert fmt_f == k
            # Check shape.
            assert ninf_p.shape == (Mc, )
            assert n_p.shape == (Mc, len(t))
            assert intn_p.shape == (Mc, len(t))
            assert ninf_p.shape == ninf_f.shape
            assert n_p.shape == n_f.shape
            assert intn_p.shape == intn_f.shape
            # Check dtype
            assert ninf_p.dtype == numpy.dtype(k)
            assert n_p.dtype == ninf_p.dtype
            assert intn_p.dtype == ninf_p.dtype
            assert ninf_p.dtype == ninf_f.dtype
            assert n_p.dtype == n_f.dtype
            assert intn_p.dtype == intn_f.dtype
            # Check that the values are close, with the values for the
            # first time (t == t0) being exactly the initial values and
            # the values for the last time (t == +inf) being exactly
            # ninf and +inf.
            if k == 'float32':
                tol = 1e-4
            else:
                tol = 1e-6
            # initial values
            assert_array_equal(n_p[:, 0], tp(n0))
            assert_array_equal(n_f[:, 0], tp(n0))
            assert_array_equal(intn_p[:, 0], tp(intn0))
            assert_array_equal(intn_f[:, 0], tp(intn0))
            # final values.
            if alph == 0 and gamm == 0:
                assert numpy.isinf(n_p[:, -1]).all()
                assert numpy.isinf(n_f[:, -1]).all()
            else:
                assert_array_equal(n_p[:, -1], ninf_p)
                assert_array_equal(n_f[:, -1], ninf_f)
            assert numpy.isinf(intn_p[:, -1]).all()
            assert numpy.isinf(intn_f[:, -1]).all()
            # the rest
            assert_allclose(ninf_p, ninf_f,
                            rtol=tol, atol=1e-7)
            assert_allclose(n_p[:, 1:-1], n_f[:, 1:-1],
                            rtol=100 * tol, atol=1e-7)
            assert_allclose(intn_p[:, 1:-1], intn_f[:, 1:-1],
                            rtol=100 * tol, atol=1e-7)
            if k != 'longdouble':
                assert_allclose(
                    ninf_f, by_f['longdouble'][0],
                    rtol=tol, atol=1e-7)
                assert_allclose(
                    n_f[:, 1:-1], by_f['longdouble'][1][:, 1:-1],
                    rtol=500 * tol, atol=1e-7)
                assert_allclose(
                    intn_f[:, 1:-1], by_f['longdouble'][2][:, 1:-1],
                    rtol=500 * tol, atol=1e-7)
            # Check that all values are non-negative.
            assert numpy.all(ninf_p >= 0.0)
            assert numpy.all(ninf_f >= 0.0)
            assert numpy.all(n_p >= 0.0)
            assert numpy.all(n_f >= 0.0)
            assert numpy.all(intn_p >= 0.0)
            assert numpy.all(intn_f >= 0.0)
            # Moreover, intn >= intn0.
            assert numpy.all(intn_p >= numpy.atleast_2d(
                numpy.asarray(intn0, dtype=k)).T)
            assert numpy.all(intn_f >= numpy.atleast_2d(
                numpy.asarray(intn0, dtype=k)).T)
            # Make sure that n_inf gives the exact same values for ninf.
            assert_array_equal(ninf_p, cf.n_inf(beta, tp(alph),
                                                tp(gamm), dtype=k,
                                                try_libpmadra=False))
            assert_array_equal(ninf_f, cf.n_inf(beta, tp(alph),
                                                tp(gamm), dtype=k,
                                                try_libpmadra=True))
        # Do the multi-precision comparisons.
        _, n_ld, intn_ld, _ = by_f['longdouble']
        for precision in (40, 60, 200):
            for emax in (128, 800):
                ninf_m = cfm.n_inf(beta, alph, gamm,
                                   precision=precision, emax=emax)
                for binom_from_gamma in (False, True):
                    for i, v in enumerate(t):
                        ninf_m_v, n_m, intn_m = cfm.solve_analytical(
                            n0, beta, alph, gamm, v, t0=t0, intn0=intn0,
                            binom_from_gamma=binom_from_gamma,
                            precision=precision, emax=emax)
                        assert ninf_m == ninf_m_v
                        if i == 0:
                            with gmpy2.local_context(
                                    gmpy2.context(),
                                    precision=precision,
                                    emax=emax,
                                    emin=1 - emax) as ctx:
                                assert [gmpy2.mpfr(v2)
                                        for v2 in n0] == n_m
                                assert [gmpy2.mpfr(v2)
                                        for v2 in intn0] == intn_m
                        elif i == len(t) - 1:
                            if alph == 0 and gamm == 0:
                                for v2 in n_m:
                                    assert gmpy2.is_infinite(v2)
                                    assert not gmpy2.is_signed(v2)
                            else:
                                assert ninf_m == n_m
                            for v2 in intn_m:
                                assert gmpy2.is_infinite(v2)
                                assert not gmpy2.is_signed(v2)
                        else:
                            assert_allclose(n_ld[:, i],
                                            numpy.longdouble(n_m),
                                            rtol=1e-6, atol=1e-7)
                            assert_allclose(intn_ld[:, i],
                                            numpy.longdouble(intn_m),
                                            rtol=1e-6, atol=1e-7)
        # Compare to the numerical solution.
        y0, yprime, _, _, _, _, _, _ = cf.build_ode_components(
            n0, beta, numpy.longdouble(alph), numpy.longdouble(gamm),
            intn0=intn0, include_integral=True, dtype='float64')
        eps = numpy.finfo('float64').eps
        sol = scipy.integrate.solve_ivp(
            yprime, (t0, t[-2]), y0,
            method='RK45', t_eval=t[:-1],
            atol=1e2 * eps, rtol=1e2 * eps)
        assert_allclose(n_ld[:, :-1], sol.y[Mc:],
                        rtol=1e5 * eps, atol=1e5 * eps)
        assert_allclose(intn_ld[:, :-1], sol.y[:Mc],
                        rtol=1e5 * eps, atol=1e5 * eps)
