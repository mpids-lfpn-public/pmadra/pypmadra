# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose, assert_array_equal


import pytest

from pypmadra import fold_Mc


# Initialize the random generators.
random.seed()
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')
dtypes_all = ('auto', 'float32', 'float64', 'longdouble')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_int_params = (-1.3, None, ..., slice(9), [], (), set(), dict(),
                  numpy.float64(-3), numpy.int32((3, 1)))
non_real_params = (complex(1, 2), complex(1, 0), ..., slice(4),
                   [], (), set(), dict())


# non-int Mc_folded

@pytest.mark.parametrize('try_libpmadra,Mc_folded',
                         [(t, v) for t in (True, False)
                          for v in non_int_params])
def test_Mc_folded_nonint(try_libpmadra, Mc_folded):
    y = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(TypeError):
        fold_Mc(y, Mc_folded, try_libpmadra=try_libpmadra)


# negative Mc_folded

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_Mc_folded_negative(try_libpmadra):
    for _ in range(100):
        y = rng.random((random.randint(2, 100), ), 'float32')
        Mc_folded = random.randint(-1000, -1)
        with pytest.raises(ValueError):
            fold_Mc(y, Mc_folded, try_libpmadra=try_libpmadra)


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    y = rng.random((random.randint(2, 40), ), 'float32')
    Mc_folded = random.randrange(100)
    with pytest.raises(TypeError):
        fold_Mc(y, Mc_folded, try_libpmadra=try_libpmadra)


# invalid dtype

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_dtype_invalid(try_libpmadra, dtype):
    y = rng.random((random.randint(2, 40), ), 'float32')
    Mc_folded = random.randrange(100)
    with pytest.raises(ValueError):
        fold_Mc(y, Mc_folded, dtype=dtype, try_libpmadra=try_libpmadra)


# non-Sequence y

@pytest.mark.parametrize('try_libpmadra,dtype,y',
                         [(t, d, b)
                          for t in (True, False)
                          for d in dtypes_all
                          for b in non_seq_params])
def test_y_nonseq(try_libpmadra, dtype, y):
    Mc_folded = random.randrange(100)
    with pytest.raises(TypeError):
        fold_Mc(y, Mc_folded, dtype=dtype, try_libpmadra=try_libpmadra)


# y has too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(t, i) for t in (True, False)
                          for i in range(3, 6)])
def test_y_ndim_toohigh(try_libpmadra, ndim):
    y = rng.random([random.randint(1, 3) for i in range(ndim)],
                   'float32')
    Mc_folded = random.randrange(100)
    with pytest.raises(ValueError):
        fold_Mc(y, Mc_folded, try_libpmadra=try_libpmadra)


# complex y

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_y_complex(try_libpmadra, dtype):
    y = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    Mc_folded = random.randrange(100)
    with pytest.raises(TypeError):
        fold_Mc(y, Mc_folded, dtype='auto',
                try_libpmadra=try_libpmadra)


# non-real element of y.

@pytest.mark.parametrize('try_libpmadra,dims,dtype,element',
                         [(t, ds, d, v)
                          for t in (False, True)
                          for ds in (1, 2)
                          for d in ('auto', 'float32')
                          for v in non_real_params])
def test_y_hasnonreal(try_libpmadra, dims, dtype, element):
    if dims == 1:
        y = rng.random((random.randint(2, 100), ), 'float32')
        y_to_pass = y.tolist()
        y_to_pass[random.randrange(len(y))] = element
    else:
        y = rng.random((random.randint(1, 10), random.randint(1, 10)),
                       'float32')
        y_to_pass = y.tolist()
        y_to_pass[random.randrange(y.shape[0])][
            random.randrange(y.shape[1])] = element
    Mc_folded = random.randrange(100)
    with pytest.raises((TypeError, ValueError)):
        fold_Mc(y_to_pass, Mc_folded, dtype=dtype,
                try_libpmadra=try_libpmadra)


# NaN or -inf element of y.

@pytest.mark.parametrize('try_libpmadra,dtype,value',
                         [(t, d, v)
                          for t in (False, True)
                          for d in dtypes_all
                          for v in (numpy.nan, -numpy.inf)])
def test_y_hasnanninf(try_libpmadra, dtype, value):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        y = tp(rng.random((random.randint(2, 100), ), 'float64'))
        y[random.randrange(len(y))] = value
        Mc_folded = random.randrange(100)
        with pytest.raises(ValueError):
            fold_Mc(y, Mc_folded, dtype=dtype,
                    try_libpmadra=try_libpmadra)


# negative element of y.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all])
def test_y_hasnegative(try_libpmadra, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        y = tp(rng.random((random.randint(2, 100), ), 'float64'))
        y[random.randrange(len(y))] *= -10
        Mc_folded = random.randrange(100)
        with pytest.raises(ValueError):
            fold_Mc(y, Mc_folded, dtype=dtype,
                    try_libpmadra=try_libpmadra)


# Mc_folded = 0

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (False, True)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_Mc_folded_zero(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dims == 1:
            shape = (random.randint(1, 100), )
        else:
            shape = tuple([random.randint(1, 10) for _ in range(2)])
        y = tp(rng.random(shape, 'float64')).reshape(shape)
        y_folded, folded_fraction = fold_Mc(
            y, 0, dtype=dtype, try_libpmadra=try_libpmadra)
        assert_array_equal(y_folded,
                           numpy.zeros([0] + list(shape[1:]), dtype=tp))
        assert_array_equal(folded_fraction,
                           numpy.ones((shape[-1], ), dtype=tp))


# Mc_folded == Mc

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (False, True)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_Mc_folded_equal_Mc(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dims == 1:
            shape = (random.randint(2, 100), )
        else:
            shape = tuple([random.randint(2, 10) for _ in range(2)])
        y = tp(rng.random(shape, 'float64').reshape(shape))
        y_folded, folded_fraction = fold_Mc(
            y, shape[0], dtype=dtype, try_libpmadra=try_libpmadra)
        assert_array_equal(y_folded, y)
        assert_array_equal(folded_fraction,
                           numpy.zeros((shape[-1], ), dtype=tp))


# Mc_folded > Mc

@pytest.mark.parametrize('try_libpmadra,dims,dtype',
                         [(t, dims, d)
                          for t in (False, True)
                          for dims in (1, 2)
                          for d in dtypes_all])
def test_Mc_folded_greater_than_Mc(try_libpmadra, dims, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dims == 1:
            shape = (random.randint(1, 100), )
        else:
            shape = tuple([random.randint(1, 10) for _ in range(2)])
        y = tp(rng.random(shape, 'float64')).reshape(shape)
        Mc = shape[0]
        Mc_folded = Mc + random.randrange(1, 100)
        y_folded, folded_fraction = fold_Mc(
            y, Mc_folded, dtype=dtype, try_libpmadra=try_libpmadra)
        assert_array_equal(y_folded,
                           numpy.concatenate(
                               (y, numpy.zeros([Mc_folded - Mc]
                                               + list(shape[1:]),
                                               dtype=tp)), 0))
        assert_array_equal(folded_fraction,
                           numpy.zeros((shape[-1], ), dtype=tp))


# 0 < Mc_folded < Mc for y = a / k

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all])
def test_y_is_a_over_k(try_libpmadra, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if tp == numpy.float32:
            tol = 2e-5
        else:
            tol = 2e-8
        ny = random.randint(1, 10)
        Mc = random.randint(2, 20)
        Mc_folded = random.randint(1, Mc - 1)
        k = numpy.arange(1, Mc + 1, dtype=tp).reshape((Mc, 1))
        a = tp(100 * rng.random((1, ny), 'float64')).reshape((1, ny))
        y = a / k
        y_folded, folded_fraction = fold_Mc(
            y, Mc_folded, dtype=dtype, try_libpmadra=try_libpmadra)
        assert y_folded.shape == (Mc_folded, ny)
        assert y_folded.dtype == numpy.dtype(tp)
        assert folded_fraction.shape == (ny, )
        assert folded_fraction.dtype == numpy.dtype(tp)
        assert_array_equal(y_folded[:-1], y[:(Mc_folded - 1)])
        assert_allclose(y_folded[-1],
                        a[0] * ((Mc - Mc_folded + 1) / Mc_folded),
                        rtol=tol, atol=0)
        assert_allclose(folded_fraction, (Mc - Mc_folded) / Mc,
                        rtol=tol, atol=0)


# 0 < Mc_folded < Mc for y = a

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(t, d)
                          for t in (False, True)
                          for d in dtypes_all])
def test_y_is_a_over_k(try_libpmadra, dtype):
    for i in range(10):
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if tp == numpy.float32:
            tol = 2e-5
        else:
            tol = 2e-8
        ny = random.randint(1, 10)
        Mc = random.randint(2, 20)
        Mc_folded = random.randint(1, Mc - 1)
        k = numpy.arange(1, Mc + 1, dtype=tp)
        a = tp(100 * rng.random((1, ny), 'float64')).reshape((1, ny))
        y = numpy.tile(a, (Mc, 1))
        y_folded, folded_fraction = fold_Mc(
            y, Mc_folded, dtype=dtype, try_libpmadra=try_libpmadra)
        assert y_folded.shape == (Mc_folded, ny)
        assert y_folded.dtype == numpy.dtype(tp)
        assert folded_fraction.shape == (ny, )
        assert folded_fraction.dtype == numpy.dtype(tp)
        assert_array_equal(y_folded[:-1], y[:(Mc_folded - 1)])
        assert_allclose(y_folded[-1],
                        a[0] * k[(Mc_folded - 1):].sum() / Mc_folded,
                        rtol=tol, atol=0)
        assert_allclose(folded_fraction, k[Mc_folded:].sum() / k.sum(),
                        rtol=tol, atol=0)
