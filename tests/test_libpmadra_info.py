# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import pypmadra


# Test the libpmadra_info function.

def test_libpmadra_info():
    out = pypmadra.libpmadra_info()
    assert isinstance(out, tuple)
    assert len(out) == 4
    available, name, version, supported_precisions = out
    assert isinstance(available, bool)
    if available:
        assert isinstance(name, str)
        assert len(name) != 0
        assert 'Version' in version.__class__.__name__
        assert isinstance(supported_precisions, tuple)
        assert len(supported_precisions) == 4
        assert all([isinstance(v, bool) for v in supported_precisions])
    else:
        assert name is None
        assert version is None
        assert supported_precisions is None
