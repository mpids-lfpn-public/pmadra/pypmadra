# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import random

import numpy
import numpy.random
from numpy.testing import assert_allclose, assert_array_less, \
    assert_array_equal

import gmpy2

import pytest

import pypmadra.const_coeff as cf
import pypmadra.const_coeff.rational as cfr
import pypmadra.const_coeff.multi as cfm



# Initialize the random generators.
random.seed()
gmpy2rng = gmpy2.random_state(random.randrange(2**32))
rng = numpy.random.default_rng()


# dtypes for python and libpmadra
dtypes = ('float32', 'float64', 'longdouble')
dtypes_all = ('auto', 'float32', 'float64', 'longdouble')


# Invalid arguments.

non_seq_params = (1, 3.3, False, None, numpy.float32(3), ..., slice(3),
                  set(), frozenset(), dict())
non_bool_params = (1, 291.1, None, numpy.float32(1.3), ..., slice(5),
                   [], (), set(), dict())
non_int_params = (-1.3, None, ..., slice(9), [], (), set(), dict(),
                  gmpy2.mpq(3, 2), numpy.float64(-3),
                  numpy.int32((3, 1)))
non_real_params = (complex(1, 2), complex(1, 0), None, ..., slice(4),
                   [], (), set(), dict())
non_real_params_numpy = (complex(1, 2), complex(1, 0), ..., slice(4),
                         [], (), set(), dict())


# gmpy2 is missing.

def test_rational_gmpy2_missing():
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    old_value = cfr._has_gmpy2
    try:
        cfr._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfr.n_inf(beta, alpha, gamma)
    except:
        raise
    finally:
        cfr._has_gmpy2 = old_value


def test_multi_gmpy2_missing():
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    old_value = cfm._has_gmpy2
    try:
        cfm._has_gmpy2 = False
        with pytest.raises(ImportError):
            cfm.n_inf(beta, alpha, gamma)
    except:
        raise
    finally:
        cfm._has_gmpy2 = old_value


# non-real alpha

@pytest.mark.parametrize('try_libpmadra,dtype,alpha',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_alpha_nonnumber(try_libpmadra, dtype, alpha):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.n_inf(beta, alpha, gamma, dtype=dtype,
                 try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('alpha', non_real_params)
def test_rational_alpha_nonnumber(alpha):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('alpha', non_real_params)
def test_multi_alpha_nonnumber(alpha):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma)


# non-real gamma

@pytest.mark.parametrize('try_libpmadra,dtype,gamma',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_gamma_nonnumber(try_libpmadra, dtype, gamma):
    beta = rng.random((random.randint(2, 100), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.n_inf(beta, alpha, gamma, dtype=dtype,
                 try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('gamma', non_real_params)
def test_rational_gamma_nonnumber(gamma):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('gamma', non_real_params)
def test_multi_gamma_nonnumber(gamma):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma)


# non-real element of beta

@pytest.mark.filterwarnings('ignore')
@pytest.mark.parametrize('try_libpmadra,dtype,element',
                         [(t, d, v)
                          for t in (False, True)
                          for d in ('auto', 'float32')
                          for v in non_real_params_numpy])
def test_beta_hasnonreal(try_libpmadra, dtype, element):
    beta = rng.random((random.randint(2, 100), ), 'float32').tolist()
    beta[random.randrange(len(beta))] = element
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.n_inf(beta, alpha, gamma, dtype=dtype,
                 try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('element', non_real_params)
def test_rational_beta_hasnonreal(element):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    beta[random.randrange(len(beta))] = element
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('element', non_real_params)
def test_multi_beta_hasnonreal(element):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 40))]
    beta[random.randrange(len(beta))] = element
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma)


# complex beta

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, d) for v in (False, True)
                          for d in ('complex64', 'complex128')])
def test_beta_complex(try_libpmadra, dtype):
    beta = rng.random((random.randint(1, 40), ), 'float32').astype(dtype)
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises((TypeError, ValueError)):
        cf.n_inf(beta, alpha, gamma, dtype='auto',
                 try_libpmadra=try_libpmadra)


# non-Sequence beta

@pytest.mark.parametrize('try_libpmadra,dtype,beta',
                         [(t, d, b)
                          for t in (True, False)
                          for d in ('auto', 'float32')
                          for b in non_seq_params])
def test_beta_nonseq(try_libpmadra, dtype, beta):
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.n_inf(beta, alpha, gamma, dtype=dtype,
                 try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('beta', non_seq_params)
def test_multi_beta_nonseq(beta):
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('beta', non_seq_params)
def test_rational_beta_nonseq(beta):
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfr.n_inf(beta, alpha, gamma)


# NaN and -inf alpha, and +inf for the rational versions.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_alpha_nan_or_ninf(try_libpmadra, value):
    alpha = float(value)
    gamma = rng.random((1, ), 'float32')[0]
    beta = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(ValueError):
        cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_alpha_nan_or_inf(value):
    alpha = float(value)
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    with pytest.raises(ValueError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_alpha_nan_or_ninf(value):
    alpha = float(value)
    gamma = float(rng.random((1, ), 'float32')[0])
    beta = rng.random((random.randint(2, 100), ), 'float32').tolist()
    with pytest.raises(ValueError):
        cfm.n_inf(beta, alpha, gamma)


# NaN and -inf gamma, and +inf for the rational versions.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_gamma_nan_or_ninf(try_libpmadra, value):
    gamma = float(value)
    alpha = rng.random((1, ), 'float32')[0]
    beta = rng.random((random.randint(2, 100), ), 'float32')
    with pytest.raises(ValueError):
        cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_gamma_nan_or_inf(value):
    gamma = float(value)
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    with pytest.raises(ValueError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_gamma_nan_or_ninf(value):
    gamma = float(value)
    alpha = float(rng.random((1, ), 'float32')[0])
    beta = rng.random((random.randint(2, 100), ), 'float32').tolist()
    with pytest.raises(ValueError):
        cfm.n_inf(beta, alpha, gamma)


# NaN or -inf element of beta, and +inf for the rational versions.

@pytest.mark.parametrize('try_libpmadra,value',
                         [(t, v) for t in (True, False)
                          for v in ('nan', '-inf')])
def test_beta_element_nan_or_ninf(try_libpmadra, value):
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    beta = rng.random((random.randint(2, 100), ), 'float32')
    beta[random.randrange(len(beta))] = float(value)
    with pytest.raises(ValueError):
        cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


@pytest.mark.parametrize('value', ('nan', '-inf', 'inf'))
def test_rational_beta_element_nan_or_inf(value):
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    beta[random.randrange(len(beta))] = float(value)
    with pytest.raises(ValueError):
        cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('value', ('nan', '-inf'))
def test_multi_beta_element_nan_or_ninf(value):
    alpha = float(rng.random((1, ), 'float32')[0])
    gamma = float(rng.random((1, ), 'float32')[0])
    beta = rng.random((random.randint(2, 100), ), 'float32').tolist()
    beta[random.randrange(len(beta))] = float(value)
    with pytest.raises(ValueError):
        cfm.n_inf(beta, alpha, gamma)


# Negative alpha.

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_alpha_negative(try_libpmadra):
    for _ in range(10):
        alpha = -100 * rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        beta = rng.random((random.randint(2, 100), ), 'float32')
        with pytest.raises(ValueError):
            cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_alpha_negative():
    for _ in range(10):
        alpha = gmpy2.mpq(random.randint(-10000, -1),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        beta = [gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
                for _ in range(random.randint(1, 30))]
        with pytest.raises(ValueError):
            cfr.n_inf(beta, alpha, gamma)


def test_multi_alpha_negative():
    for _ in range(10):
        alpha = float(-100 * rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        beta = rng.random((random.randint(2, 100), ),
                          'float32').tolist()
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma)


# Negative alpha.

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_gamma_negative(try_libpmadra):
    for _ in range(10):
        alpha = rng.random((1, ), 'float32')[0]
        gamma = -100 * rng.random((1, ), 'float32')[0]
        beta = rng.random((random.randint(2, 100), ), 'float32')
        with pytest.raises(ValueError):
            cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_gamma_negative():
    for _ in range(10):
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(-10000, -1),
                          random.randint(1, 100))
        beta = [gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
                for _ in range(random.randint(1, 30))]
        with pytest.raises(ValueError):
            cfr.n_inf(beta, alpha, gamma)


def test_multi_gamma_negative():
    for _ in range(10):
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(-100 * rng.random((1, ), 'float32')[0])
        beta = rng.random((random.randint(2, 100), ),
                          'float32').tolist()
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma)


# Negative element of beta.

@pytest.mark.parametrize('try_libpmadra', (True, False))
def test_beta_element_negative(try_libpmadra):
    for _ in range(10):
        alpha = rng.random((1, ), 'float32')[0]
        gamma = rng.random((1, ), 'float32')[0]
        beta = rng.random((random.randint(2, 100), ), 'float32')
        beta[random.randrange(len(beta))] *= -1
        with pytest.raises(ValueError):
            cf.n_inf(beta, alpha, gamma, try_libpmadra=try_libpmadra)


def test_rational_beta_element_negative():
    for _ in range(10):
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        beta = [gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
                for _ in range(random.randint(1, 30))]
        beta[random.randrange(len(beta))] *= -1
        with pytest.raises(ValueError):
            cfr.n_inf(beta, alpha, gamma)


def test_multi_beta_element_negative():
    for _ in range(10):
        alpha = float(rng.random((1, ), 'float32')[0])
        gamma = float(rng.random((1, ), 'float32')[0])
        beta = rng.random((random.randint(2, 100), ),
                          'float32').tolist()
        beta[random.randrange(len(beta))] *= -1
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma)


# non-int precision and emax

@pytest.mark.parametrize('precision', non_int_params)
def test_multi_precision_nonint(precision):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma, precision=precision)


@pytest.mark.parametrize('emax', non_int_params)
def test_multi_emax_nonint(emax):
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    with pytest.raises(TypeError):
        cfm.n_inf(beta, alpha, gamma, emax=emax)


# precision and emax too small

def test_multi_precision_nonpositive():
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for precision in range(0, -10, -1):
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma, precision=precision)
    for _ in range(1000):
        precision = random.randint(-2**63 + 1, 0)
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma, precision=precision)


def test_multi_emax_lessthantwo():
    beta = [gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
            for _ in range(random.randint(1, 30))]
    alpha = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    gamma = gmpy2.mpq(random.randint(1, 100), random.randint(1, 100))
    for emax in range(1, -10, -1):
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma, emax=emax)
    for _ in range(1000):
        emax = random.randint(-2**63 + 1, 1)
        with pytest.raises(ValueError):
            cfm.n_inf(beta, alpha, gamma, emax=emax)


# non-bool libpmadra

@pytest.mark.parametrize('try_libpmadra', non_bool_params)
def test_try_libpmadra_nonbool(try_libpmadra):
    beta = rng.random((random.randint(2, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(TypeError):
        cf.n_inf(beta, alpha, gamma, dtype='auto',
                 try_libpmadra=try_libpmadra)


# invalid dtype

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(v, k)
                          for v in (False, True)
                          for k in ('uint8', 'uint16', 'uint32',
                                    'uint64', 'int8', 'int16', 'int32',
                                    'int64', 'float96', 'float128',
                                    'complex32', 'complex64',
                                    'complex128', 'complex192',
                                    'complex256', 'bool', 'object')])
def test_dtype_invalid(try_libpmadra, dtype):
    beta = rng.random((random.randint(2, 40), ), 'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.n_inf(beta, alpha, gamma, dtype=dtype,
                 try_libpmadra=try_libpmadra)


# beta has too many axes

@pytest.mark.parametrize('try_libpmadra,ndim',
                         [(t, i) for t in (True, False)
                          for i in range(2, 6)])
def test_beta_ndim_toohigh(try_libpmadra, ndim):
    beta = rng.random([random.randint(1, 3) for i in range(ndim)],
                      'float32')
    alpha = rng.random((1, ), 'float32')[0]
    gamma = rng.random((1, ), 'float32')[0]
    with pytest.raises(ValueError):
        cf.n_inf(beta, alpha, gamma, dtype='auto',
                 try_libpmadra=try_libpmadra)


# gamma = 0 gives beta / alpha

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(False, v) for v in dtypes_all]
                         + [(True, v) for v in dtypes_all])
def test_gamma_zero(try_libpmadra, dtype):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dtype not in ('float32', 'float64'):
            beta = tp(rng.random((Mc, ), 'float64'))
            alpha = tp(rng.random((1, ), 'float64')[0])
        else:
            beta = rng.random((Mc, ), dtype)
            alpha = rng.random((1, ), dtype)[0]
        gamma = tp(0)
        ninf = cf.n_inf(numpy.atleast_1d(beta), alpha, gamma,
                        dtype=dtype, try_libpmadra=try_libpmadra)
        eps = numpy.finfo(tp).eps
        numpy.testing.assert_allclose(ninf, alpha**-1 * beta,
                                      atol=2 * eps, rtol=2 * eps)


def test_rational_gamma_zero():
    for _ in range(10):
        Mc = random.randint(1, 100)
        beta = [gmpy2.mpq(random.randint(0, 100),
                          random.randint(1, 100))
                for _ in range(Mc)]
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(0)
        ninf = cfr.n_inf(beta, alpha, gamma)
        assert isinstance(ninf, list)
        assert ninf == [v / alpha for v in beta]


@pytest.mark.parametrize('precision,emax',
                         [(p, e) for p in (2, 8, 16, 30, 64, 128, 280)
                          for e in (128, 256, 1024, 2000)])
def test_multi_gamma_zero(precision, emax):
    for _ in range(10):
        Mc = random.randint(1, 100)
        with gmpy2.local_context(gmpy2.context(), precision=precision,
                                 emax=emax, emin=1 - emax) as ctx:
            beta = [gmpy2.mpfr_random(gmpy2rng) for _ in range(Mc)]
            alpha = gmpy2.mpfr_random(gmpy2rng)
            gamma = gmpy2.mpfr(0)
            ninf = cfm.n_inf(beta, alpha, gamma, precision=precision,
                             emax=emax)
            assert isinstance(ninf, list)
            assert ninf == [v / alpha for v in beta]


# alpha = 0 and gamma = 0 gives infinity or raises a ValueError
# for the rational case.

@pytest.mark.parametrize('try_libpmadra,dtype',
                         [(False, v) for v in dtypes_all]
                         + [(True, v) for v in dtypes_all])
def test_alpha_gamma_zero(try_libpmadra, dtype):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        if dtype not in ('float32', 'float64'):
            beta = tp(rng.random((Mc, ), 'float64'))
        else:
            beta = rng.random((Mc, ), dtype)
        alpha = tp(0)
        gamma = tp(0)
        ninf = cf.n_inf(numpy.atleast_1d(beta), alpha, gamma,
                        dtype=dtype, try_libpmadra=try_libpmadra)
        assert numpy.all(numpy.isinf(ninf) & (ninf > 0))


def test_rational_alpha_gamma_zero():
    for _ in range(10):
        Mc = random.randint(1, 100)
        beta = [gmpy2.mpq(random.randint(0, 100),
                          random.randint(1, 100))
                for _ in range(Mc)]
        alpha = gmpy2.mpq(0)
        gamma = gmpy2.mpq(0)
        with pytest.raises(ValueError):
            cfr.n_inf(beta, alpha, gamma)


@pytest.mark.parametrize('precision,emax',
                         [(p, e) for p in (2, 8, 16, 30, 64, 128, 280)
                          for e in (128, 256, 1024, 2000)])
def test_multi_alpha_gamma_zero(precision, emax):
    for _ in range(10):
        Mc = random.randint(1, 100)
        with gmpy2.local_context(gmpy2.context(), precision=precision,
                                 emax=emax, emin=1 - emax) as ctx:
            beta = [gmpy2.mpfr_random(gmpy2rng) for _ in range(Mc)]
            alpha = gmpy2.mpfr(0)
            gamma = gmpy2.mpfr(0)
            ninf = cfm.n_inf(beta, alpha, gamma, precision=precision,
                             emax=emax)
            assert all([gmpy2.is_infinite(v) and not gmpy2.is_signed(v)
                        for v in ninf])


# beta_k == 0 for all k gives zero

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_beta_zero(try_libpmadra, dtype, alpha, gamma):
    for i in range(10):
        Mc = random.randint(1, 100)
        if dtype == 'auto':
            tp = (numpy.float32, numpy.float64, numpy.longdouble)[i % 3]
        else:
            tp = numpy.dtype(dtype).type
        beta = numpy.zeros((Mc, ), dtype=tp)
        if alpha == 'zero':
            alph = tp(0)
        else:
            alph = tp(rng.random((1, ), 'float64')[0])
        if gamma == 'zero':
            gamm = tp(0)
        else:
            gamm = tp(rng.random((1, ), 'float64')[0])
        ninf = cf.n_inf(numpy.atleast_1d(beta), alph, gamm,
                        dtype=dtype, try_libpmadra=try_libpmadra)
        numpy.testing.assert_equal(ninf, 0)


@pytest.mark.parametrize('alpha,gamma',
                         [(a, g)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if a != 'zero' or g != 'zero'])
def test_rational_beta_zero(alpha, gamma):
    for _ in range(10):
        Mc = random.randint(1, 100)
        beta = Mc * [gmpy2.mpq(0)]
        if alpha == 'zero':
            alph = gmpy2.mpq(0)
        else:
            alph = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        if gamma == 'zero':
            gamm = gmpy2.mpq(0)
        else:
            gamm = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        ninf = cfr.n_inf(beta, alph, gamm)
        assert ninf == beta


@pytest.mark.parametrize('precision,emax,alpha,gamma',
                         [(p, e, a, g)
                          for p in (2, 8, 16, 30, 64, 128, 280)
                          for e in (128, 256, 1024, 2000)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_beta_zero(precision, emax, alpha, gamma):
    for _ in range(10):
        Mc = random.randint(1, 100)
        with gmpy2.local_context(gmpy2.context(), precision=precision,
                                 emax=emax, emin=1 - emax) as ctx:
            beta = Mc * [gmpy2.mpfr(0)]
            if alpha == 'zero':
                alph = gmpy2.mpfr(0)
            else:
                alph = gmpy2.mpfr_random(gmpy2rng)
            if gamma == 'zero':
                gamm = gmpy2.mpfr(0)
            else:
                gamm = gmpy2.mpfr_random(gmpy2rng)
            ninf = cfm.n_inf(beta, alph, gamm, precision=precision,
                             emax=emax)
            assert ninf == beta


# Scaling alpha and gamma.

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_scaling_alpha_gamma(try_libpmadra, dtype, alpha, gamma):
    if dtype == 'float32':
        tol = 1e3 * numpy.finfo('float32').eps
    else:
        tol = 1e3 * numpy.finfo('float64').eps
    for _ in range(10):
        beta = rng.random((random.randint(1, 100), ), 'float64')
        if alpha == 'zero':
            alph = numpy.float32(0)
        else:
            alph = rng.random((1, ), 'float64')[0]
        if gamma == 'zero':
            gamm = numpy.float32(0)
        else:
            gamm = rng.random((1, ), 'float64')[0]
        ninf = cf.n_inf(numpy.atleast_1d(beta), alph, gamm,
                        dtype=dtype, try_libpmadra=try_libpmadra)
        for _ in range(10):
            scale = 10.0 ** (-3 + 5 * rng.random((1, ), 'float64')[0])
            ninf_t = cf.n_inf(numpy.atleast_1d(beta), scale * alph,
                              scale * gamm,
                              dtype=dtype, try_libpmadra=try_libpmadra)
            assert ninf.shape == ninf_t.shape
            assert ninf.dtype == ninf_t.dtype
            assert_allclose(ninf / scale, ninf_t, rtol=tol, atol=0)


@pytest.mark.parametrize('alpha,gamma',
                         [(a, g)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if a != 'zero' or g != 'zero'])
def test_rational_scaling_alpha_gamma(alpha, gamma):
    for _ in range(10):
        beta = [gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
                for _ in range(random.randint(1, 100))]
        if alpha == 'zero':
            alph = gmpy2.mpq(0)
        else:
            alph = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        if gamma == 'zero':
            gamm = gmpy2.mpq(0)
        else:
            gamm = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        ninf = cfr.n_inf(beta, alph, gamm)
        for _ in range(10):
            scale = gmpy2.mpq(random.randint(1, 100),
                              random.randint(1, 100))
            ninf_t = cfr.n_inf(beta, scale * alph, scale * gamm)
            assert [v / scale for v in ninf] == ninf_t


@pytest.mark.parametrize('precision,alpha,gamma',
                         [(p, a, g)
                          for p in (80, 130, 240)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_scaling_alpha_gamma(precision, alpha, gamma):
    for _ in range(10):
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(random.randint(1, 100))]
        if alpha == 'zero':
            alph = gmpy2.mpfr(0)
        else:
            alph = gmpy2.mpfr_random(gmpy2rng)
        if gamma == 'zero':
            gamm = gmpy2.mpfr(0)
        else:
            gamm = gmpy2.mpfr_random(gmpy2rng)
        ninf = cfm.n_inf(beta, alph, gamm, precision=precision)
        for _ in range(10):
            scale = gmpy2.mpfr(10.0) ** (-3 + 5 * gmpy2.mpfr_random(gmpy2rng))
            ninf_t = cfm.n_inf(beta, scale * alph, scale * gamm,
                               precision=precision)
            assert_allclose(numpy.longdouble([v / scale for v in ninf]),
                            numpy.longdouble(ninf_t), rtol=1e-9, atol=0)


# Scaling beta.

@pytest.mark.parametrize('try_libpmadra,dtype,alpha,gamma',
                         [(t, d, a, g)
                          for t in (False, True)
                          for d in dtypes_all
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_scaling_beta(try_libpmadra, dtype, alpha, gamma):
    if dtype == 'float32':
        tol = 1e3 * numpy.finfo('float32').eps
    else:
        tol = 1e3 * numpy.finfo('float64').eps
    for _ in range(10):
        beta = rng.random((random.randint(1, 100), ), 'float64')
        if alpha == 'zero':
            alph = numpy.float32(0)
        else:
            alph = rng.random((1, ), 'float64')[0]
        if gamma == 'zero':
            gamm = numpy.float32(0)
        else:
            gamm = rng.random((1, ), 'float64')[0]
        ninf = cf.n_inf(numpy.atleast_1d(beta), alph, gamm,
                        dtype=dtype, try_libpmadra=try_libpmadra)
        for _ in range(10):
            scale = 10.0 ** (-3 + 5 * rng.random((1, ), 'float64')[0])
            ninf_t = cf.n_inf(scale * numpy.atleast_1d(beta),
                              alph, gamm,
                              dtype=dtype, try_libpmadra=try_libpmadra)
            assert ninf.shape == ninf_t.shape
            assert ninf.dtype == ninf_t.dtype
            assert_allclose(scale * ninf, ninf_t, rtol=tol, atol=0)


@pytest.mark.parametrize('alpha,gamma',
                         [(a, g)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')
                          if a != 'zero' or g != 'zero'])
def test_rational_scaling_beta(alpha, gamma):
    for _ in range(10):
        beta = [gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
                for _ in range(random.randint(1, 100))]
        if alpha == 'zero':
            alph = gmpy2.mpq(0)
        else:
            alph = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        if gamma == 'zero':
            gamm = gmpy2.mpq(0)
        else:
            gamm = gmpy2.mpq(random.randint(1, 100),
                             random.randint(1, 100))
        ninf = cfr.n_inf(beta, alph, gamm)
        for _ in range(10):
            scale = gmpy2.mpq(random.randint(1, 100),
                              random.randint(1, 100))
            ninf_t = cfr.n_inf([scale * v for v in beta], alph, gamm)
            assert [scale * v for v in ninf] == ninf_t


@pytest.mark.parametrize('precision,alpha,gamma',
                         [(p, a, g)
                          for p in (80, 130, 240)
                          for a in ('zero', 'positive')
                          for g in ('zero', 'positive')])
def test_multi_scaling_beta(precision, alpha, gamma):
    for _ in range(10):
        beta = [gmpy2.mpfr_random(gmpy2rng)
                for _ in range(random.randint(1, 100))]
        if alpha == 'zero':
            alph = gmpy2.mpfr(0)
        else:
            alph = gmpy2.mpfr_random(gmpy2rng)
        if gamma == 'zero':
            gamm = gmpy2.mpfr(0)
        else:
            gamm = gmpy2.mpfr_random(gmpy2rng)
        ninf = cfm.n_inf(beta, alph, gamm, precision=precision)
        for _ in range(10):
            scale = gmpy2.mpfr(10.0) ** (-3 + 5 * gmpy2.mpfr_random(gmpy2rng))
            ninf_t = cfm.n_inf([scale * v for v in beta], alph, gamm,
                               precision=precision)
            assert_allclose(numpy.longdouble([scale * v for v in ninf]),
                            numpy.longdouble(ninf_t), rtol=1e-9, atol=0)


# Do a cross comparison between all functions calculating ninf.
def test_compare_all():
    for _ in range(10):
        Mc = random.randint(1, 30)
        beta = [gmpy2.mpq(random.randint(0, 100),
                          random.randint(1, 100))
                for _ in range(Mc)]
        alpha = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        gamma = gmpy2.mpq(random.randint(1, 100),
                          random.randint(1, 100))
        # Get the ninf in masse from const_coeff.n_inf for
        # all dtypes with and without libpmadra and check that they
        # agree with each other.
        by_p = {v: cf.n_inf(beta, numpy.dtype(v).type(alpha),
                            numpy.dtype(v).type(gamma), dtype=v,
                            try_libpmadra=False)
                for v in dtypes}
        by_f = {v: cf.n_inf(beta, numpy.dtype(v).type(alpha),
                            numpy.dtype(v).type(gamma), dtype=v,
                            try_libpmadra=True)
                for v in dtypes}
        for k in dtypes:
            ninf_p = by_p[k]
            ninf_f = by_f[k]
            # Check shape.
            if Mc == 1:
                assert ninf_p.shape == (1, )
                assert ninf_f.shape == (1, )
            else:
                assert ninf_p.shape == (Mc, )
                assert ninf_f.shape == (Mc, )
            # Check dtype
            assert ninf_p.dtype == numpy.dtype(k)
            assert ninf_f.dtype == numpy.dtype(k)
            # Check that the values are close.
            assert_allclose(ninf_p, ninf_f,
                            rtol=1e-5, atol=1e-7)
            if k != 'longdouble':
                assert_allclose(
                    ninf_f, by_f['longdouble'],
                    rtol=1e-5, atol=1e-7)
            # Check that the ninfs are all non-negative.
            assert numpy.all(ninf_p >= 0.0)
            assert numpy.all(ninf_f >= 0.0)
        # Check the exact solution and compare it.
        ninf_p = by_p['longdouble']
        ninf_r = cfr.n_inf(beta, alpha, gamma)
        assert isinstance(ninf_r, list)
        assert all([v >= 0 for v in ninf_r])
        ninf_r_ld = numpy.longdouble(ninf_r)
        assert_allclose(ninf_r_ld, ninf_p, rtol=1e-8, atol=1e-8)
        # Do the multi-precision comparisons.
        for precision in (40, 60, 200):
            for emax in (128, 800):
                ninf_m = cfm.n_inf(beta, alpha, gamma,
                                   precision=precision, emax=emax)
                assert isinstance(ninf_m, list)
                assert all([v >= 0.0 for v in ninf_m])
                assert_allclose(ninf_r_ld, numpy.longdouble(ninf_m),
                                rtol=1e-8, atol=1e-8)
