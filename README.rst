Overview
========

Python package for Poly-Multiplicity Airborne Disease Risk Assessment (PMADRA).
This package is for calculating risk assessments for airborne diseases for
aerosols with potentially more than one pathogen copy (poly-multiplicity) in
them following the model described by

    F Nordsiek, E Bodenschatz, & G Bagheri. *Risk assessment for airborne disease
    transmission by poly-pathogen aerosols*. [Pre-print] (2020)
    `doi:10.1101/2020.11.30.20241083 <https://doi.org/10.1101/2020.11.30.20241083>`_
    `arXiv:2011.14118 <https://arxiv.org/abs/2011.14118>`_

This software was written by the Laboratory for Fluid Physics and Biocomplexity
(LFPB) of the Max-Planck-Insitut für Dynamik und Selbstorganisation (Max Planck
Institute for Dynamics and Self-Organization) and is provided under the MIT
license (see :file:`COPYING.txt`).


Dependencies
============

Only Python 3 is supported, with the exact version mostly depending on the
requirements of the dependencies.

The following Python packages are required for operation

* `numpy <https://pypi.org/project/numpy>`_ >= 1.17
* `scipy <https://pypi.org/project/scipy>`_ >= 0.18
* `setuptools <https://pypi.org/project/setuptools>`_ >= 36.6

The following Python packages and Fortran libraries are optional.

* `gmpy2 <https://pypi.org/project/gmpy2>`_ >= 2.0, which is needed to
  calculate quantities for coefficients constant in time in multi-precision
  arithmetic or exact rational arithmetic for a smaller subset of quantities.
* `libpmadra <https://gitlab.gwdg.de/mpids-lfpn-public/pmadra/libpmadra>`_
  version 0.2.x which can accelerate many operations, some by more than a
  factor of 100. If it is not available, slower python versions of its
  functions are used.

The following Python package is required for building

* `setuptools <https://pypi.org/project/setuptools>`_ >= 36.6
* `wheel <https://pypi.org/project/wheel>`_


Installation
============

The package's dependencies can be installed by::

    pip install -r requirements.txt

or if the optional dependeicies are to also be installed::

    pip install -r requirements_optional.txt

Then, to install the package, run either::

    pip install .

or, using the legacy :file:`setup.py` script::

    python setup.py install


Unit Tests
==========

The unit tests require all `numpy <https://pypi.org/project/numpy>`_ >= 1.5.0,
all optional dependencies except for
`libpmadra <https://gitlab.gwdg.de/mpids-lfpn-public/pmadra/libpmadra>`_, and
the following additional Python packages

* `pytest <https://pypi.org/project/pytest>`_ >= 5.0
* `pytest-cov <https://pypi.org/project/pytest-cov>`_ >= 2.7

The package's Python dependencies can be installed by::

    pip install -r requirements_tests.txt

The tests themselves are run by the command::

    pytest-3


.. note::

   Sometimes a comparison unit test fails due to the strictness of the
   tolerances chosen and the number of trials run.


Documentation
=============

The documentation requires the following packages

* `Sphinx <https://pypi.org/project/Sphinx>`_ >= 1.4
* `sphinx_rtd_theme <https://pypi.org/project/sphinx_rtd_theme>`_

The required dependencies to build it can be installed from the main
directory with the command::

    pip install -r requirements_doc.txt

To build the HTML documentation, run either::

    sphinx-build doc/source doc/build/html

or, using the legacy :file:`setup.py` script::

    python setup.py build_sphinx

Documentation is put in the ``doc/build`` subdirectory. HTML
documentation is the default generated.


Version History
===============

0.3 (2021-02-23)
----------------

Updated the build system and removed all use of the ``distutils`` module from
the standard library since it is now deprecated (:pep:`632`). Additionally,
the duplicate ``numpy`` requirement in :file:`requirements_tests.txt` was
removed.

The build system was changed by moving all information in :file:`setup.py` over
:file:`pyproject.toml` and :file:`setup.cfg` and now requires
`setuptools <https://pypi.org/project/setuptools>`_ >= 36.6 since :pep:`517` and
:pep:`518` support are now required. :file:`setup.py` has been reduced to the
very minimum required to still support use int he traditional fashion.

``pkg_resources.parse_version`` in
`setuptools <https://pypi.org/project/setuptools>`_  is now used to parse
version strings rather than using the ``distutils.version.StrictVersion``
class directly. In this way, the dependency on ``distutils`` is removed.


0.2.1 (2021-02-05)
------------------

Fixed bug where ``intn`` could be negative due to numerical precision and
rounding for low ``k`` when ``n`` is small (all the infectious aerosol
production is at much higher ``k``). It was fixed by setting these negative
elements to zero.

0.2 (2021-01-11)
----------------

Main API solidified. Compatible only with libpmadra version 0.2.x.

0.1
---

Initial development version with many changes.
