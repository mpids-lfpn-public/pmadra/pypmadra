# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Bindings to the Fortran library libpmadra.

Wrapper functions are made for each proceedure with automatic type
dispatch including automatically determining a suitable type. The
wrappers also convert the arguments and allocate any output arguments.

Note
----
All arrays are made to have Fortran ordering and be Fortran contiguous.

"""

import collections.abc
import ctypes
import ctypes.util
import numbers

from pkg_resources import parse_version

import numpy
import numpy.ctypeslib

# We need to get the gmpy2 number classes, if possible.
try:
    import gmpy2
    _gmpy2_real_classes = (gmpy2.mpz(0).__class__,
                           gmpy2.mpq(0).__class__,
                           gmpy2.mpfr(0).__class__)
    _gmpy2_int_class = (gmpy2.mpz(0).__class__, )
except:
    _gmpy2_real_classes = ()
    _gmpy2_int_class = ()

# Declare the classes for integers and reals.
int_classes = tuple([int, bool, numpy.integer, numpy.bool_,
                     numbers.Integral] + list(_gmpy2_int_class))
real_classes = tuple([int, float, bool, numpy.floating,
                      numpy.integer, numpy.bool_, numbers.Real]
                     + list(_gmpy2_real_classes))


def scalar_array_like_dims(arr):
    """ Determine the number of dimensions of a scalar or array-like.

    Warning
    -------
    Ragged arrays will not cause an error to be raised. This function
    just goes down the first axis repeatedly till it can't go any
    further or finds a ``numpy.ndarray``.

    Parameters
    ----------
    arr : scalar or array-like
        The object to check.

    Returns
    -------
    ndim : int
        The number of dimensions.

    Raises
    ------
    TypeError
        If `arr` is not a scalar or array-like.

    """
    # We will essentially recurse down the first element of first axis
    # (starting with the whole of arr) till we run into a scalar (done),
    # and ndarray (done), an empty Sequence (done), a non-empty Sequence
    # (continue with first element), or something else (error).
    ndim = 0
    while True:
        if isinstance(arr, real_classes):
            return ndim
        if isinstance(arr, numpy.ndarray):
            return ndim + arr.ndim
        if isinstance(arr, (str, bytes, numpy.flexible)):
            raise TypeError('arr is not a scalar or array-like.')
        if isinstance(arr, (list, tuple, collections.abc.Sequence)):
            if len(arr) == 0:
                return ndim + 1
            arr = arr[0]
            ndim += 1
        else:
            raise TypeError('arr is not a scalar or array-like.')


# The dtypes that the libpmadra procedures accept.
_accepted_dtype_names = {'float32', 'float64', 'longdouble'}
_accepted_dtypes = {numpy.dtype(v) for v in _accepted_dtype_names}

# Make a lookup from dtypes to dtype names, and another from dtype names
# to ctypes.
_dtype_lookup = {numpy.dtype(k): k
                 for k in ('float32', 'float64', 'longdouble')}
_ctypes_lookup = {'float32': ctypes.c_float,
                  'float64': ctypes.c_double,
                  'longdouble': ctypes.c_longdouble}

# Make a lookup for the library's format specifiers and back.
_format_specifiers_by_number = {-2: False,
                                -1: None,
                                1: 'float32',
                                2: 'float64',
                                3: 'longdouble',
                                4: 'quad'}
_format_specifiers_by_str = {'auto': 0,
                             'float32': 1,
                             'float64': 2,
                             'longdouble': 3,
                             'quad': 4}


# Go through each dtype and make the mapping of argument and return
# names to types for the procedures, all of which have consistent
# declarations.
_argtypes = dict()
_returntypes = dict()
for dtype, ctype in _ctypes_lookup.items():
    # Declare the commonly used array types for both input and output.
    int_ptr = ctypes.POINTER(ctypes.c_int)
    one_d_array_in = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=1, flags='C_CONTIGUOUS')
    two_d_array_in = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=2, flags='C_CONTIGUOUS')
    three_d_array_in = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=3, flags='C_CONTIGUOUS')
    one_d_array_out = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=1, flags=('C_CONTIGUOUS', 'WRITEABLE'))
    two_d_array_out = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=2, flags=('C_CONTIGUOUS', 'WRITEABLE'))
    three_d_array_out = numpy.ctypeslib.ndpointer(
        dtype=dtype, ndim=3, flags=('C_CONTIGUOUS', 'WRITEABLE'))
    # Make the argument and return types.
    _argtypes[dtype] = {'Mc': ctypes.c_int,
                        'Mc_folded': ctypes.c_int,
                        'ny': ctypes.c_int,
                        'nx': ctypes.c_int,
                        'nt': ctypes.c_int,
                        'nmu': ctypes.c_int,
                        'k': ctypes.c_int,
                        'use_format': ctypes.c_int,
                        'desired_format': ctypes.c_int,
                        'emax_safety_factor': ctype,
                        'status': int_ptr,
                        'format_used': int_ptr,
                        'compute_by_k': ctypes.c_bool,
                        'validate_arrays': ctypes.c_bool,
                        't0': ctype,
                        'alpha': ctype,
                        'gamma': ctype,
                        'beta': one_d_array_in,
                        'n0': one_d_array_in,
                        'intn0': one_d_array_in,
                        't': one_d_array_in,
                        'ninf_out': one_d_array_out,
                        'n_out': two_d_array_out,
                        'intn_out': two_d_array_out,
                        'x': one_d_array_in,
                        'folded_fraction_out': one_d_array_out,
                        'y': two_d_array_in,
                        'y_folded_out': two_d_array_out,
                        'vk_in': two_d_array_in,
                        'vks_in': three_d_array_in,
                        'vk_out': two_d_array_out,
                        'vks_out': three_d_array_out,
                        'uk_in': two_d_array_in,
                        'uk_before': two_d_array_in,
                        'uks_in': three_d_array_in,
                        'uk_out': two_d_array_out,
                        'uks_out': three_d_array_out,
                        'uk_one_in': one_d_array_in,
                        'uk_one_before': one_d_array_in,
                        'uks_one_in': two_d_array_in,
                        'uk_one_out': one_d_array_out,
                        'uks_one_out': two_d_array_out,
                        'wk_in': two_d_array_in,
                        'wk_before': two_d_array_in,
                        'wk_out': two_d_array_out,
                        'wks_out': three_d_array_out,
                        'mus_in': two_d_array_in,
                        'risks_out': one_d_array_out,
                        'r': ctype}
    _returntypes[dtype] = {'emax': ctype,
                           'required_format': ctypes.c_int,
                           'Mc_folded_required': ctypes.c_int}

# Cleanup temporary variables.
del ctype
del dtype
del int_ptr
del one_d_array_in
del two_d_array_in
del three_d_array_in
del one_d_array_out
del two_d_array_out
del three_d_array_out

# Declare the scalar arguments and their types.
_int_args = {'ny', 'nx', 'nt', 'nmu', 'Mc', 'Mc_folded', 'use_format',
             'desired_format', 'format_used', 'status'}
_bool_args = {'compute_by_k', 'validate_arrays'}
_float_args = {'alpha', 'gamma', 't0', 'emax_safety_factor', 'r'}

# Declare the dimensions for array arguments.
_dimension_args = {'ny', 'nx', 'nt', 'nmu', 'Mc'}
_dimension_args_explicit = {'Mc_folded'}
_array_dim_declarations = {'x': ('nx', ),
                           'y': ('ny', 'Mc'),
                           'y_folded_out': ('ny', 'Mc_folded'),
                           'folded_fraction_out': ('ny', ),
                           't': ('nt', ),
                           'beta': ('Mc', ),
                           'n0': ('Mc', ),
                           'intn0': ('Mc', ),
                           'ninf_out': ('Mc', ),
                           'n_out': ('nt', 'Mc'),
                           'intn_out': ('nt', 'Mc'),
                           'vk_in': ('nx', 'ny'),
                           'vks_in': ('nx', 'ny', 'Mc'),
                           'vk_out': ('nx', 'ny'),
                           'vks_out': ('nx', 'ny', 'Mc'),
                           'uk_in': ('nx', 'ny'),
                           'uk_before': ('nx', 'ny'),
                           'uks_in': ('nx', 'ny', 'Mc'),
                           'uk_out': ('nx', 'ny'),
                           'uks_out': ('nx', 'ny', 'Mc'),
                           'uk_one_in': ('ny', ),
                           'uk_one_before': ('ny', ),
                           'uks_one_in': ('ny', 'Mc'),
                           'uk_one_out': ('ny', ),
                           'uks_one_out': ('ny', 'Mc'),
                           'wk_in': ('nx', 'ny'),
                           'wk_before': ('nx', 'ny'),
                           'wk_out': ('nx', 'ny'),
                           'wks_out': ('nx', 'ny', 'Mc'),
                           'mus_in': ('nmu', 'Mc'),
                           'risks_out': ('nmu', )}
_dim_expanders = (numpy.atleast_1d, numpy.atleast_2d, numpy.atleast_3d)


# Declare the procedures (both those that get a precision suffix and
# other ones which don't) with their argument and return names. For each
# procedure name, we use a Sequence of minimum libpmadra version
# (inclusive), maximum libpmadra version (exclusive), their return name,
# and argument names Sequence tuples. The version information is
# important for making these bindings library version aware. None is
# used for as the return name for subroutines. Versions are specified by
# strings. For the other procedures, rather than names being given, the
# types themselves are given.
_declarations = {
    # subroutines
    'fold_Mc': (('0.2', '0.3', None,
                 ('folded_fraction_out', 'y_folded_out', 'y',
                  'Mc_folded', 'Mc', 'ny', 'validate_arrays',
                  'status')), ),
    'vk': (('0.2', '0.3', None,
            ('vk_out', 'y', 'x', 'Mc', 'ny', 'nx', 'k',
             'validate_arrays', 'status')), ),
    'vks': (('0.2', '0.3', None,
             ('vks_out', 'y', 'x', 'Mc', 'ny', 'nx', 'compute_by_k',
              'validate_arrays', 'status')), ),
    'uk_top': (('0.2', '0.3', None,
                ('uk_out', 'y', 'alpha', 'gamma', 'Mc', 'ny', 'nx',
                 'validate_arrays', 'status')), ),
    'uk_next': (('0.2', '0.3', None,
                 ('uk_out', 'uk_before', 'vk_in', 'x', 'alpha', 'gamma',
                  'Mc', 'ny', 'nx', 'k', 'validate_arrays',
                  'status')), ),
    'uk_one_next': (('0.2', '0.3', None,
                     ('uk_one_out', 'uk_one_before', 'y', 'alpha',
                      'gamma', 'Mc', 'ny', 'k', 'validate_arrays',
                      'status')), ),
    'uks': (('0.2', '0.3', None,
             ('uks_out', 'vks_in', 'y', 'x', 'alpha', 'gamma', 'Mc',
              'ny', 'nx', 'validate_arrays', 'status')), ),
    'uks_one': (('0.2', '0.3', None,
                 ('uks_one_out', 'y', 'alpha', 'gamma', 'Mc', 'ny',
                  'validate_arrays', 'status')), ),
    'wk_top': (('0.2', '0.3', None,
                ('wk_out', 'y', 'x', 'alpha', 'gamma', 'Mc', 'ny', 'nx',
                 'validate_arrays', 'status')), ),
    'wk_next': (('0.2', '0.3', None,
                 ('wk_out', 'wk_before', 'uk_in', 'uk_one_in', 'x',
                  'alpha', 'gamma', 'Mc', 'ny', 'nx', 'k',
                  'validate_arrays', 'status')), ),
    'wks': (('0.2', '0.3', None,
             ('wks_out', 'uks_in', 'uks_one_in', 'y', 'x', 'alpha',
              'gamma', 'Mc', 'ny', 'nx', 'validate_arrays',
              'status')), ),
    'ninf': (('0.2', '0.3', None,
              ('ninf_out', 'beta', 'alpha', 'gamma', 'Mc',
               'validate_arrays', 'status')), ),
    'solve_analytical_ninf_n': (
        ('0.2', '0.3', None,
         ('ninf_out', 'n_out', 'n0', 't', 'beta', 'alpha', 'gamma',
          't0', 'Mc', 'nt', 'validate_arrays', 'status')), ),
    'solve_analytical_ninf_n_intn': (
        ('0.2', '0.3', None,
         ('ninf_out', 'n_out', 'intn_out', 'n0', 'intn0', 't', 'beta',
          'alpha', 'gamma', 't0', 'Mc', 'nt', 'validate_arrays',
          'status')), ),
    'auto_solve_analytical_ninf_n': (
        ('0.2', '0.3', None,
         ('ninf_out', 'n_out', 'n0', 't', 'beta', 'alpha', 'gamma',
          't0', 'emax_safety_factor', 'Mc', 'nt', 'use_format',
          'validate_arrays', 'format_used', 'status')), ),
    'auto_solve_analytical_ninf_n_intn': (
        ('0.2', '0.3', None,
         ('ninf_out', 'n_out', 'intn_out', 'n0', 'intn0', 't', 'beta',
          'alpha', 'gamma', 't0', 'emax_safety_factor', 'Mc', 'nt',
          'use_format', 'validate_arrays', 'format_used',
          'status')), ),
    'mean_risk_exponential': (
        ('0.2', '0.3', None,
         ('risks_out', 'mus_in', 'r', 'Mc', 'nmu', 'validate_arrays',
          'status')), ),
    # functions
    'required_emax_vks': (('0.2', '0.3', 'emax',
                           ('y', 'Mc', 'ny', 'validate_arrays')), ),
    'required_emax_solution': (('0.2', '0.3', 'emax',
                                ('n0', 'beta', 'alpha', 'gamma', 'Mc',
                                 'validate_arrays')), ),
    'required_format_solution': (('0.2', '0.3', 'required_format',
                                  ('n0', 'beta', 'alpha', 'gamma',
                                   'emax_safety_factor', 'Mc',
                                   'validate_arrays')), ),
    'required_Mc_folded': (('0.2', '0.3', 'Mc_folded_required',
                            ('n0', 'beta', 'alpha', 'gamma',
                             'emax_safety_factor', 'Mc',
                             'desired_format',
                             'validate_arrays')), )}
_other_declarations = {
    'pmadra_supported_precisions': (
        ('0.2', '0.3', None,
         (ctypes.POINTER(ctypes.c_bool),
          ctypes.POINTER(ctypes.c_bool),
          ctypes.POINTER(ctypes.c_bool),
          ctypes.POINTER(ctypes.c_bool))), )}


# Find the library, if available, and make a flag indicating if it is
# here or not.
lib_name = ctypes.util.find_library('pmadra')
has_lib = (lib_name is not None)


# Load the library and make the function prototype for the
# pmadra_version subroutine and get the version. If there is an error
# loading the library, or the procedure is not available; then we
# effectively do not have the library.

libpmadra_version = None
if has_lib:
    try:
        _lib = ctypes.CDLL(lib_name)
    except:
        has_lib = False
    else:
        if not hasattr(_lib, 'pmadra_version'):
            has_lib = False
        else:
            pmadra_version_proc = ctypes.CFUNCTYPE(
                None, ctypes.POINTER(ctypes.c_int),
                ctypes.POINTER(ctypes.c_int),
                ctypes.POINTER(ctypes.c_int))(('pmadra_version', _lib))
            args = [ctypes.c_int() for _ in range(3)]
            pmadra_version_proc(*[ctypes.byref(v) for v in args])
            libpmadra_version = parse_version(
                '.'.join([str(v.value) for v in args]))
            # Clean up the variables used.
            del pmadra_version_proc
            del args


# If the library is here, load all the procedures for each precision for
# those with multiple precisions available and then load the ones that
# only have single versions. For each one, select the first group whose
# version range matches the library version. We also need to store the
# information (argument and return names) with the precision specific
# procedures, which will all be None if the procedure is not found.
_procedures = {k: (None, None, None) for k in _declarations}
_other_procedures = dict()
if has_lib:
    # Procedures with a different version for each precision.
    for name, versions in _declarations.items():
        for ver_min, ver_max, retname, args in versions:
            if parse_version(ver_min) <= libpmadra_version \
                    and parse_version(ver_max) > libpmadra_version:
                _procedures[name] = (retname, args, dict())
                for suffix, dtype in (('_sp', 'float32'),
                                      ('_dp', 'float64'),
                                      ('_ld', 'longdouble')):
                    funcname = name.lower() + suffix
                    if not hasattr(_lib, funcname):
                        continue
                    if retname is None:
                        rettype = None
                    else:
                        rettype = _returntypes[dtype][retname]
                    argtypes = [_argtypes[dtype][v] for v in args]
                    func = ctypes.CFUNCTYPE(rettype, *argtypes)(
                        (funcname, _lib))
                    _procedures[name][-1][dtype] = func
                # This was the first version range match, so we don't
                # consider any others.
                break
    # Procedures with only a single version.
    for name, versions in _other_declarations.items():
        for ver_min, ver_max, rettype, argtypes in versions:
            if parse_version(ver_min) <= libpmadra_version \
                    and parse_version(ver_max) > libpmadra_version:
                if hasattr(_lib, name):
                    _other_procedures[name] = ctypes.CFUNCTYPE(
                        rettype, *argtypes)((name.lower(), _lib))
                # This was the first version range match, so we don't
                # consider any others.
                break
    # Clean up temporary variables.
    del name
    del versions
    del ver_min
    del ver_max
    del retname
    del args
    del rettype
    del argtypes
    try:
        del suffix
    except:
        pass
    try:
        del dtype
    except:
        pass
    try:
        del funcname
    except:
        pass
    try:
        del func
    except:
        pass


# Make the wrapper function that makes the function that checks the
# arguments, converts the ndarrays and other types as necessary,
# allocates output arrays, etc.
def _make_wrapper(name, output_name, lib_argnames, funcs):
    # If funcs is None, then no suitable version was found and we should
    # just return a function that raises a key error.
    if funcs is None:
        def wrapped(*args, **keywords):
            raise KeyError(name + ' procedures were not found.')
        return wrapped
    # Make a version with all arguments that must be passed as input and
    # all outputs. Dimensions and output variables don't have to be
    # passed.
    if output_name is None:
        outputs = []
    else:
        outputs = [output_name]
    argnames = tuple(
        [v for v in lib_argnames
         if v not in _dimension_args
         and v not in ('status', 'format_used')
         and not v.endswith('_out')])
    outputs = tuple(outputs + [v for v in lib_argnames
                               if v in ('status', 'format_used')
                               or v.endswith('_out')])
    # Get all the argument names that need to be converted to the right
    # dtype, which is everything except k and compute_by_k which must
    # be int and bool respectively.
    args_that_need_dtype = frozenset([v for v in argnames
                                      if v not in {'k', 'compute_by_k',
                                                   'validate_arrays',
                                                   'use_format',
                                                   'desired_format',
                                                   'Mc_folded'}])
    # Get all the bool, integer, and float arguments.
    args_bool = frozenset([v for v in argnames if v in _bool_args])
    args_int = frozenset([v for v in argnames if v in _int_args])
    args_float = frozenset([v for v in argnames if v in _float_args])

    # Make a set of all arguments that should be passed by value, which
    # is all dimension and int, bool, and float input arguments.
    args_by_value = frozenset(list(args_bool) + list(args_int)
                              + list(args_float)
                              + [k for k in lib_argnames
                                 if k in _dimension_args])

    # Get the arguments that have dimensions.
    args_with_dims = frozenset([v for v in argnames
                                if v in _array_dim_declarations])

    # Make the wrapped function that checks the arguments, does
    # conversions, makes the function call, checks status, and returns
    # the outputs.
    def wrapped(*args, dtype='float64'):
        # Check that the dtype is available, or is 'auto' in which case
        # we must determine it.
        if dtype != 'auto':
            if dtype not in _accepted_dtype_names:
                raise ValueError('Invalid dtype.')
            if dtype not in funcs:
                raise KeyError(name + ' not available for ' + dtype)
        # Check that we got the right number of arguments.
        if len(args) != len(argnames):
            raise TypeError(
                '{0} arguments are required but {1} were given'.format(
                    len(argnames), len(args)))
        # Turn the arguments into a dict where we will also do our
        # transformations and add the generated arguments later.
        to_pass = {k: args[i] for i, k in enumerate(argnames)}
        # If use_format or desired_format are present, we must convert
        # their str values to an int.
        for k in ('use_format', 'desired_format'):
            if k in to_pass:
                if to_pass[k] not in _format_specifiers_by_str:
                    raise ValueError('Invalid value for ' + k + '.')
                to_pass[k] = _format_specifiers_by_str[to_pass[k]]
        # Check the types of all bool, int, and float arguments.
        for k in args_bool:
            if not isinstance(to_pass[k], (bool, numpy.bool_)):
                raise TypeError(k + ' is not a bool type.')
        for k in args_int:
            if not isinstance(to_pass[k], int_classes):
                raise TypeError(k + ' is not an integer type.')
        for k in args_float:
            if not isinstance(to_pass[k], real_classes):
                raise TypeError(k + ' must be a real number.')
        for k in args_with_dims:
            if scalar_array_like_dims(to_pass[k]) == 0:
                raise TypeError(k + ' must be an array-like.')
        # If dtype is 'auto', we must find a compatible dtype.
        if dtype == 'auto':
            # Convert array-like arguments to ndarray and make sure they
            # aren't scalars, check that scalar arguments are real
            # scalars, and then get the dtype.
            for k in args_that_need_dtype:
                if k in _array_dim_declarations:
                    to_pass[k] = numpy.ascontiguousarray(to_pass[k])
            dtype = numpy.result_type(
                'float32', *[to_pass[k] for k in args_that_need_dtype])
            if dtype not in _accepted_dtypes:
                raise TypeError('Arguments don''t map to a valid '
                                'dtype.')
            dtype = _dtype_lookup[dtype]
            if dtype not in funcs:
                raise KeyError(name + ' not available for ' + dtype)
        tp = numpy.dtype(dtype).type
        # Convert the arguments one by one if necessary.
        for k in args_that_need_dtype:
            v = to_pass[k]
            if k in _array_dim_declarations:
                dims = tuple(reversed(_array_dim_declarations[k]))
                # Convert to a fortran contiguous array while checking
                # that the number of dimensions is either right or one
                # less (but not scalars) and then increasing the number
                # of dimensions up to what is required.
                v_orig = numpy.ascontiguousarray(v, dtype)
                if v_orig.ndim == 0 or v_orig.ndim > len(dims) \
                        or v_orig.ndim < len(dims) - 1:
                    if len(dims) == 1:
                        raise ValueError(k + ' has the wrong number of '
                                         'dimensions. It should have '
                                         '1.')
                    else:
                        raise ValueError(k + ' has the wrong number of '
                                         'dimensions. It should have '
                                         '{0} or {1}.'.format(
                                             len(dims), len(dims) - 1))
                v2 = _dim_expanders[len(dims) - 1](v_orig)
                if v_orig.ndim == 1 and v2.ndim > 1:
                    v2.shape = tuple(reversed(v2.shape))
                elif v_orig.ndim == 2 and v2.ndim == 3:
                    v2.shape = (v_orig.shape[0], v_orig.shape[1], 1)
                # Go through each dimension and if it is already present
                # in to_pass, it must match, and if not it should be put
                # in.
                for i, k2 in enumerate(dims):
                    length = v2.shape[i]
                    if k2 in to_pass:
                        if to_pass[k2] != length:
                            raise ValueError(
                                'The length of ' + k + 'along axis '
                                '{0} doesn''t match the other '
                                'arguments, which should be {1:s} = '
                                '{2}.'.format(i, k2, to_pass[k2]))
                    else:
                        to_pass[k2] = length
                # Replace the entry in to_pass.
                to_pass[k] = v2
            elif not numpy.isscalar(v):
                raise TypeError(k + ' must be a scalar.')
            else:
                to_pass[k] = tp(v)
        # If k is in the arguments to pass, we need to make sure it is
        # between 1 and Mc (or Mc - 1 if it is a next procedure)
        # inclusive.
        if 'k' in to_pass:
            if not isinstance(to_pass['k'], int):
                raise TypeError('k must be an int.')
            if to_pass['k'] < 1:
                raise ValueError('k must be positive.')
            if '_next' in name:
                if to_pass['k'] >= to_pass['Mc']:
                    raise ValueError('k must be less than Mc.')
            else:
                if to_pass['k'] > to_pass['Mc']:
                    raise ValueError('k must be less than or equal to '
                                     'Mc.')
        # Allocate any outputs. Only arrays and integers are currently
        # supported.
        for k in outputs:
            if k in _array_dim_declarations:
                to_pass[k] = numpy.empty(
                    [to_pass[k2] for k2
                     in reversed(_array_dim_declarations[k])],
                    dtype=dtype, order='C')
            elif k != output_name:
                to_pass[k] = 0
        # Convert all non-numpy scalars in to_pass to ctypes
        # types. Float arguments require copying the raw bytes to
        # prevent loss by converting to float and back.
        for k in to_pass:
            if k in _int_args:
                to_pass[k] = ctypes.c_int(to_pass[k])
            elif k in _bool_args:
                to_pass[k] = ctypes.c_bool(to_pass[k])
            elif k in _float_args:
                v = _ctypes_lookup[dtype]()
                memoryview(v).cast('b')[:] = to_pass[k].data.cast('b')[:]
                to_pass[k] = v
        # Call the function and return the outputs, checking status for
        # errors if it was passed. ndarrays, numpy.number, arguments to
        # pass by value are passed as is; with the rest passed by
        # reference.
        args_to_pass = []
        for k in lib_argnames:
            v = to_pass[k]
            if isinstance(v, (numpy.ndarray, numpy.number)) \
                    or k in args_by_value:
                args_to_pass.append(v)
            else:
                args_to_pass.append(ctypes.byref(v))
        if output_name is None:
            funcs[dtype](*args_to_pass)
            if 'status' in to_pass:
                v = to_pass['status'].value
                if v == -1:
                    raise ValueError('Invalid argument.')
                elif v == -2:
                    raise MemoryError('Could not allocate memory.')
                elif v == -3:
                    raise OverflowError('Could not find a floating '
                                        'point type that seemed like '
                                        'it would not overflow.')
                elif v != 0:
                    raise RuntimeError(
                        'Unknown error status {0}.'.format(v))
            out = [to_pass[k] for k in outputs]
        else:
            out = [funcs[dtype](*args_to_pass)]
            out.extend([to_pass[k] for k in outputs[1:]])
        # Extract and convert the output, while checking for error
        # values in outputs that have them.
        for i, k in enumerate(outputs):
            if k in ('format_used', 'required_format'):
                if isinstance(out[i], int):
                    out[i] = _format_specifiers_by_number[out[i]]
                else:
                    out[i] = _format_specifiers_by_number[out[i].value]
                if k == 'required_format' and out[i] == -2:
                    raise ValueError('Invalid argument.')
            elif k == 'Mc_folded_required':
                if out[i] == -1:
                    out[i] = None
                elif out[i] == -2:
                    raise ValueError('Invalid argument.')
                elif out[i] == -3:
                    raise MemoryError('Could not allocate memory.')
                elif out[i] <= -4:
                    raise RuntimeError(
                        'Unknown error {0}.'.format(v))
                else:
                    if not isinstance(out[i], int):
                        out[i] = out[i].value
            elif k == 'emax' and numpy.isnan(out[i]):
                raise ValueError('Invalid argument.')
            elif k != 'status':
                if not isinstance(out[i], (numpy.ndarray,
                                           numpy.number,
                                           int, float)):
                    out[i] = out[i].value
        # Remove status.
        out = [v for i, v in enumerate(out) if outputs[i] != 'status']
        if len(out) == 0:
            return
        elif len(out) == 1:
            return out[0]
        else:
            return tuple(out)

    return wrapped


# Make the wrappers for all precision specific procedures.
for k, v in _procedures.items():
    locals()[k] = _make_wrapper(k, *v)
del k
del v


# Make other wrappers manually.


def libpmadra_supported_precisions():
    """ Get the floating point precisions suppored by libpmadra.

    Returns
    -------
    supported : tuple or None
        If the library is not available, it is ``None``. If the library
        is available, it is a 4-element ``tuple`` of ``bool`` indicating
        support for single, double, long double, and quad precision;
        which are ``C_FLOAT``, ``C_DOUBLE``, ``C_LONG_DOUBLE``, and
        ``REAL128`` in Fortran respectively.

    """
    if 'pmadra_supported_precisions' in _other_procedures:
        args = [ctypes.c_bool() for _ in range(4)]
        _other_procedures['pmadra_supported_precisions'](
            *[ctypes.byref(v) for v in args])
        return tuple([v.value for v in args])
    else:
        return None
