# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Poly-Multiplicity Airborne Disease Risk Assessment package.

Version 0.3

Many functions take a `dtype` argument to specify the dtype/precision
the computations should be done in. Some are potentially accelerated by
using libpmadra if it is present. ``'quad'`` can't be done in NumPy and
therefore completely relies on libpmadra for the functions that can use
it. They are

================  =================  =============  ================
dtype             libpmadra support  NumPy support  Meaning
================  =================  =============  ================
``'float32'``     yes                yes            single precision
``'float64'``     yes                yes            double precision
``'longdouble'``  yes                yes            C long double
``'quad'``        yes                no             quad precision
================  =================  =============  ================

"""

__version__ = '0.3'

import numpy

import pypmadra._libpmadra as _libpmadra


def libpmadra_info():
    """ Get information on libpmadra.

    Returns
    -------
    available : bool
        Whether the library is available.
    name : str or None
        The library name (``str``), or ``None`` if it isn't available.
    version : version or None
        The version if the library is available, or ``None`` if it
        isn't. The version is the type returned by the
        ``pkg_resourses.parse_version`` function, which will generally
        be a ``packaging.version.Version`` but has changed over time.
    supported_precisions : tuple or None
        If the library is not available, it is ``None``. If the library
        is available, it is a 4-element ``tuple`` of ``bool`` indicating
        support for single, double, long double, and quad precision;
        which are ``C_FLOAT``, ``C_DOUBLE``, ``C_LONG_DOUBLE``, and
        ``REAL128`` in Fortran respectively.

    See Also
    --------
    pkg_resources.parse_version

    """
    precisions = _libpmadra.libpmadra_supported_precisions()
    return _libpmadra.has_lib, _libpmadra.lib_name, \
        _libpmadra.libpmadra_version, precisions


@numpy.errstate(all='ignore')
def fold_Mc(y, Mc_folded, dtype='auto', try_libpmadra=True):
    """ Folds y-vectors down to lower Mc.

    Folds y-vectors to lower Mc (`Mc_folded`) by putting everything
    above the fold into the k right below the fold as if all the
    pathogens, production, etc. above the fold was actually in,
    producted at, etc. right below the fold. Additionally, the fraction
    that was folded is also returned. For example, folding

    ``y = [1, 1, 1, 1, 1, 1, 1]``

    from ``Mc = 7`` down to ``Mc = 5`` would produce

    ``y_folded = [1, 1, 1, 1, 3.6]``

    with a folded fraction of

    ``folded_fraction = 0.4643``

    Folding can be used to make a lower mc approximation of a problem,
    which can be useful to improve the speed of computation and prevent
    numerical overflow.

    Parameters
    ----------
    y : array-like
        1D or 2D array-like of non-negative numbers. If 2D, each column
        is a different y-vector. Its length along the leading axis is
        Mc.
    Mc_folded : int
        The value of Mc to fold `y` to. Must be non-negative.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The dtype to do the folding in. ``'auto'`` (the default) means
        to select it based on `y`.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    y_folded : numpy.ndarray
        The folded `y`. It is 1D or 2D array (same rank as `y`) with k
        as the leading dimension and if `y` is 2D, each column being the
        different folded y-vectors.
    folded_fraction : scalar or numpy.ndarray
        The fraction of pathogens, pathogen production, etc. that was
        folded. If `y` is 1D, it is a scalar. If `y` is 2D, it is a 1D
        array with one element for each y-vector.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Check Mc_folded.
    if not isinstance(Mc_folded, int):
        raise TypeError('Mc_folded must be int.')
    if Mc_folded < 0:
        raise ValueError('Mc_folded must be non-negative.')
    # Convert y to an array of the specified dtype.
    if dtype not in ('auto', 'float32', 'float64', 'longdouble'):
        raise ValueError('Invalid value of dtype.')
    # Make sure that y is 1D or 2D.
    ndim = _libpmadra.scalar_array_like_dims(y)
    if ndim == 0:
        raise TypeError('y must be an array-like.')
    if ndim > 2:
        raise ValueError('y must be 1D or 2D.')
    if dtype == 'auto':
        y = numpy.asarray(y, dtype=numpy.result_type('float32', y))
        if y.dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = y.dtype.name
    else:
        y = numpy.asarray(y, dtype=dtype)
    # Make sure the dtype is suitable.
    if dtype not in {'float32', 'float64', 'longdouble'}:
        raise TypeError('Inputs don''t map to a valid dtype.')
    # Try the libpmadra version if it is available and we are supposed
    # to try.
    if try_libpmadra:
        try:
            folded_fraction, y_folded = _libpmadra.fold_Mc(y, Mc_folded,
                                                           True,
                                                           dtype=dtype)
            if ndim == 1:
                return y_folded[:, 0], folded_fraction[0]
            else:
                return y_folded, folded_fraction
        except KeyError:
            pass
        except:
            raise
    # We have to do it in pure NumPy.
    #
    # Check y and then get Mc and make a vector of the k.
    if numpy.isnan(y).any() or numpy.any(y < 0):
        raise ValueError('The elements of y must be non-NaN and '
                         'non-negative.')
    Mc = y.shape[0]
    if y.ndim == 1:
        k = numpy.arange(1, Mc + 1, dtype='int64')
    else:
        k = numpy.arange(1, Mc + 1, dtype='int64').reshape((Mc, 1))
    # Allocate y_folded and folded_fraction.
    if y.ndim == 1:
        y_folded = numpy.zeros((Mc_folded, ), dtype=dtype)
        folded_fraction = y.dtype.type(0)
    else:
        y_folded = numpy.zeros((Mc_folded, y.shape[1]), dtype=dtype)
        folded_fraction = numpy.zeros((y.shape[1], ), dtype=dtype)
    # Hanlde Mc_folded == 0, Mc_folded >= Mc, and Mc_folded < Mc.
    if Mc_folded == 0:
        folded_fraction += 1
    elif Mc_folded >= Mc:
        y_folded[:Mc] = y
    else:
        y_folded[:] = y[:Mc_folded]
        folded_fraction += numpy.sum(k[Mc_folded:] * y[Mc_folded:],
                                     axis=0)
        y_folded[Mc_folded - 1] += folded_fraction / Mc_folded
        folded_fraction /= folded_fraction + numpy.sum(
            k[:Mc_folded] * y[:Mc_folded], axis=0)
    return y_folded, folded_fraction
