# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Exact rational aritmetic implementations of Vk and Uk.

Calculations are done with the assistence of ``gmpy2`` using the
``gmpy2.mpq`` and ``gmpy2.mpz`` types.

See Also
--------
gmpy2
gmpy2.mpz
gmpy2.mpz

"""

import collections.abc

# Try to import gmpy2, and set a flag whether successful or not.
try:
    import gmpy2
except:
    _has_gmpy2 = False
else:
    _has_gmpy2 = True


def Vks(y, x):
    """ Calculates the ``V_k(y, x)`` exactly for one y-vector and x.

    Calculates the following

    .. math::

       V_k\\left(\\vec{y},x\\right) = \\sum_{i=k}^{M_c}{
         \\binom{i}{k} y_i (1 - x)^{i - k}}

    where ``M_c`` is the length of `y`.

    Parameters
    ----------
    y : Sequence
        Sequence of finite non-negative numbers.
    x : scalar
        Scalar between 0 and 1 inclusive.

    Returns
    -------
    vks : list
        The Vk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpq``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpq

    """
    if not _has_gmpy2:
        raise ImportError('gmpy2 not available.')
    if not isinstance(y, collections.abc.Sequence):
        raise TypeError('y must be a Sequence.')
    # Convert and check arguments.
    try:
        y = [gmpy2.mpq(v) for v in y]
        x = gmpy2.mpq(x)
    except OverflowError:
        # There was an infinity, which must be converted to a value
        # error.
        raise ValueError('The arguments must be finite.')
    # Check the arguments' values.
    zero = gmpy2.mpq(0)
    if x < zero or x > 1:
        raise ValueError('x must be between 0 and 1 inclusive.')
    for v in y:
        if v < zero:
            raise ValueError('The values of y must be non-negative.')
    # Evaluate
    return _raw_Vks(y, x)


def Uks(y, x, alpha, gamma):
    """ Calculates the ``U_k(y, x)`` exactly for one y-vector and x.

    Calculates the following

    .. math::

       U_k\\left(\\vec{y},x\\right) =
         \\begin{cases}
           -\\frac{y_{M_c}}{\\gamma s} & \\text{if } k = M_c \\; , \\\\
           \\frac{(k + 1) x}{s} U_{k+1}\\left(\\vec{y},x\\right)
           - \\frac{1}{\\gamma s} V_k\\left(\\vec{y},x\\right)
           & \\text{otherwise} \\; .
         \\end{cases}

    where ``M_c`` is the length of `y` and

    .. math::

       s = \\frac{\\alpha}{\\gamma} + k

    Parameters
    ----------
    y : Sequence
        Sequence of finite non-negative numbers.
    x : scalar
        Scalar between 0 and 1 inclusive.
    alpha : scalar
        Finite positive scalar. This is the sink.
    gamma : scalar
        Finite positive scalar. This is the inactivation rate.

    Returns
    -------
    uks : list
        The Uk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpq``.
    vks : list
        The Vk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpq``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpq

    """
    if not _has_gmpy2:
        raise ImportError('gmpy2 not available.')
    if not isinstance(y, collections.abc.Sequence):
        raise TypeError('y must be a Sequence.')
    # Convert and check arguments.
    try:
        y = [gmpy2.mpq(v) for v in y]
        x = gmpy2.mpq(x)
        alpha = gmpy2.mpq(alpha)
        gamma = gmpy2.mpq(gamma)
    except OverflowError:
        # There was an infinity, which must be converted to a value
        # error.
        raise ValueError('The arguments must be finite.')
    # Check the arguments' values.
    zero = gmpy2.mpq(0)
    if x < zero or x > 1:
        raise ValueError('x must be between 0 and 1 inclusive.')
    if alpha < zero:
        raise ValueError('alpha must be non-negative.')
    if gamma < zero:
        raise ValueError('gamma must be non-negative.')
    if alpha == zero and gamma == zero:
        raise ValueError('alpha and gamma cannot be simultaneously '
                         'zero.')
    for v in y:
        if v < zero:
            raise ValueError('The values of y must be non-negative.')
    # Evaluate
    return _raw_Uks(y, x, alpha, gamma)


def _raw_Vks(y, x):
    # Arguments must already be converted.
    #
    # Handle x == 1 as a special case.
    if x == 1:
        return y
    Mc = len(y)
    one_minus_x = gmpy2.mpq(1) - x
    zero = gmpy2.mpq(0)
    out = []
    for k in range(1, Mc + 1):
        value = zero
        for i in range(k, Mc + 1):
            value += gmpy2.bincoef(i, k) * (y[i - 1]
                                            * one_minus_x**(i - k))
        out.append(value)
    return out


def _raw_Uks(y, x, alpha, gamma):
    # Arguments must already be converted.
    Mc = len(y)
    vks = _raw_Vks(y, x)
    # Allocate the Uks and calculate the value for Mc, then calculate
    # backwards in k. Handle the special case of gamma = 0 separately.
    uks = Mc * [None]
    if gamma == 0:
        uks[-1] = -y[-1] / alpha
        for k in range(1, Mc):
            uks[k - 1] = -vks[k - 1] / alpha
    else:
        sbase = alpha / gamma
        uks[-1] = -y[-1] / (alpha + Mc * gamma)
        for k in range(Mc - 1, 0, -1):
            uks[k - 1] = (((k + 1) * x) / (sbase + k)) * uks[k] \
                - vks[k - 1] / (alpha + k * gamma)
    return uks, vks


def n_inf(beta, alpha, gamma):
    """ Calculates ``n_inf`` exactly.

    Calculates

    .. math::

       n_{\\infty,k} = -U_k\\left(\\vec{\\beta},1\\right)

    Parameters
    ----------
    beta : Sequence
        Sequence of finite non-negative numbers, which is the source
        vector.
    alpha : scalar
        Finite positive scalar. This is the sink.
    gamma : scalar
        Finite positive scalar. This is the inactivation rate.

    Returns
    -------
    ninf : list
        The ``n_inf`` values from ``k = 1`` to ``k = M_c``. Each element
        will be a ``gmpy2.mpq``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpq

    """
    if not _has_gmpy2:
        raise ImportError('gmpy2 not available.')
    if not isinstance(beta, collections.abc.Sequence):
        raise TypeError('beta must be a Sequence.')
    # Convert and check arguments.
    try:
        beta = [gmpy2.mpq(v) for v in beta]
        alpha = gmpy2.mpq(alpha)
        gamma = gmpy2.mpq(gamma)
    except OverflowError:
        # There was an infinity, which must be converted to a value
        # error.
        raise ValueError('The arguments must be finite.')
    # Check the arguments' values.
    zero = gmpy2.mpq(0)
    if alpha < zero:
        raise ValueError('alpha must be non-negative.')
    if gamma < zero:
        raise ValueError('gamma must be non-negative.')
    for v in beta:
        if v < zero:
            raise ValueError('The values of beta must be non-negative.')
    # Evaluate, handle the zero case explicitly.
    if gamma == zero:
        if alpha == zero:
            raise ValueError('alpha and gamma can''t both be zero.')
        return [v / alpha for v in beta]
    else:
        uks, _ = _raw_Uks(beta, gmpy2.mpq(1), alpha, gamma)
        return [-v for v in uks]
