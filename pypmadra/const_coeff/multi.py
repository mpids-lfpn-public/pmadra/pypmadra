# Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Multi-precision floating point analytical solution, Vk, Uk, and Wk.

Calculations are done with the assistence of ``gmpy2`` using the
``gmpy2.mpfr`` types. All functions allow both the precision and the
maximum supported exponent (``emax``) to be specified.

See Also
--------
gmpy2
gmpy2.mpfr

"""

import collections.abc
import functools
import inspect

# Try to import gmpy2, and set a flag whether successful or not.
try:
    import gmpy2
except:
    _has_gmpy2 = False
else:
    _has_gmpy2 = True


########################################################################
#
# Wrapper that sets up a gmpy2 context if precision and emax don't match
# the current context's. They are also checked.
#
########################################################################

def _precision_context_setup(func):
    # Get the default precision and emax arguments.
    sig = inspect.signature(func)
    if 'precision' in sig.parameters:
        default_precision = sig.parameters['precision'].default
    else:
        default_precision = 64
    if 'emax' in sig.parameters:
        default_emax = sig.parameters['emax'].default
    else:
        default_emax = 512 * 1024

    @functools.wraps(func)
    def wrapper(*args, precision=default_precision, emax=default_emax,
                **keywords):
        if not _has_gmpy2:
            raise ImportError('gmpy2 not available.')
        if not isinstance(precision, int):
            raise TypeError('precision must be int.')
        if precision < 1:
            raise ValueError('precision must be positive.')
        if not isinstance(emax, int):
            raise TypeError('emax must be int.')
        if emax < 2:
            raise ValueError('emax must be at least 2.')
        ctx = gmpy2.get_context()
        if ctx.precision != precision or ctx.emax != emax \
                or ctx.emin != 1 - emax:
            with gmpy2.local_context(gmpy2.context(),
                                     precision=precision,
                                     emax=emax,
                                     emin=1 - emax) as ctx:
                return func(*args, precision=precision, emax=emax,
                            **keywords)
        return func(*args, precision=precision, emax=emax, **keywords)
    return wrapper


########################################################################
#
# Multi-precision Vk, Uk, and Wk with gmpy2
#
########################################################################


@_precision_context_setup
def Vks(y, x, binom_from_gamma=True,
        precision=64, emax=512 * 1024):
    """ Calculates the ``V_k(y, x)`` in the specified multiple precision.

    Calculates the following

    .. math::

       V_k\\left(\\vec{y},x\\right) = \\sum_{i=k}^{M_c}{
         \\binom{i}{k} y_i (1 - x)^{i - k}}

    where ``M_c`` is the length of `y`.

    Parameters
    ----------
    y : Sequence
        Sequence of non-negative numbers.
    x : scalar
        Scalar between 0 and 1 inclusive.
    binom_from_gamma : bool
        Whether binomial coefficients should be calculated with the help
        of the Gamma function or not. Default is ``True``, which means
        to calculate them exactly as integers before converting them to
        ``gmpy2.mpfr``.
    precision : int, optional
        The number of bits of precision to do the calculations in. Must
        be positive. Default is 64.
    emax : int
        The maximum exponent (minimum exponent will be ``1 - emax``) to
        use in the floating point format. Must be greater than one. The
        default is ``512 * 1024``.

    Returns
    -------
    vks : list
        The Vk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpfr

    """
    if not isinstance(y, collections.abc.Sequence):
        raise TypeError('y must be a Sequence.')
    if not isinstance(binom_from_gamma, bool):
        raise TypeError('binom_from_gamma must be bool.')
    # Convert and check the arguments.
    y = [gmpy2.mpfr(v) for v in y]
    x = gmpy2.mpfr(x)
    if not gmpy2.is_finite(x) or gmpy2.is_signed(x) \
            or x > gmpy2.mpfr(1):
        raise ValueError('x must be between 0 and 1 inclusive.')
    for v in y:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of y must be non-NaN and '
                             'non-negative.')
    # Evaluate.
    return _raw_Vks(y, x, binom_from_gamma)


@_precision_context_setup
def Uks(y, x, alpha, gamma, binom_from_gamma=True,
        precision=64, emax=512 * 1024):
    """ Calculates the ``U_k(y, x)`` in the specified multiple precision.

    Calculates the following

    .. math::

       U_k\\left(\\vec{y},x\\right) =
         \\begin{cases}
           -\\frac{y_{M_c}}{\\gamma s} & \\text{if } k = M_c \\; , \\\\
           \\frac{(k + 1) x}{s} U_{k+1}\\left(\\vec{y},x\\right)
           - \\frac{1}{\\gamma s} V_k\\left(\\vec{y},x\\right)
           & \\text{otherwise} \\; .
         \\end{cases}

    where ``M_c`` is the length of `y` and

    .. math::

       s = \\frac{\\alpha}{\\gamma} + k

    Parameters
    ----------
    y : Sequence
        Sequence of non-negative numbers.
    x : scalar
        Scalar between 0 and 1 inclusive.
    alpha : scalar
        The sink, which must be non-negative. Must not be simultaneously
        zero with `gamma`.
    gamma : scalar
        The inactivation rate, which must be non-negative. Must not be
        simultaneously zero with `alpha`.
    binom_from_gamma : bool
        Whether binomial coefficients should be calculated with the help
        of the Gamma function or not. Default is ``True``, which means
        to calculate them exactly as integers before converting them to
        ``gmpy2.mpfr``.
    precision : int, optional
        The number of bits of precision to do the calculations in. Must
        be positive. Default is 64.
    emax : int
        The maximum exponent (minimum exponent will be ``1 - emax``) to
        use in the floating point format. Must be greater than one. The
        default is ``512 * 1024``.

    Returns
    -------
    uks : list
        The Uk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.
    vks : list
        The Vk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpfr

    """
    if not isinstance(y, collections.abc.Sequence):
        raise TypeError('y must be a Sequence.')
    if not isinstance(binom_from_gamma, bool):
        raise TypeError('binom_from_gamma must be bool.')
    # Convert and check the arguments.
    y = [gmpy2.mpfr(v) for v in y]
    x = gmpy2.mpfr(x)
    alpha = gmpy2.mpfr(alpha)
    gamma = gmpy2.mpfr(gamma)
    if not gmpy2.is_finite(x) or gmpy2.is_signed(x) \
            or x > gmpy2.mpfr(1):
        raise ValueError('x must be between 0 and 1 inclusive.')
    if gmpy2.is_nan(alpha) or gmpy2.is_signed(alpha):
        raise ValueError('alpha must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_nan(gamma) or gmpy2.is_signed(gamma):
        raise ValueError('gamma must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_zero(alpha) and gmpy2.is_zero(gamma):
        raise ValueError('alpha and gamma must not be simultaneously '
                         'zero.')
    for v in y:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of y must be non-NaN and '
                             'non-negative.')
    # Evaluate.
    return _raw_Uks(y, x, alpha, gamma, binom_from_gamma)


@_precision_context_setup
def Wks(y, x, alpha, gamma, binom_from_gamma=True,
        precision=64, emax=512 * 1024):
    """ Calculates the ``W_k(y, x)`` in the specified multiple precision.

    Calculates the following

    .. math::

       W_k\\left(\\vec{y},x\\right) =
         \\begin{cases}
           \\frac{y_{M_c}}{\\gamma s^2} (1 - x^s)
           & \\text{if } k = M_c \\; , \\\\
           \\frac{1}{s}\\big[(k + 1) W_{k+1}\\left(\\vec{y},x\\right)
           & \\\\
           \\quad + x^s U_k\\left(\\vec{y},x\\right)
           - U_k\\left(\\vec{y},1\\right)\\big]
           & \\text{otherwise} \\; .
         \\end{cases}

    where ``M_c`` is the length of `y` and

    .. math::

       s = \\frac{\\alpha}{\\gamma} + k

    Parameters
    ----------
    y : Sequence
        Sequence of finite non-negative numbers.
    x : scalar
        Scalar between 0 and 1 inclusive.
    alpha : scalar
        The sink, which must be non-negative. Must not be simultaneously
        zero with `gamma`.
    gamma : scalar
        The inactivation rate, which must be non-negative. Must not be
        simultaneously zero with `alpha`.
    binom_from_gamma : bool
        Whether binomial coefficients should be calculated with the help
        of the Gamma function or not. Default is ``True``, which means
        to calculate them exactly as integers before converting them to
        ``gmpy2.mpfr``.
    precision : int, optional
        The number of bits of precision to do the calculations in. Must
        be positive. Default is 64.
    emax : int
        The maximum exponent (minimum exponent will be ``1 - emax``) to
        use in the floating point format. Must be greater than one. The
        default is ``512 * 1024``.

    Returns
    -------
    wks : list
        The Wk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.
    uks : list
        The Uk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.
    vks : list
        The Vk values from ``k = 1`` to ``k = M_c``. Each element will
        be a ``gmpy2.mpfr``.
    uks_one : list
        The ``Uk(y,1)`` values from ``k = 1`` to ``k = M_c``. Each
        element will be a ``gmpy2.mpfr``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpfr

    """
    if not isinstance(y, collections.abc.Sequence):
        raise TypeError('y must be a Sequence.')
    if not isinstance(binom_from_gamma, bool):
        raise TypeError('binom_from_gamma must be bool.')
    # Convert and check the arguments.
    y = [gmpy2.mpfr(v) for v in y]
    x = gmpy2.mpfr(x)
    alpha = gmpy2.mpfr(alpha)
    gamma = gmpy2.mpfr(gamma)
    if not gmpy2.is_finite(x) or gmpy2.is_signed(x) \
            or x > gmpy2.mpfr(1):
        raise ValueError('x must be between 0 and 1 inclusive.')
    if gmpy2.is_nan(alpha) or gmpy2.is_signed(alpha):
        raise ValueError('alpha must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_nan(gamma) or gmpy2.is_signed(gamma):
        raise ValueError('gamma must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_zero(alpha) and gmpy2.is_zero(gamma):
        raise ValueError('alpha and gamma must not be simultaneously '
                         'zero.')
    for v in y:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of y must be non-NaN and '
                             'non-negative.')
    # Evaluate.
    return _raw_Wks(y, x, alpha, gamma, binom_from_gamma)


def _raw_Vks(y, x, binom_from_gamma):
    # Arguments must already be converted.
    #
    # Handle x == 1 as a special case.
    if x == gmpy2.mpfr(1):
        return y
    one_minus_x = gmpy2.mpfr(1) - x
    Mc = len(y)
    zero = gmpy2.mpfr(0)
    out = []
    for k in range(1, Mc + 1):
        value = zero
        # Compute ln(k!) ahead of time if we are calculating binomial
        # coefficients from the Gamma function.
        if binom_from_gamma:
            ln_k_factorial = gmpy2.lgamma(k + 1)[0]
        for i in range(k, Mc + 1):
            if binom_from_gamma:
                bincoef = gmpy2.exp(gmpy2.lgamma(i + 1)[0]
                                    - ln_k_factorial
                                    - gmpy2.lgamma(i - k + 1)[0])
            else:
                bincoef = gmpy2.mpfr(gmpy2.bincoef(i, k))
            value += bincoef * y[i - 1] * one_minus_x**(i - k)
        out.append(value)
    return out


def _raw_Uks(y, x, alpha, gamma, binom_from_gamma):
    # Arguments must already be converted.
    Mc = len(y)
    vks = _raw_Vks(y, x, binom_from_gamma)
    sbase = alpha / gamma
    # Allocate the Uks and calculate the value for Mc, then calculate
    # backwards in k.
    uks = Mc * [None]
    uks[-1] = -y[-1] / (alpha + Mc * gamma)
    for k in range(Mc - 1, 0, -1):
        uks[k - 1] = (((k + 1) * x) / (sbase + k)) * uks[k] \
            - vks[k - 1] / (alpha + k * gamma)
    return uks, vks


def _raw_Wks(y, x, alpha, gamma, binom_from_gamma):
    # Arguments must already be converted.
    Mc = len(y)
    uks, vks = _raw_Uks(y, x, alpha, gamma, binom_from_gamma)
    # We need Uk(y, 1).
    one = gmpy2.mpfr(1)
    uks_one, _ = _raw_Uks(y, one, alpha, gamma,
                          binom_from_gamma)
    # Handle x == 1 differently than x != 1:
    zero = gmpy2.mpfr(0)
    if x == one:
        wks = Mc * [zero]
    else:
        # Preallocate the Wks and set the last element. We need to
        # handle gamma = 0 specially for W_{M_c} which should be zero
        # but is calculated to be NaN when calculated like the rest (0 *
        # inf ---> NaN).
        sbase = alpha / gamma
        wks = Mc * [None]
        if gamma == zero:
            wks[-1] = zero
        else:
            wks[-1] = y[-1] * (1 - x**(sbase + Mc)) / (
                gamma * (sbase + Mc)**2)
        # Backward in k.
        for k in range(Mc - 1, 0, -1):
            s = sbase + k
            wks[k - 1] = ((k + 1) * wks[k]
                          + x**s * uks[k - 1]
                          - uks_one[k - 1]) / s
    return wks, uks, vks, uks_one


########################################################################
#
# Multi-precision analytical model solution in gmpy2.
#
########################################################################


@_precision_context_setup
def n_inf(beta, alpha, gamma,
          precision=64, emax=512 * 1024):
    """ Calculates ``n_inf`` in the specified precision.

    Calculates

    .. math::

       n_{\\infty,k} = -U_k\\left(\\vec{\\beta},1\\right)

    Parameters
    ----------
    beta : Sequence
        Sequence of non-negative numbers (numbers are not checked),
        which is the source vector.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    precision : int, optional
        The number of bits of precision to do the calculations in. Must
        be positive. Default is 64.
    emax : int
        The maximum exponent (minimum exponent will be ``1 - emax``) to
        use in the floating point format. Must be greater than one. The
        default is ``512 * 1024``.

    Returns
    -------
    ninf : list
        The ``n_inf`` values from ``k = 1`` to ``k = M_c``. Each element
        will be a ``gmpy2.mpfr``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpfr

    """
    if not isinstance(beta, collections.abc.Sequence):
        raise TypeError('beta must be a Sequence.')
    # Convert and check the arguments.
    beta = [gmpy2.mpfr(v) for v in beta]
    alpha = gmpy2.mpfr(alpha)
    gamma = gmpy2.mpfr(gamma)
    if gmpy2.is_nan(alpha) or gmpy2.is_signed(alpha):
        raise ValueError('alpha must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_nan(gamma) or gmpy2.is_signed(gamma):
        raise ValueError('gamma must be non-NaN and '
                         'non-negative.')
    for v in beta:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of beta must be non-NaN and '
                             'non-negative.')
    # Explicitly handle the alpha = gamma = 0 case (all +inf for
    # positive beta and 0 for zero beta) and otherwise use the recursive
    # formula.
    if gmpy2.is_zero(alpha) and gmpy2.is_zero(gamma):
        sel = {True: gmpy2.mpfr(0),
               False: gmpy2.inf()}
        return [sel[gmpy2.is_zero(v)] for v in beta]
    else:
        uks, _ = _raw_Uks(beta, gmpy2.mpfr(1), alpha, gamma, True)
        return [-v for v in uks]


@_precision_context_setup
def solve_analytical(n0, beta, alpha, gamma, t, t0=0,
                     intn0=None,
                     binom_from_gamma=True,
                     precision=64, emax=512 * 1024):
    """ Analytical solution to n and its integral in multi-precision.

    Does the recursive analytical solution in multi-precision floating
    point with the specified `precision` and `emax`.

    Parameters
    ----------
    n0 : Sequence
        Sequence of non-negative numbers, which is the initial
        concentration densities.
    beta : Sequence
        Sequence of non-negative numbers, which is the source vector.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    t : scalar
        The time, which must be non-NaN and at least `t0`.
    t0 : scalar, optional
        The initial time, which must be non-NaN, finite, and no larger
        than `t`. The default is zero.
    intn0 : Sequence or None, optional
        Sequence of non-negative numbers, which is the initial time
        integral values of the concentration densities (this is
        particularly useful when breaking a situation into stages with
        different constant coefficients), or ``None`` (default) to mean
        all zero.
    binom_from_gamma : bool
        Whether binomial coefficients should be calculated with the help
        of the Gamma function or not. Default is ``True``, which means
        to calculate them exactly as integers before converting them to
        ``gmpy2.mpfr``.
    precision : int, optional
        The number of bits of precision to do the calculations in. Must
        be positive. Default is 64.
    emax : int
        The maximum exponent (minimum exponent will be ``1 - emax``) to
        use in the floating point format. Must be greater than one. The
        default is ``512 * 1024``.

    Returns
    -------
    ninf : list
        The ``n_inf`` values from ``k = 1`` to ``k = M_c``. Each element
        will be a ``gmpy2.mpfr``.
    n : list
        The n values from ``k = 1`` to ``k = M_c``. Each element will be
        a ``gmpy2.mpfr``.
    intn : list
        The time integral of n values from ``k = 1`` to ``k = M_c``.
        Each element will be a ``gmpy2.mpfr``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    ImportError
        If the gmpy2 package is unavailable.

    See Also
    --------
    gmpy2.mpfr

    """
    # Check n0, beta, and intn0 and convert them.
    if not isinstance(n0, collections.abc.Sequence):
        raise TypeError('n0 must be a Sequence.')
    if not isinstance(beta, collections.abc.Sequence):
        raise TypeError('beta must be a Sequence.')
    if not isinstance(binom_from_gamma, bool):
        raise TypeError('binom_from_gamma must be bool.')
    # Convert and check the arguments except for intn0.
    n0 = [gmpy2.mpfr(v) for v in n0]
    beta = [gmpy2.mpfr(v) for v in beta]
    alpha = gmpy2.mpfr(alpha)
    gamma = gmpy2.mpfr(gamma)
    t = gmpy2.mpfr(t)
    t0 = gmpy2.mpfr(t0)
    if gmpy2.is_nan(alpha) or gmpy2.is_signed(alpha):
        raise ValueError('alpha must be non-NaN and '
                         'non-negative.')
    if gmpy2.is_nan(gamma) or gmpy2.is_signed(gamma):
        raise ValueError('gamma must be non-NaN and '
                         'non-negative.')
    if not gmpy2.is_finite(t0):
        raise ValueError('t0 must be non-NaN and finite.')
    if gmpy2.is_nan(t) or t < t0:
        raise ValueError('t must be non-NaN and greater than or equal '
                         'to t0.')
    for v in n0:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of n0 must be non-NaN and '
                             'non-negative.')
    for v in beta:
        if gmpy2.is_nan(v) or gmpy2.is_signed(v):
            raise ValueError('The elements of beta must be non-NaN and '
                             'non-negative.')
    # Convert and check intn0, setting it to all zero if it wasn't
    # given.
    if intn0 is None:
        intn0 = len(n0) * [gmpy2.mpfr(0)]
    else:
        if not isinstance(intn0, collections.abc.Sequence):
            raise TypeError('intn0 must be a Sequence or be None.')
        intn0 = [gmpy2.mpfr(v) for v in intn0]
        for v in intn0:
            if gmpy2.is_nan(v) or gmpy2.is_signed(v):
                raise ValueError('The elements of intn0 must be '
                                 'non-NaN and non-negative.')
    # Check that n0, beta, and intn0 have the same length.
    if len(n0) != len(beta) or len(n0) != len(intn0):
        raise ValueError('n0, intn0, and beta must have the same '
                         'length.')
    # If gamma is zero, the equations decouple and the solution is
    # easy. Otherwise, we have to do the full thing.
    dt = t - t0
    if gmpy2.is_zero(gamma):
        # If alpha = 0, the equation is linear.
        #
        #            / 0       if beta_k == 0,
        #   ninf_k = |
        #            \ +inf    otherwise.
        #   n = n_0 + beta * dt
        #   intn = intn0 + n_0 * dt + (beta / 2) * dt**2
        #
        # Otherwise,
        #
        #   ninf = beta / alpha
        #   n = ninf + (n0 - ninf) * exp(-alpha * dt)
        #   intn = intn0 + ninf * dt
        #           + (1/alpha) * (n0 - ninf) * (1 - exp(-alpha * dt))
        if gmpy2.is_zero(alpha):
            sel = {True: gmpy2.mpfr(0),
                   False: gmpy2.inf()}
            ninf = [sel[gmpy2.is_zero(v)] for v in beta]
            temp = dt**2 / 2
            n = [v + beta[i] * dt for i, v in enumerate(n0)]
            intn = [v + n0[i] * dt + beta[i] * temp
                    for i, v in enumerate(intn0)]
        else:
            inv_alpha = alpha**-1
            ninf = [v / alpha for v in beta]
            exp_neg_alpha_dt = gmpy2.exp(-alpha * dt)
            temp = inv_alpha * (gmpy2.mpfr(1) - exp_neg_alpha_dt)
            one = gmpy2.mpfr(1)
            n = [n0[i] * exp_neg_alpha_dt + (one - exp_neg_alpha_dt) * v
                 for i, v in enumerate(ninf)]
            intn = [intn0[i] + v * dt + (n0[i] - v) * temp
                    for i, v in enumerate(ninf)]
    else:
        # Set z, get all the Wks(beta, z) stuff, Uks(n0, z) stuff, and
        # Uks(n0, 1).
        sbase = alpha / gamma
        z = gmpy2.exp(-gamma * dt)
        wks_beta, uks_beta, _, uks_beta_one = _raw_Wks(
            beta, z, alpha, gamma, binom_from_gamma)
        uks_n0, vks_n0 = _raw_Uks(n0, z, alpha, gamma,
                                  binom_from_gamma)
        uks_n0_one, _ = _raw_Uks(n0, gmpy2.mpfr(1), alpha, gamma,
                                 binom_from_gamma)
        # Set n_inf, n, and intn.
        ninf = [-v for v in uks_beta_one]
        n = []
        intn = []
        for k in range(1, len(n0) + 1):
            z_to_s = z**(sbase + k)
            n.append(z_to_s * vks_n0[k - 1]
                     + (ninf[k - 1] + z_to_s * uks_beta[k - 1]))
            intn.append(intn0[k - 1]
                        + dt * ninf[k - 1]
                        + (z_to_s * uks_n0[k - 1] - uks_n0_one[k - 1])
                        - wks_beta[k - 1] / gamma)
    return ninf, n, intn
