# Copyright 2020-2021 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Solution for coefficients constant in time.

The recursive analytical solution is used in all cases. The analytical
solution when :math:`\\gamma \\neq 0` is

.. math::

   n_k & = & n_{\\infty,k} + z^s \\left[
       U_k\\left(\\vec{\\beta}, z\\right)
       + V_k\\left(\\vec{n}_0, z\\right)\\right] \\quad , \\\\
   \\int_{t_0}^t{n_k(v) dv} & = & (t - t_0) n_{\\infty,k}
       - U_k\\left(\\vec{n}_0, 1\\right)
       + z^s U_k\\left(\\vec{n}_0, z\\right)
       - \\frac{1}{\\gamma} W_k\\left(\\vec{\\beta}, z\\right)
       \\quad , \\\\
   n_{\\infty,k} & = & -U_k\\left(\\vec{\\beta}, 1\\right) \\quad , \\\\
   V_k\\left(\\vec{y}, x\\right) & = & \\sum_{i=k}^{M_c}
       \\binom{i}{k} y_i (1 - x)^{i - k} \\quad , \\\\
   U_k\\left(\\vec{y}, x\\right) & = & -\\frac{1}{\\gamma}
       \\sum_{i=k}^{M_c}
       \\binom{i}{k} y_i \\sum_{p=0}^{i-k} \\binom{i - k}{p}
       \\frac{(-1)^p x^p}{s + p} \\quad , \\\\
   W_k\\left(\\vec{y}, x\\right) & = &
       \\int_1^x{v^{s - 1} U_k\\left(\\vec{y}, v\\right) dv} \\quad ,

where

.. math::

   z & = & e^{-(t - t_0) \\gamma} \\quad , \\\\
   s & = & \\frac{\\alpha}{\\gamma} + k \\quad .

Now, :math:`U_k\\left(\\vec{y}, x\\right)` and
:math:`W_k\\left(\\vec{y}, x\\right)` are calculated with the following
recursive formulas

.. math::

   U_k\\left(\\vec{y}, x\\right) & = &
       \\begin{cases}
           -\\frac{y_{M_c}}{\\gamma s} & \\text{if $k = M_c$}
               \\quad , \\\\
           \\frac{(k + 1) x}{s} U_{k+1}\\left(\\vec{y}, x\\right)
               - \\frac{1}{\\gamma s} V_k\\left(\\vec{y}, x\\right)
               & \\text{otherwise} \\quad ,
       \\end{cases} \\\\
   W_k\\left(\\vec{y}, x\\right) & = &
       \\begin{cases}
           \\frac{y_{M_c}}{\\gamma s^2} (1 - x^s)
               & \\text{if $k = M_c$} \\quad , \\\\
           \\frac{1}{s}\\left[(k + 1) W_{k+1}\\left(\\vec{y}, x\\right)
               + x^s U_k\\left(\\vec{y}, x\\right)
               - U_k\\left(\\vec{y}, 1\\right) \\right]
               & \\text{otherwise} \\quad ,
       \\end{cases}

When :math:`\\gamma = \\alpha = 0`, the solution is

.. math::

    n_{\\infty,k} & = &
        \\begin{cases}
            0 & \\text{if $\\beta_k = 0$} \\quad , \\\\
            +\\infty & \\text{otherwise} \\quad ,
        \\end{cases} \\\\
        \\vec{n} & = & \\vec{n}_0 + (t - t_0) \\vec{\\beta} \\\\
        \\int_{t_0}^t{\\vec{n}(v) dv} & = & (t - t_0) \\vec{n}_0
            + \\frac{1}{2} (t - t_0)^2 \\vec{\\beta}

When :math:`\\gamma = 0` and :math:`\\alpha \\neq 0`, the solution is

.. math::

   \\vec{n}_\\infty & = & \\frac{1}{\\alpha} \\vec{\\beta} \\\\
   \\vec{n} & = & \\vec{n}_\\infty
       + \\left(\\vec{n}_0 - \\vec{n}_\\infty\\right)
       e^{\\alpha (t - t_0)} \\\\
   \\int_{t_0}^t{\\vec{n}(v) dv} & = & (t - t_0) \\vec{n}_\\infty
       + \\frac{1}{\\alpha} \\left(\\vec{n}_0 - \\vec{n}_\\infty\\right)
       \\left(1 - e^{\\alpha (t - t_0)}\\right)

"""

import numpy
import scipy.sparse
import scipy.special

from pypmadra import fold_Mc
import pypmadra._libpmadra as _libpmadra


# Determine the exponent limits and precisions for float32, float64, and
# longdouble. Then determine if longdouble is the same as double.
_numpy_type_info = dict()
for _k in ('float32', 'float64', 'longdouble'):
    _v = numpy.finfo(_k)
    _numpy_type_info[_k] = (numpy.dtype(_k).type, _v.maxexp, _v.nmant)
_longdouble_is_double = (numpy.dtype('float64')
                         == numpy.dtype('longdouble'))
_numpy_types_supported = {numpy.dtype(_k) for _k in _numpy_type_info}
del _k
del _v


########################################################################
#
# Vk, Uk, and Wk in Fortan or numpy.
#
########################################################################


@numpy.errstate(all='ignore')
def Vks(y, x, dtype='auto', try_libpmadra=True):
    """ Calculates the ``V_k(y, x)`` in the specified numpy precision.

    Calculates the following

    .. math::

       V_k\\left(\\vec{y},x\\right) = \\sum_{i=k}^{M_c}{
         \\binom{i}{k} y_i (1 - x)^{i - k}}

    where ``M_c`` is the length of `y`.

    Parameters
    ----------
    y : array-like
        1D array-like (or 2D if libpmadra is used) of non-negative
        numbers. If 2D, each column is a different y-vector.
    x : array-like
        1D array-like of numbers between 0 and 1 inclusive.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The numpy dtype, which must be a floating point type. The
        default is ``'auto'``.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    vks : numpy.ndarray
        Vk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Use the libpmadra version if possible and we are supposed to try.
    if try_libpmadra:
        try:
            vks =  _libpmadra.vks(y, x, False, True,
                                  dtype=dtype)
            if _libpmadra.scalar_array_like_dims(y) == 1:
                return vks[:, 0, :]
            else:
                return vks
        except KeyError:
            pass
        except:
            raise
    # Check that x and y are 1D.
    ndim = _libpmadra.scalar_array_like_dims(y)
    if ndim == 0:
        raise TypeError('y must be an array-like.')
    if ndim > 1:
        raise ValueError('y must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(x)
    if ndim == 0:
        raise TypeError('x must be an array-like.')
    if ndim > 1:
        raise ValueError('x must be 1D.')
    # Check the dtype and convert ys and xs.
    if dtype == 'auto':
        y = numpy.asarray(y)
        x = numpy.asarray(x)
        dtype = numpy.result_type('float32', y, x)
        if dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = dtype.name
        if dtype not in _numpy_type_info:
            raise TypeError('Inputs don''t map to a valid dtype.')
    else:
        if dtype not in _numpy_type_info:
            raise ValueError('Invalid value of dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    ys = numpy.asarray(y, dtype=dtype)
    xs = numpy.asarray(x, dtype=dtype)
    if not numpy.isfinite(xs).all() or numpy.any(xs < 0) or numpy.any(
            xs > 1):
        raise ValueError('The elements of x must be between 0 and 1 '
                         'inclusive.')
    if numpy.isnan(ys).any() or numpy.any(ys < 0):
        raise ValueError('The elements of y must be non-NaN and '
                         'non-negative.')
    return _raw_Vks(ys, xs, tp)


@numpy.errstate(all='ignore')
def Uks(y, x, alpha, gamma, dtype='auto', try_libpmadra=True):
    """ Calculates the ``U_k(y, x)`` in the specified numpy precision.

    Calculates the following

    .. math::

       U_k\\left(\\vec{y},x\\right) =
         \\begin{cases}
           -\\frac{y_{M_c}}{\\gamma s} & \\text{if } k = M_c \\; , \\\\
           \\frac{(k + 1) x}{s} U_{k+1}\\left(\\vec{y},x\\right)
           - \\frac{1}{\\gamma s} V_k\\left(\\vec{y},x\\right)
           & \\text{otherwise} \\; .
         \\end{cases}

    where ``M_c`` is the length of `y` and

    .. math::

       s = \\frac{\\alpha}{\\gamma} + k

    Parameters
    ----------
    y : array-like
        1D array-like (or 2D if libpmadra is used) of non-negative
        numbers. If 2D, each column is a different y-vector.
    x : array-like
        1D array-like of numbers between 0 and 1 inclusive.
    alpha : scalar
        The sink, which must be non-negative. Cannot be simultaneously
        zero with `gamma`.
    gamma : scalar
        The inactivation rate, which must be non-negative. Cannot be
        simultaneously zero with `alpha`.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The numpy dtype, which must be a floating point type. The
        default is ``'auto'``.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    uks : numpy.ndarray
        Uk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.
    vks : numpy.ndarray
        Vk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Use the libpmadra version if possible and we are supposed to try.
    if try_libpmadra:
        try:
            vks = _libpmadra.vks(y, x, False, True, dtype=dtype)
            uks = _libpmadra.uks(vks, y, x, alpha, gamma, False,
                                 dtype=dtype)
            if _libpmadra.scalar_array_like_dims(y) == 1:
                return uks[:, 0, :], vks[:, 0, :]
            else:
                return uks, vks
        except KeyError:
            pass
        except:
            raise
    # Check that alpha and gamma are real numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    # Check that x and y are 1D.
    ndim = _libpmadra.scalar_array_like_dims(y)
    if ndim == 0:
        raise TypeError('y must be an array-like.')
    if ndim > 1:
        raise ValueError('y must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(x)
    if ndim == 0:
        raise TypeError('x must be an array-like.')
    if ndim > 1:
        raise ValueError('x must be 1D.')
    # Check the dtype and convert ys and xs.
    if dtype == 'auto':
        y = numpy.asarray(y)
        x = numpy.asarray(x)
        dtype = numpy.result_type('float32', y, x, alpha, gamma)
        if dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = dtype.name
        if dtype not in _numpy_type_info:
            raise TypeError('Inputs don''t map to a valid dtype.')
    else:
        if dtype not in _numpy_type_info:
            raise ValueError('Invalid value of dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    ys = numpy.asarray(y, dtype=dtype)
    xs = numpy.asarray(x, dtype=dtype)
    gamma = tp(gamma)
    alpha = tp(alpha)
    if numpy.isnan(alpha) or alpha < 0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if numpy.isnan(gamma) or gamma < 0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if gamma == 0 and alpha == 0:
        raise ValueError('alpha and gamma must not be simultaneously '
                         'zero.')
    if not numpy.isfinite(xs).all() or numpy.any(xs < 0) or numpy.any(
            xs > 1):
        raise ValueError('The elements of x must be between 0 and 1 '
                         'inclusive.')
    if numpy.isnan(ys).any() or numpy.any(ys < 0):
        raise ValueError('The elements of y must be non-NaN and '
                         'non-negative.')
    return _raw_Uks(ys, xs, alpha, gamma, tp)


@numpy.errstate(all='ignore')
def Wks(y, x, alpha, gamma, dtype='auto', try_libpmadra=True):
    """ Calculates the ``W_k(y, x)`` in the specified numpy precision.

    Calculates the following

    .. math::

       W_k\\left(\\vec{y},x\\right) =
         \\begin{cases}
           \\frac{y_{M_c}}{\\gamma s^2} (1 - x^s)
           & \\text{if } k = M_c \\; , \\\\
           \\frac{1}{s}\\big[(k + 1) W_{k+1}\\left(\\vec{y},x\\right)
           & \\\\
           \\quad + x^s U_k\\left(\\vec{y},x\\right)
           - U_k\\left(\\vec{y},1\\right)\\big]
           & \\text{otherwise} \\; .
         \\end{cases}

    where ``M_c`` is the length of `y` and

    .. math::

       s = \\frac{\\alpha}{\\gamma} + k

    Parameters
    ----------
    y : array-like
        1D array-like (or 2D if libpmadra is used) of non-negative
        numbers. If 2D, each column is a different y-vector.
    x : array-like
        1D array-like of numbers between 0 and 1 inclusive.
    alpha : scalar
        The sink, which must be non-negative. Cannot be simultaneously
        zero with `gamma`.
    gamma : scalar
        The inactivation rate, which must be non-negative. Cannot be
        simultaneously zero with `alpha`.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The numpy dtype, which must be a floating point type. The
        default is ``'auto'``.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    wks : numpy.ndarray
        Wk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.
    uks : numpy.ndarray
        Uk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.
    vks : numpy.ndarray
        Vk values from ``k = 1`` to ``k = M_c``. If `y` is 1D, it is one
        row for each k and one column for each element of `x`. If `y` is
        2D, it is one page for each k, one row for each y-vector, and
        one column for each element of `x`.
    uks_one : numpy.ndarray
        The values of ``Uk(y,1)`` from ``k = 1`` to ``k = M_c``. If `y`
        is 1D, it is 1D with one element for each k. If `y` is 2D, it is
        one row for each k and one column for each y-vector.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Use the libpmadra version if possible and we are supposed to try.
    if try_libpmadra:
        try:
            vks = _libpmadra.vks(y, x, False, True, dtype=dtype)
            uks = _libpmadra.uks(vks, y, x, alpha, gamma, False,
                                 dtype=dtype)
            uks_one = _libpmadra.uks_one(y, alpha, gamma, False,
                                         dtype=dtype)
            wks = _libpmadra.wks(uks, uks_one, y, x, alpha, gamma,
                                 False, dtype=dtype)
            if _libpmadra.scalar_array_like_dims(y) == 1:
                return wks[:, 0, :], uks[:, 0, :], vks[:, 0, :], \
                    uks_one[:, 0]
            else:
                return wks, uks, vks, uks_one
        except KeyError:
            pass
        except:
            raise
    # Check that alpha and gamma are real numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    # Check that x and y are 1D.
    ndim = _libpmadra.scalar_array_like_dims(y)
    if ndim == 0:
        raise TypeError('y must be an array-like.')
    if ndim > 1:
        raise ValueError('y must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(x)
    if ndim == 0:
        raise TypeError('x must be an array-like.')
    if ndim > 1:
        raise ValueError('x must be 1D.')
    # Check the dtype and convert ys and xs.
    if dtype == 'auto':
        y = numpy.asarray(y)
        x = numpy.asarray(x)
        dtype = numpy.result_type('float32', y, x, alpha, gamma)
        if dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = dtype.name
        if dtype not in _numpy_type_info:
            raise TypeError('Inputs don''t map to a valid dtype.')
    else:
        if dtype not in _numpy_type_info:
            raise ValueError('Invalid value of dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    ys = numpy.asarray(y, dtype=dtype)
    xs = numpy.asarray(x, dtype=dtype)
    gamma = tp(gamma)
    alpha = tp(alpha)
    if numpy.isnan(alpha) or alpha < 0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if numpy.isnan(gamma) or gamma < 0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if gamma == 0 and alpha == 0:
        raise ValueError('alpha and gamma must not be simultaneously '
                         'zero.')
    if not numpy.isfinite(xs).all() or numpy.any(xs < 0) or numpy.any(
            xs > 1):
        raise ValueError('The elements of x must be between 0 and 1 '
                         'inclusive.')
    if numpy.isnan(ys).any() or numpy.any(ys < 0):
        raise ValueError('The elements of y must be non-NaN and '
                         'non-negative.')
    return _raw_Wks(ys, xs, alpha, gamma, tp)


def _raw_Vks(y, x, tp):
    # Arguments must already be converted.
    one_minus_x = tp(1) - x
    Mc = y.shape[0]
    # Rather than iterating over k and then i, we will iterate over i -
    # k and then k because it is much faster for many elements of x.
    #
    # First, just assign the i - k = 0 elements, which is tiling by y.
    vks = numpy.tile(y.reshape((Mc, 1)), (1, one_minus_x.shape[0]))
    # Preallocate a variable to hold (1 - x)**(i - k)
    one_minus_x_raised = numpy.empty((one_minus_x.shape[0], ))
    # Now, for i - k > 0.
    for i_minus_k in range(1, Mc):
        # Calculate ln((i - k!) and (1 - x)**(i - k) since they will be
        # the same for the whole iteration.
        ln_i_minus_k_factorial = tp(scipy.special.gammaln(
            i_minus_k + 1))
        numpy.power(one_minus_x, i_minus_k, out=one_minus_x_raised)
        # Iterate over k until i reaches Mc.
        for k in range(1, Mc - i_minus_k + 1):
            i = k + i_minus_k
            bincoef = numpy.exp(
                tp(scipy.special.gammaln(i + 1))
                - tp(scipy.special.gammaln(k + 1))
                - ln_i_minus_k_factorial)
            vks[k - 1] += (bincoef * y[i - 1]) * one_minus_x_raised
    return vks


def _raw_Uks(y, x, alpha, gamma, tp):
    # Arguments must already be converted.
    Mc = y.shape[0]
    vks = _raw_Vks(y, x, tp)
    if gamma == 0:
        sbase = tp(numpy.inf)
    else:
        sbase = alpha / gamma
    # Allocate the Uks and calculate the value for Mc, then calculate
    # backwards in k.
    uks = numpy.zeros_like(vks)
    uks[-1] = -y[-1] / (alpha + tp(Mc) * gamma)
    for k in range(Mc - 1, 0, -1):
        uks[k - 1] = (tp(k + 1) / (sbase + tp(k))) * x * uks[k] \
            - vks[k - 1] / (alpha + tp(k) * gamma)
    return uks, vks


def _raw_Uks_one(y, alpha, gamma, tp):
    # For the case that x = 1.
    #
    # Arguments must already be converted.
    Mc = y.shape[0]
    if gamma == 0:
        sbase = tp(numpy.inf)
    else:
        sbase = alpha / gamma
    # Allocate the Uks and calculate the value for Mc, then calculate
    # backwards in k.
    uks = numpy.zeros_like(y)
    uks[-1] = -y[-1] / (alpha + tp(Mc) * gamma)
    for k in range(Mc - 1, 0, -1):
        uks[k - 1] = (tp(k + 1) / (sbase + tp(k))) * uks[k] \
            - y[k - 1] / (alpha + tp(k) * gamma)
    return uks, y


def _raw_Wks(y, x, alpha, gamma, tp):
    # Arguments must already be converted.
    Mc = y.shape[0]
    uks, vks = _raw_Uks(y, x, alpha, gamma, tp)
    # We need Uk(y, 1).
    uks_one, _ = _raw_Uks_one(y, alpha, gamma, tp)
    # Preallocate wks, set its last element, and then go backwards in k.
    wks = numpy.zeros_like(uks)
    if gamma == 0:
        sbase = tp(numpy.inf)
        wks[-1] = 0
    else:
        sbase = alpha / gamma
        wks[-1] = y[-1] * (tp(1) - x**(sbase + tp(Mc))) / (
            gamma * (sbase + tp(Mc))**2)
    # Backward in k.
    for k in range(Mc - 1, 0, -1):
        s = sbase + tp(k)
        wks[k - 1] = (tp(k + 1) * wks[k]
                      + (x**s * uks[k - 1]
                         - uks_one[k - 1])) / s
    return wks, uks, vks, uks_one


########################################################################
#
# n_inf in Fortran or numpy.
#
########################################################################


@numpy.errstate(all='ignore')
def n_inf(beta, alpha, gamma, dtype='auto', try_libpmadra=True):
    """ Calculates the ``n_inf`` in the specified numpy precision.

    Calculates

    .. math::

       n_{\\infty,k} = -U_k\\left(\\vec{\\beta},1\\right)

    Parameters
    ----------
    beta : array-like
        The source vector, which must be a non-negative 1D array-like.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The numpy dtype, which must be a floating point type. The
        default is ``'auto'``.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    ninf : numpy.ndarray
        1D array of ``n_inf`` values from ``k = 1`` to ``k = M_c``.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Use the libpmadra version if possible and we are supposed to try.
    if try_libpmadra:
        try:
            return _libpmadra.ninf(beta, alpha, gamma, True, dtype=dtype)
        except KeyError:
            pass
        except:
            raise
    # Check that alpha and gamma are real numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    # Check that beta is 1D.
    ndim = _libpmadra.scalar_array_like_dims(beta)
    if ndim == 0:
        raise TypeError('beta must be an array-like.')
    if ndim > 1:
        raise ValueError('beta must be 1D.')
    # Check the dtype and convert the parameters.
    if dtype == 'auto':
        beta = numpy.asarray(beta)
        dtype = numpy.result_type('float32', beta, alpha, gamma)
        if dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = dtype.name
        if dtype not in _numpy_type_info:
            raise TypeError('Inputs don''t map to a valid dtype.')
    else:
        if dtype not in _numpy_type_info:
            raise ValueError('Invalid value of dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    beta = numpy.asarray(beta, dtype=dtype)
    gamma = tp(gamma)
    alpha = tp(alpha)
    if numpy.isnan(alpha) or alpha < 0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if numpy.isnan(gamma) or gamma < 0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if numpy.isnan(beta).any() or numpy.any(beta < 0):
        raise ValueError('The elements of beta must be non-NaN and '
                         'non-negative.')
    # Explicitly handle the alpha = gamma = 0 case (0 if beta_k is 0 and
    # +inf if beta_k > 0).
    if alpha == 0 and gamma == 0:
        ninf = numpy.zeros_like(beta)
        ninf[beta > 0] = numpy.inf
        return ninf
    else:
        uks, _ = _raw_Uks_one(beta, alpha, gamma, tp)
        return -uks


########################################################################
#
# Determining the required emax and the smallest precision that supports
# it, and the largest folding Mc that must be used to make sure that the
# required format is a particular format or smaller.
#
########################################################################


@numpy.errstate(all='ignore')
def required_floating_point_format(n0, beta, alpha, gamma,
                                   emax_safety_factor=10.0,
                                   try_libpmadra=True):
    """ The format required to not overflow the analytical solution.

    Determines the approximate required floating point format emax
    `require_emax` without the safety factor (`emax_safety_factor`) to
    not get numerical overflow when calculating the recursive analytical
    solution, and the resulting smallest available floating point format
    that meets this requirement with the safety factor.

    Warning
    -------
    The safety factor `emax_safety_factor` is critical because the
    required emax is gotten from considering the required emax to make
    sure :math:`\\frac{1}{\\gamma} V_k\\left(\\vec{y},z\\right)` does
    not overflow, but does not track the required exponent through the
    whole recursive formulas for the solution. A sufficient safety
    factor helps prevent overflow when close to the limits of a
    particular floating point type.

    Parameters
    ----------
    n0 : array-like
        1D array-like of non-negative numbers, which is the initial
        concentration densities.
    beta : array-like
        The source vector, which must be a non-negative 1D array-like.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    emax_safety_factor : scalar, optional
        The safety factor to add to the approximate required emax. Must
        be non-negative. The default is 10, which corresponds to a
        multiplicative safety factor of 1024.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    required_emax : float
        The approximate required emax to not overflow.
    fmt : str or None
        The floating point format with the smallest emax that is at
        least ``required_emax + emax_safety_factor``, or ``None`` if
        none meet this requirement. The formats are ``'float32'`` for
        single precision, ``'float64'`` for double precision,
        ``'longdouble'`` for long double, and ``'quad'`` for quad
        precision.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    See Also
    --------
    solve_analytical

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Check that alpha, gamma, and emax_safety_factor are real numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    if not isinstance(emax_safety_factor, _libpmadra.real_classes):
        raise TypeError('emax_safety_factor must be a real number.')
    # Check that n0 and beta are 1D.
    ndim = _libpmadra.scalar_array_like_dims(n0)
    if ndim == 0:
        raise TypeError('n0 must be an array-like.')
    if ndim > 1:
        raise ValueError('n0 must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(beta)
    if ndim == 0:
        raise TypeError('beta must be an array-like.')
    if ndim > 1:
        raise ValueError('beta must be 1D.')
    # Convert the arguments.
    dtype = numpy.result_type('float32', n0, beta, alpha, gamma)
    if dtype == numpy.dtype('longdouble'):
        dtype = 'longdouble'
    else:
        dtype = dtype.name
    if dtype not in _numpy_type_info:
        raise TypeError('Inputs don''t map to a valid dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    n0 = numpy.asarray(n0, dtype=dtype)
    beta = numpy.asarray(beta, dtype=dtype)
    alpha = tp(alpha)
    gamma = tp(gamma)
    emax_safety_factor = tp(emax_safety_factor)
    # Check the arguments.
    if len(n0) != len(beta):
        raise ValueError('n0 and beta must have the same length.')
    if numpy.isnan(gamma) or gamma < 0.0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if numpy.isnan(alpha) or alpha < 0.0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if not numpy.isfinite(emax_safety_factor) \
            or emax_safety_factor < 0.0:
        raise ValueError('emax_safety_factor must be non-NaN, finite, '
                         'and non-negative.')
    if numpy.isnan(n0).any() or numpy.any(n0 < 0):
        raise ValueError('The elements of n0 must be non-NaN and '
                         'non-negative.')
    if numpy.isnan(beta).any() or numpy.any(beta < 0):
        raise ValueError('The elements of beta must be non-NaN and '
                         'non-negative.')
    # Get the required emax. Try to use libpmadra's version if we are
    # suppose do try. If it isn't available, we need to calculate it in
    # python.
    try:
        if not try_libpmadra:
            raise KeyError('not supposed to try libpmadra.')
        required_emax = float(_libpmadra.required_emax_solution(
            n0, beta, alpha, gamma, False, dtype=dtype))
    except KeyError:
        # Determine the required emax.
        Mc = len(n0)
        if Mc == 0:
            required_emax = 0.0
        elif gamma == 0.0:
            emax_n0 = numpy.log2(n0.max())
            emax_beta = numpy.log2(beta.max()) - numpy.log2(alpha)
            required_emax = 1.0 + float(max(emax_n0, emax_beta))
        else:
            max_element = max(1.0, n0.max(), beta.max())
            if Mc == 1:
                emax_vks = numpy.log(max_element) / numpy.log(2)
            else:
                half_Mc_floor = numpy.floor(Mc / 2.0)
                half_Mc_ceil = numpy.ceil(Mc / 2.0)
                emax_vks = (numpy.log(max_element)
                            + (Mc + 1.5) * numpy.log(Mc)
                            - (half_Mc_floor + 0.5)
                            * numpy.log(half_Mc_floor)
                            - (half_Mc_ceil + 0.5)
                            * numpy.log(half_Mc_ceil)
                            + (12.0 * Mc)**-1
                            - (12.0 * half_Mc_floor + 1.0)**-1
                            - (12.0 * half_Mc_ceil + 1.0)**-1
                            - 0.5 * numpy.log(2 * numpy.pi)
                            ) / numpy.log(2)
            required_emax = float(
                1.0 + emax_vks + numpy.log2(max(1.0, gamma**-1)))
    except:
        raise
    # Get the appropriate floating point format. Try to use libpmadra's
    # version if we are supposed to try. If it isn't available, we need
    # to calculate it in python.
    try:
        if not try_libpmadra:
            raise KeyError('not supposed to try libpmadra.')
        fmt = _libpmadra.required_format_solution(n0, beta, alpha,
                                                  gamma,
                                                  emax_safety_factor,
                                                  False, dtype=dtype)
    except KeyError:
        # Add the safety factor.
        needed_emax = numpy.ceil(required_emax +
                                 emax_safety_factor)
        fmt = None
        for k in ('float32', 'float64', 'longdouble'):
            if _numpy_type_info[k][1] >= needed_emax:
                fmt = k
                break
    except:
        raise
    # Done.
    return required_emax, fmt


@numpy.errstate(all='ignore')
def required_Mc_folded(desired_dtype, n0, beta, alpha, gamma,
                       emax_safety_factor=10.0,
                       try_libpmadra=True):
    """ Find the required folding Mc to get the desired largest format.

    Finds the maximum folding Mc value that can be used to make sure
    that the required floating point format is no larger than that
    specified in `desired_dtype`.

    Warning
    -------
    The safety factor `emax_safety_factor` is critical because the
    required emax is gotten from considering the required emax to make
    sure :math:`\\frac{1}{\\gamma} V_k\\left(\\vec{y},z\\right)` does
    not overflow, but does not track the required exponent through the
    whole recursive formulas for the solution. A sufficient safety
    factor helps prevent overflow when close to the limits of a
    particular floating point type.

    Parameters
    ----------
    desired_dtype : {'float32', 'float64', 'longdouble', 'quad'}
        The maximum (inclusive) floating point format to find the
        folding Mc for.
    n0 : array-like
        1D array-like of non-negative numbers, which is the initial
        concentration densities.
    beta : array-like
        The source vector, which must be a non-negative 1D array-like.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    emax_safety_factor : scalar, optional
        The safety factor to add to the approximate required emax. Must
        be non-negative. The default is 10, which corresponds to a
        multiplicative safety factor of 1024.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    Mc_folded : int or None
        The maximum Mc that `n0` and `beta` must be folded to in order
        to get the required floating point to be no larger than
        `desired_dtype`, or ``None`` if even a value of one would not
        work.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    See Also
    --------
    required_floating_point_format
    pypmadra.fold_Mc

    """
    # Check desired_dtype.
    if desired_dtype not in ('float32', 'float64', 'longdouble',
                             'quad'):
        raise ValueError('desired_dtype does not have a valid value.')
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Try the libpmadra version if it is available and we are supposed
    # to try.
    if try_libpmadra:
        try:
            return _libpmadra.required_Mc_folded(
                n0, beta, alpha, gamma, emax_safety_factor,
                desired_dtype, True, dtype='auto')
        except KeyError:
            pass
        except:
            raise
    # Check that alpha, gamma, and emax_safety_factor are real numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    if not isinstance(emax_safety_factor, _libpmadra.real_classes):
        raise TypeError('emax_safety_factor must be a real number.')
    # Check that n0 and beta are 1D.
    ndim = _libpmadra.scalar_array_like_dims(n0)
    if ndim == 0:
        raise TypeError('n0 must be an array-like.')
    if ndim > 1:
        raise ValueError('n0 must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(beta)
    if ndim == 0:
        raise TypeError('beta must be an array-like.')
    if ndim > 1:
        raise ValueError('beta must be 1D.')
    # Convert the arguments.
    dtype = numpy.result_type('float32', n0, beta, alpha, gamma)
    if dtype == numpy.dtype('longdouble'):
        dtype = 'longdouble'
    else:
        dtype = dtype.name
    if dtype not in _numpy_type_info:
        raise TypeError('Inputs don''t map to a valid dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    n0 = numpy.asarray(n0, dtype=dtype)
    beta = numpy.asarray(beta, dtype=dtype)
    alpha = tp(alpha)
    gamma = tp(gamma)
    emax_safety_factor = tp(emax_safety_factor)
    # Check the arguments.
    if len(n0) != len(beta):
        raise ValueError('n0 and beta must have the same length.')
    if numpy.isnan(gamma) or gamma < 0.0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if numpy.isnan(alpha) or alpha < 0.0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if not numpy.isfinite(emax_safety_factor) \
            or emax_safety_factor < 0.0:
        raise ValueError('emax_safety_factor must be non-NaN, finite, '
                         'and non-negative.')
    if numpy.isnan(n0).any() or numpy.any(n0 < 0):
        raise ValueError('The elements of n0 must be non-NaN and '
                         'non-negative.')
    if numpy.isnan(beta).any() or numpy.any(beta < 0):
        raise ValueError('The elements of beta must be non-NaN and '
                         'non-negative.')
    # We need a lookup to turn floating point formats into their number
    # order. Then, we need to get the numerical value for the desired
    # format.
    fmt_lookup = {v: i for i, v in enumerate(('float32', 'float64',
                                              'longdouble', 'quad',
                                              None))}
    fmt_desired = fmt_lookup[desired_dtype]
    # Get Mc and the required format without any folding. This will be
    # the upper bound of the binary search, assuming we need to do a search.
    Mc = len(n0)
    if Mc == 0:
        return 0
    _, fmt_name_upper = required_floating_point_format(
        n0, beta, alpha, gamma, emax_safety_factor=emax_safety_factor,
        try_libpmadra=try_libpmadra)
    fmt_upper = fmt_lookup[fmt_name_upper]
    if fmt_upper <= fmt_desired:
        return Mc
    # If Mc is 1, then folding can't be done.
    if Mc == 1:
        return None
    # Folding is required. Now we need to do test the lower end with a
    # Mc_folded of 1. If this is larger than fmt, then folding won't
    # make the format small enough.
    n0_folded, _ = fold_Mc(n0, 1, dtype=dtype,
                           try_libpmadra=try_libpmadra)
    beta_folded, _ = fold_Mc(beta, 1, dtype=dtype,
                             try_libpmadra=try_libpmadra)
    _, fmt_name_lower = required_floating_point_format(
        n0_folded, beta_folded, alpha, gamma,
        emax_safety_factor=emax_safety_factor,
        try_libpmadra=try_libpmadra)
    fmt_lower = fmt_lookup[fmt_name_lower]
    if fmt_lower > fmt_desired:
        return None
    # A binary search is required. We go till Mc_upper = Mc_lower + 1
    # while maintaining fmt_lower <= fmt_desired and
    # fmt_upper > fmt_desired; after which Mc_lower is the maximum
    # folding Mc that meets the requirements.
    Mc_upper = Mc
    Mc_lower = 1
    while Mc_upper > Mc_lower + 1:
        Mc_test = (Mc_upper + Mc_lower) // 2
        n0_folded, _ = fold_Mc(n0, Mc_test, dtype=dtype,
                               try_libpmadra=try_libpmadra)
        beta_folded, _ = fold_Mc(beta, Mc_test, dtype=dtype,
                                 try_libpmadra=try_libpmadra)
        _, fmt_name_test = required_floating_point_format(
            n0_folded, beta_folded, alpha, gamma,
            emax_safety_factor=emax_safety_factor,
            try_libpmadra=try_libpmadra)
        fmt_test = fmt_lookup[fmt_name_test]
        if fmt_test <= fmt_desired:
            Mc_lower = Mc_test
            fmt_lower = fmt_test
        else:
            Mc_upper = Mc_test
            fmt_upper = fmt_test
    return Mc_lower


########################################################################
#
# Full analytical solution to n with Fortran or numpy.
#
########################################################################


@numpy.errstate(all='ignore')
def solve_analytical(n0, beta, alpha, gamma, t, t0=0, intn0=None,
                     emax_safety_factor=10.0, dtype='auto',
                     try_libpmadra=True):
    """ Analytical solution to n and its integral in numpy.

    Does the recursive analytical solution in the specified numpy
    floating point `dtype`, which is hopefully hardware accelerated.

    Parameters
    ----------
    n0 : array-like
        1D array-like of non-negative numbers, which is the initial
        concentration densities.
    beta : array-like
        The source vector, which must be a non-negative 1D array-like.
    alpha : scalar
        The sink, which must be non-negative.
    gamma : scalar
        The inactivation rate, which must be non-negative.
    t : array-like
        1D array-like of the time, which must be non-NaN and at least
        `t0`.
    t0 : scalar, optional
        The initial time, which must be non-NaN, finite, and no larger
        than `t`. The default is zero.
    intn0 : array-like or None, optional
        1D array-like of non-negative numbers, which is the initial time
        integral values of the concentration densities (this is
        particularly useful when breaking a situation into stages with
        different constant coefficients), or ``None`` (default) to mean
        all zero.
    emax_safety_factor : scalar, optional
        The safety factor to add to the approximate required emax when
        `dtype` is ``'auto'``. Must be non-negative. The default is 10,
        which corresponds to a multiplicative safety factor of 1024.
    dtype : {'auto', 'float32', 'float64', 'longdouble', 'quad'}, optional
        The numpy dtype, which must be a floating point type, or
        ``'auto'`` (default) if it should be determined automatically
        (most reliable way to avoid overflow) or ``'quad'`` if libpmadra
        is available and one wants it to use quad precision.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or
        not.

    Returns
    -------
    ninf : numpy.ndarray
        The 1D array of ``n_inf`` values from ``k = 1`` to ``k = M_c``.
    n : numpy.ndarray
        The 2D array n values from ``k = 1`` to ``k = M_c``, with one
        row per k and one column per element of `t`.
    intn : numpy.ndarray
        The 2D array of time integral of n values from ``k = 1`` to
        ``k = M_c``, with one row per k and one column per element of
        `t`.
    format_used : {'float32', 'float64', 'longdouble', 'quad'}
        The floating point format used in the calculations.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.
    OverflowError
        If it is estimated that calculating the solution would overflow
        the floating point type when `dtype` is ``auto``. Note that this
        heuristic is approximate. Set `dtype` to something other than
        ``'auto'`` to force computation even if overflow would occur.

    See Also
    --------
    required_floating_point_format

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Check the dtype and convert vecotrs.
    if dtype not in ('auto', 'quad') and dtype not in _numpy_type_info:
        raise ValueError('Invalid value of dtype.')
    # Use the libpmadra version if possible if we are supposed to
    # try. We first have to make intn0 if it was not given.
    if intn0 is None:
        if dtype in ('float32', 'float64', 'longdouble'):
            intn0 = numpy.zeros((len(n0), ), dtype=dtype)
        else:
            intn0 = numpy.zeros((len(n0), ), dtype='float32')
    if try_libpmadra:
        try:
            dtype_to_use = dtype
            dtype_to_pass = dtype
            if dtype == 'quad':
                dtype_to_pass = 'auto'
            ninf, n, intn, fmt = \
                _libpmadra.auto_solve_analytical_ninf_n_intn( \
                n0, intn0, t, beta, alpha, gamma, t0, \
                emax_safety_factor, dtype_to_use, True, \
                dtype=dtype_to_pass)
            return ninf, n, intn, fmt
        except KeyError:
            pass
        except:
            raise
    # Can't handle dtype of quad anymore.
    if dtype == 'quad':
        raise ValueError('dtype of ''quad'' only supported if '
                         'libpmadra is available.')
    # Check that alpha, gamma, emax_safety_factor, and t0 are real
    # numbers.
    if not isinstance(alpha, _libpmadra.real_classes):
        raise TypeError('alpha must be a real number.')
    if not isinstance(gamma, _libpmadra.real_classes):
        raise TypeError('gamma must be a real number.')
    if not isinstance(emax_safety_factor, _libpmadra.real_classes):
        raise TypeError('emax_safety_factor must be a real number.')
    if not isinstance(t0, _libpmadra.real_classes):
        raise TypeError('t0 must be a real number.')
    # Check that n0, intn0, and beta are 1D.
    ndim = _libpmadra.scalar_array_like_dims(n0)
    if ndim == 0:
        raise TypeError('n0 must be an array-like.')
    if ndim > 1:
        raise ValueError('n0 must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(intn0)
    if ndim == 0:
        raise TypeError('intn0 must be an array-like.')
    if ndim > 1:
        raise ValueError('intn0 must be 1D.')
    ndim = _libpmadra.scalar_array_like_dims(beta)
    if ndim == 0:
        raise TypeError('beta must be an array-like.')
    if ndim > 1:
        raise ValueError('beta must be 1D.')
    # Determine the dtype if it was passed as 'auto':
    if dtype == 'auto':
        _, dtype = required_floating_point_format(
            n0, beta, alpha, gamma, emax_safety_factor,
            try_libpmadra=try_libpmadra)
        if dtype is None or dtype == 'quad':
            raise OverflowError('Could not find a floating point type '
                                'that seemed like it would not '
                                'overflow.')
        if numpy.result_type(dtype,
                             intn0) not in _numpy_types_supported:
            raise TypeError('intn0''s dtype can''t map to a '
                            'supported dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    n0 = numpy.asarray(n0, dtype=dtype)
    intn0 = numpy.asarray(intn0, dtype=dtype)
    beta = numpy.asarray(beta, dtype=dtype)
    gamma = tp(gamma)
    alpha = tp(alpha)
    t = numpy.asarray(t, dtype=dtype)
    t0 = tp(t0)
    if numpy.isnan(alpha) or alpha < 0:
        raise ValueError('alpha must be non-NaN and non-negative.')
    if numpy.isnan(gamma) or gamma < 0:
        raise ValueError('gamma must be non-NaN and non-negative.')
    if not numpy.isfinite(t0):
        raise ValueError('t0 must be non-NaN and finite.')
    if numpy.isnan(n0).any() or numpy.any(n0 < 0):
        raise ValueError('The elements of n0 must be non-NaN and '
                         'non-negative.')
    if numpy.isnan(intn0).any() or numpy.any(intn0 < 0):
        raise ValueError('The elements of intn0 must be non-NaN and '
                         'non-negative.')
    if numpy.isnan(beta).any() or numpy.any(beta < 0):
        raise ValueError('The elements of beta must be non-NaN and '
                         'non-negative.')
    if numpy.isnan(t).any() or numpy.any(t < t0):
        raise ValueError('t must be non-NaN and greater than or equal '
                         'to t0.')
    if len(n0) != len(beta) or len(n0) != len(intn0):
        raise ValueError('n0, beta, and intn0 must have the same '
                         'length.')
    # If gamma is zero, the equations decouple and the solution is
    # easy. Otherwise, we have to do the full thing.
    dt = t - t0
    if gamma == 0:
        # If alpha = 0, the equation is linear.
        #
        #            / 0       if beta_k == 0,
        #   ninf_k = |
        #            \ +inf    otherwise.
        #   n = n_0 + beta * dt
        #   intn = intn0 + n_0 * dt + (beta / 2) * dt**2
        #
        # Otherwise,
        #
        #   ninf = beta / alpha
        #   n = ninf + (n0 - ninf) * exp(-alpha * dt)
        #   intn = intn0 + ninf * dt
        #           + (1/alpha) * (n0 - ninf) * (1 - exp(-alpha * dt))
        if alpha == 0:
            ninf = numpy.zeros_like(n0)
            ninf[beta > 0] = numpy.inf
        else:
            ninf = beta / alpha
        ninf_vert = ninf.reshape((len(ninf), 1))
        n0_vert = n0.reshape((len(ninf), 1))
        intn0_vert = intn0.reshape((len(ninf), 1))
        dt_horiz = numpy.atleast_2d(dt)
        if alpha == 0:
            beta_vert = beta.reshape((len(beta), 1))
            n = n0_vert + beta_vert * dt_horiz
            intn = intn0_vert + n0_vert * dt_horiz \
                + (tp(0.5) * beta_vert) * dt_horiz**2
        else:
            exp_neg_alpha_dt = numpy.exp(-alpha * dt_horiz)
            temp = alpha**-1 * (tp(1) - exp_neg_alpha_dt)
            n = n0_vert * exp_neg_alpha_dt + (
                tp(1) - exp_neg_alpha_dt) * ninf_vert
            intn = intn0_vert \
                + ninf_vert * dt_horiz \
                + (n0_vert - ninf_vert) * temp
    else:
        # Set z, get all the Wks(beta, z) stuff, Uks(n0, z) stuff, and
        # Uks(n0, 1).
        z = numpy.exp(-gamma * dt)
        wks_beta, uks_beta, _, uks_beta_one = _raw_Wks(
            beta, z, alpha, gamma, tp)
        uks_n0, vks_n0 = _raw_Uks(n0, z, alpha, gamma, tp)
        uks_n0_one, _ = _raw_Uks_one(n0, alpha, gamma, tp)
        # Set n_inf, n, and intn.
        z_to_s = numpy.atleast_2d(z)**(
            (alpha / gamma) + numpy.atleast_2d(
                numpy.arange(1, len(beta) + 1, dtype=tp)).T)
        ninf = -uks_beta_one
        n = z_to_s * vks_n0 + (
            numpy.atleast_2d(ninf).T + z_to_s * uks_beta)
        intn = intn0.reshape((len(ninf), 1)) \
            + numpy.atleast_2d(dt) * numpy.atleast_2d(ninf).T \
            + (z_to_s * uks_n0 - numpy.atleast_2d(uks_n0_one).T) \
            - gamma**-1 * wks_beta
        # When n is very small for low k (e.g. there is little
        # production and n0 at low k because everything is happening at
        # large k), intn is very small and due to round off and
        # numerical uncertainty can sometimes be negative. We will set
        # such negative values to zero.
        intn[intn < 0] = 0
    return ninf, n, intn, dtype


########################################################################
#
# Full numerical solution with numpy and scipy.
#
########################################################################


@numpy.errstate(all='ignore')
def build_ode_components(n0, beta, alpha, gamma, intn0=None,
                         include_integral=True, dtype='float64'):
    """ Build the components for solving the ODEs numerically.

    The structure of the state vector :math:`\\vec{y}` describing the
    system of ODEs depends on the value of `include_integral`. If
    `include_integral` is ``False``, then it is

    .. math::

       \\vec{y}(t) = \\vec{n}(t)

    If `include_integral` is ``True``, then it is

    .. math::

       \\vec{y}(t) =
         \\begin{pmatrix}
           \\int^t{\\vec{n}(v) dv} \\\\
           \\vec{n}(t)
         \\end{pmatrix}

    Parameters
    ----------
    n0 : array-like
        1D array-like of non-negative numbers, which is the initial
        concentration densities.
    beta : array-like
        1D array-like of non-negative numbers, which is the source
        vector.
    alpha : scalar
        Positive scalar. This is the sink.
    gamma : scalar
        Positive scalar. This is the inactivation rate.
    intn0 : array-like or None, optional
        1D array-like of non-negative numbers, which is the initial time
        integral values of the concentration densities (this is
        particularly useful when breaking a situation into stages with
        different constant coefficients), or ``None`` (default) to mean
        all zero.
    include_integral : bool, optional
        Whether the time integral of n should be included in the state
        and components (default) or not.
    dtype : {'float32', 'float64', 'longdouble'}, optional
        The numpy dtype, which must be a floating point type. The
        default is ``'float64'``.

    Returns
    -------
    y0 : numpy.ndarray
        Initial state vector.
    yprime : callable
        Callable that returns the first time derivative of the state
        vector when passed the arguments t and y.
    jac : scipy.sparse.csr_matrix
        The Jacobian matrix of the system. Needed by some ODE solvers.
    lband : int
        Parameter specifying the bandwidth of `jac`. All non-zero
        elements of ``jac[i, j]`` satisfy
        ``i - lband <= j <= i + uband``.
    uband: int
        See `lband`.
    beta : numpy.ndarray
        The converted `beta`.
    alpha : numpy.floating
        The converted `alpha`.
    gamma : numpy.floating
        The converted `gamma`.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    See Also
    --------
    scipy.integrate.solve_ivp

    """
    # Check the dtype and convert vectors.
    if dtype not in _numpy_type_info:
        raise ValueError('Invalid value of dtype.')
    tp, _, _ = _numpy_type_info[dtype]
    n0 = numpy.asarray(n0, dtype=dtype)
    beta = numpy.asarray(beta, dtype=dtype)
    if intn0 is None:
        intn0 = numpy.zeros_like(n0)
    else:
        intn0 = numpy.asarray(intn0, dtype=dtype)
    for k, v in (('n0', n0), ('beta', beta),
                 ('intn0', intn0)):
        if v.ndim != 1:
            raise ValueError(k + ' must be 1D.')
        if numpy.any(v < 0):
            raise ValueError('The elements of ' + k
                             + ' must be non-negative.')
    if len(n0) != len(beta) or len(n0) != len(intn0):
        raise TypeError('n0, beta, and intn0 must have the same '
                        'length.')
    Mc = len(n0)
    # Convert alpha and gamma and check that they are non-negative
    # respectively.
    alpha = tp(alpha)
    gamma = tp(gamma)
    if alpha < 0:
        raise ValueError('alpha must be non-negative.')
    if gamma < 0:
        raise ValueError('gamma must be non-negative.')
    # Build the sink diagonal and the flux in upper diagonal.
    sinks = -alpha - gamma * numpy.arange(1, Mc + 1, dtype=dtype)
    flux_in = gamma * numpy.arange(2, Mc + 1, dtype=dtype)
    # The initial state and yprime depend on whether we are including
    # the integral or not.
    if include_integral:
        y0 = numpy.hstack((intn0, n0))

        def yprime(_, y):
            out = numpy.tile(y[Mc:], (2, ))
            out[Mc:] *= sinks
            out[Mc:-1] += flux_in * y[(Mc + 1):]
            out[Mc:] += beta
            return out
    else:
        y0 = n0

        def yprime(_, y):
            out = sinks * y
            out[:-1] += flux_in * y[1:]
            out += beta
            return out
    # Build the Jacobian matrix and the bandwidths.
    jacblock_lowerright = scipy.sparse.diags(
        (sinks, flux_in), (0, 1), shape=(Mc, Mc), format='csr')
    if include_integral:
        jacblock_upperleft = scipy.sparse.csr_matrix((Mc, Mc),
                                                     dtype=dtype)
        jacblock_lowerleft = jacblock_upperleft
        jacblock_upperright = scipy.sparse.eye(Mc, dtype=dtype,
                                               format='csr')
        jac = scipy.sparse.bmat([[jacblock_upperleft,
                                  jacblock_upperright],
                                 [jacblock_lowerleft,
                                  jacblock_lowerright]],
                                'csr', dtype)
        lband = 0
        uband = Mc
    else:
        jac = jacblock_lowerright
        lband = 0
        uband = 1
    return y0, yprime, jac, lband, uband, beta, alpha, gamma
