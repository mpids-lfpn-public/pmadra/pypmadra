# Copyright 2020 Max Planck Institute for Dynamics and Self-Organization
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

""" Dose-response models for poly-multiplicity aerosols.

Dose-response models modified for poly-multiplicity aerosols where the
aerosol dose for each multiplicity follows a Poisson distribution.

"""


import numpy

import pypmadra._libpmadra as _libpmadra


@numpy.errstate(all='ignore')
def mean_risk_exponential(mus, r, dtype='auto', try_libpmadra=True):
    """ Calculate the mean infection risk with the exponential model.

    The exact aerosol doses for each multiplicity are assumed to follow
    Poisson distributions. Then, the infection risk is

    .. math::

       \\mathfrak{R}_E\\left(\\mu_1, \\dots, \\mu_\\infty\\right) =
           1 - \\exp{\\left[-\\sum_{k=1}^\\infty{
           \\left(1 - (1 - r)^k\\right) \\mu_k}\\right]} \\quad .

    Parameters
    ----------
    mus : array-like
        1D or 2D array-like of the mean aerosol dose vectors
        (mu-vectors), whose elements must all be non-negative. If 2D,
        each column is a different mu-vector.
    r : scalar
        The infection risk for a single pathogen copy. Must be between 0
        and 1 inclusive.
    dtype : {'auto', 'float32', 'float64', 'longdouble'}, optional
        The dtype to do the calculations in. ``'auto'`` (the default)
        means to select it automatically based on `mus` and `r`.
    try_libpmadra : bool, optional
        Whether to try to use libpmadra if it is available (default) or not.

    Returns
    -------
    risks : scalar or numpy.ndarray
        The mean infection risk/s, one for each mu-vector. Is a scalar
        if `mus` is 1D and a 1D array if `mus` is 2D.

    Raises
    ------
    TypeError
        If an argument has an invalid type.
    ValueError
        If an argument has an invalid value.

    """
    # Check try_libpmadra
    if not isinstance(try_libpmadra, bool):
        raise TypeError('try_libpmadra must be bool.')
    # Convert mus to an array of the specified dtype.
    if dtype not in ('auto', 'float32', 'float64', 'longdouble'):
        raise ValueError('Invalid value of dtype.')
    if not numpy.isscalar(r):
        raise TypeError('r must be a scalar.')
    # Make sure mus is 1D or 2D.
    ndim = _libpmadra.scalar_array_like_dims(mus)
    if ndim == 0:
        raise TypeError('mus must be an array-like.')
    if ndim > 2:
        raise ValueError('mus must be 1D or 2D.')
    if dtype == 'auto':
        mus = numpy.asarray(mus, dtype=numpy.result_type('float32', r,
                                                         mus))
        if mus.dtype == numpy.dtype('longdouble'):
            dtype = 'longdouble'
        else:
            dtype = mus.dtype.name
    else:
        mus = numpy.asarray(mus, dtype=dtype)
    # Make sure the dtype is suitable.
    if dtype not in {'float32', 'float64', 'longdouble'}:
        raise TypeError('Inputs don''t map to a valid dtype.')
    # Convert r.
    r = numpy.dtype(dtype).type(r)
    # Try the libpmadra version if it is available and we are supposed
    # to try.
    if try_libpmadra:
        try:
            risks = _libpmadra.mean_risk_exponential(mus, r, True,
                                                     dtype=dtype)
            if ndim == 1:
                return risks[0]
            else:
                return risks
        except KeyError:
            pass
        except:
            raise
    # We have to do it in pure NumPy.
    #
    # Check mus and r.
    if not numpy.isfinite(r) or r < 0 or r > 1:
        raise ValueError('r must be between 0 and 1 inclusive.')
    if numpy.isnan(mus).any() or numpy.any(mus < 0):
        raise ValueError('mus must be non-NaN and non-negative.')
    # Get Mc and define one in the desired precision.
    Mc = mus.shape[0]
    one = numpy.dtype(dtype).type(1)
    # Allocate risks as all zeros.
    if mus.ndim == 1:
        risks = numpy.dtype(dtype).type(0)
    else:
        risks = numpy.zeros((mus.shape[1], ), dtype=dtype)
    # Handle the special cases r == 0 and r == 1.
    if r == 0.0:
        return risks
    elif r == 1.0:
        return one - numpy.exp(-mus.sum(axis=0))
    # Make a vector of the 1 - (1 - r)**k.
    coeffs = one - (one - r)**numpy.arange(1, Mc + 1, dtype='int64')
    if mus.ndim == 2:
        coeffs.shape = (Mc, 1)
    # Do the sum into risks, then exponentiate and take the difference
    # with one.
    risks += numpy.sum(coeffs * mus, axis=0)
    return one - numpy.exp(-risks)
